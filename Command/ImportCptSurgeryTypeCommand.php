<?php
/**
 * Created by PhpStorm.
 * User: Jeroen
 * Date: 27-8-2019
 * Time: 04:43
 */

namespace App\Command;

use App\Entity\CptSurgeryType;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportCptSurgeryTypeCommand extends Command
{
	protected static $defaultName = 'medica:import-cpt-surgery-type';

	private $entityManager;

	private $container;

	public function __construct(ContainerInterface $container)
	{
		parent::__construct();
		$this->container = $container;
	}

	protected function configure()
	{
		$this
			->setDescription("Import Cpt surgery type")
			->setHelp("This command allows you to import all Cpt surgery types")
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$output->writeln("Start import import-cpt-surgery-types");

		$InputPath = getcwd() . '/src/Command/ImportData/';

		$InputFile = "tbl_type_of_surgery.csv";

		$InputPathFile = $InputPath . $InputFile;

		if(!file_exists($InputPathFile)) {

			$LogData = "ERROR: LET OP! Bestand " . $InputPathFile . " bestaat niet";

			$output->writeln("<info>". $LogData . "</info>");

			exit;
		}

		$this->entityManager = $this->container->get('doctrine')->getManager();

		$handle = fopen($InputPathFile, "r");

		if (empty($handle) === false) {

			while (($data = fgetcsv($handle, 999999, "|")) !== FALSE) {

				$aContent['tos_id'] = (int) $data[0];
				$aContent['description'] = $data[1];

				if(!$this->existsInDb($aContent['description']))
				{
					$CptTypeOfSurgery = new CptSurgeryType();
					$CptTypeOfSurgery->setDescription($aContent['description']);
					$CptTypeOfSurgery->setTosId($aContent['tos_id']);

					$this->entityManager->persist($CptTypeOfSurgery);
				}
			}

			$this->entityManager->flush();
		}

		$output->writeln("Einde import import-cpt-surgery-type");
	}

	private function existsInDb($description)
	{
		$result = $this->entityManager->getRepository(CptSurgeryType::class)->findBy(
			['Description' => $description]
		);

		if($result) {

			return true;
		}
		return false;
	}
}