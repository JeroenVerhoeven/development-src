<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MaxCptNumRepository")
 */
class MaxCptNum
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $MaxCptNum;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMaxCptNum(): ?int
    {
        return $this->MaxCptNum;
    }

    public function setMaxCptNum(int $MaxCptNum): self
    {
        $this->MaxCptNum = $MaxCptNum;

        return $this;
    }
}
