<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Table(indexes={
 *         @Index(name="cpt_Component_number", columns={"cpt_number","component_number"}),
 *         @Index(name="component_number", columns={"component_number"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\CptComponentenRepository")
 */
class CptComponenten
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $CptNumber;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ComponentNumber;

    /**
     * @ORM\Column(type="integer")
     */
    private $Quantity;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cpt", inversedBy="cptComponentens")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Cpt;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5, nullable=true)
     */
    private $PurchasePrice;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $SampleSupplied;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Image;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=4, nullable=true)
     */
    private $NormTime;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ComponentStatus")
     */
    private $CptComponentStatus;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ComponentGroup", inversedBy="cptComponentens")
     */
    private $ComponentGroup;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ComponentClassification", inversedBy="cptComponentens")
     */
    private $ComponentClassification;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ComponentStatus", inversedBy="cptComponentens")
     */
    private $Status;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ComponentType", inversedBy="cptComponentens")
     */
    private $ComponentType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Component")
     */
    private $Component;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ComponentSupplier")
     */
    private $ComponentSupplier;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ComponentNumberSupplier;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCptNumber(): ?string
    {
        return $this->CptNumber;
    }

    public function setCptNumber(string $CptNumber): self
    {
        $this->CptNumber = $CptNumber;

        return $this;
    }

    public function getComponentNumber(): ?string
    {
        return $this->ComponentNumber;
    }

    public function setComponentNumber(string $ComponentNumber): self
    {
        $this->ComponentNumber = $ComponentNumber;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->Quantity;
    }

    public function setQuantity(int $Quantity): self
    {
        $this->Quantity = $Quantity;

        return $this;
    }

    public function getCpt(): ?Cpt
    {
        return $this->Cpt;
    }

    public function setCpt(?Cpt $Cpt): self
    {
        $this->Cpt = $Cpt;

        return $this;
    }

    public function getPurchasePrice(): ?string
    {
        return $this->PurchasePrice;
    }

    public function setPurchasePrice(?string $PurchasePrice): self
    {
        $this->PurchasePrice = $PurchasePrice;

        return $this;
    }

    public function getSampleSupplied(): ?string
    {
        return $this->SampleSupplied;
    }

    public function setSampleSupplied(?string $SampleSupplied): self
    {
        $this->SampleSupplied = $SampleSupplied;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->Image;
    }

    public function setImage(?string $Image): self
    {
        $this->Image = $Image;

        return $this;
    }

    public function getNormTime(): ?string
    {
        return $this->NormTime;
    }

    public function setNormTime(?string $NormTime): self
    {
        $this->NormTime = $NormTime;

        return $this;
    }

    public function getCptComponentStatus(): ?ComponentStatus
    {
        return $this->CptComponentStatus;
    }

    public function setCptComponentStatus(?ComponentStatus $CptComponentStatus): self
    {
        $this->CptComponentStatus = $CptComponentStatus;

        return $this;
    }

    public function getComponentGroup(): ?ComponentGroup
    {
        return $this->ComponentGroup;
    }

    public function setComponentGroup(?ComponentGroup $ComponentGroup): self
    {
        $this->ComponentGroup = $ComponentGroup;

        return $this;
    }

    public function getComponentClassification(): ?ComponentClassification
    {
        return $this->ComponentClassification;
    }

    public function setComponentClassification(?ComponentClassification $ComponentClassification): self
    {
        $this->ComponentClassification = $ComponentClassification;

        return $this;
    }

    public function getStatus(): ?ComponentStatus
    {
        return $this->Status;
    }

    public function setStatus(?ComponentStatus $Status): self
    {
        $this->Status = $Status;

        return $this;
    }

    public function getComponentType(): ?ComponentType
    {
        return $this->ComponentType;
    }

    public function setComponentType(?ComponentType $ComponentType): self
    {
        $this->ComponentType = $ComponentType;

        return $this;
    }

    public function getComponent(): ?Component
    {
        return $this->Component;
    }

    public function setComponent(?Component $Component): self
    {
        $this->Component = $Component;

        return $this;
    }

    public function getComponentSupplier(): ?ComponentSupplier
    {
        return $this->ComponentSupplier;
    }

    public function setComponentSupplier(?ComponentSupplier $ComponentSupplier): self
    {
        $this->ComponentSupplier = $ComponentSupplier;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(?string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getComponentNumberSupplier(): ?string
    {
        return $this->ComponentNumberSupplier;
    }

    public function setComponentNumberSupplier(?string $ComponentNumberSupplier): self
    {
        $this->ComponentNumberSupplier = $ComponentNumberSupplier;

        return $this;
    }
}
