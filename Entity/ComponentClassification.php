<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ComponentClassificationRepository")
 */
class ComponentClassification
{

	public function __toString()
	{
		return $this->getDescription();
	}


	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $Description;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Component", mappedBy="Classification")
	 */
	private $components;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\CptTempComponenten", mappedBy="Classification")
	 */
	private $cptTempComponentens;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\CptComponenten", mappedBy="ComponentClassification")
	 */
	private $cptComponentens;

	public function __construct()
	{
		$this->components = new ArrayCollection();
		$this->cptTempComponentens = new ArrayCollection();
		$this->cptComponentens = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getDescription(): ?string
	{
		return $this->Description;
	}

	public function setDescription(string $Description): self
	{
		$this->Description = $Description;

		return $this;
	}

	/**
	 * @return Collection|Component[]
	 */
	public function getComponents(): Collection
	{
		return $this->components;
	}

	public function addComponent(Component $component): self
	{
		if (!$this->components->contains($component)) {
			$this->components[] = $component;
			$component->setClassification($this);
		}

		return $this;
	}

	public function removeComponent(Component $component): self
	{
		if ($this->components->contains($component)) {
			$this->components->removeElement($component);
			// set the owning side to null (unless already changed)
			if ($component->getClassification() === $this) {
				$component->setClassification(null);
			}
		}

		return $this;
	}

	/**
	 * @return Collection|CptTempComponenten[]
	 */
	public function getCptTempComponentens(): Collection
	{
		return $this->cptTempComponentens;
	}

	public function addCptTempComponenten(CptTempComponenten $cptTempComponenten): self
	{
		if (!$this->cptTempComponentens->contains($cptTempComponenten)) {
			$this->cptTempComponentens[] = $cptTempComponenten;
			$cptTempComponenten->setClassification($this);
		}

		return $this;
	}

	public function removeCptTempComponenten(CptTempComponenten $cptTempComponenten): self
	{
		if ($this->cptTempComponentens->contains($cptTempComponenten)) {
			$this->cptTempComponentens->removeElement($cptTempComponenten);
			// set the owning side to null (unless already changed)
			if ($cptTempComponenten->getClassification() === $this) {
				$cptTempComponenten->setClassification(null);
			}
		}

		return $this;
	}

	/**
	 * @return Collection|CptComponenten[]
	 */
	public function getCptComponentens(): Collection
	{
		return $this->cptComponentens;
	}

	public function addCptComponenten(CptComponenten $cptComponenten): self
	{
		if (!$this->cptComponentens->contains($cptComponenten)) {
			$this->cptComponentens[] = $cptComponenten;
			$cptComponenten->setComponentClassification($this);
		}

		return $this;
	}

	public function removeCptComponenten(CptComponenten $cptComponenten): self
	{
		if ($this->cptComponentens->contains($cptComponenten)) {
			$this->cptComponentens->removeElement($cptComponenten);
			// set the owning side to null (unless already changed)
			if ($cptComponenten->getComponentClassification() === $this) {
				$cptComponenten->setComponentClassification(null);
			}
		}

		return $this;
	}
}
