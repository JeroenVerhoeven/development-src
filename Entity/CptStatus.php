<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CptStatusRepository")
 */
class CptStatus
{
	public function __toString()
	{
		return $this->getDescription();
	}

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $CptStatusId;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $Description;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Cpt", mappedBy="CptStatus")
	 */
	private $cpts;

	public function __construct()
	{
		$this->cpts = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getCptStatusId(): ?int
	{
		return $this->CptStatusId;
	}

	public function setCptStatusId(int $CptStatusId): self
	{
		$this->CptStatusId = $CptStatusId;

		return $this;
	}

	public function getDescription(): ?string
	{
		return $this->Description;
	}

	public function setDescription(string $Description): self
	{
		$this->Description = $Description;

		return $this;
	}

	/**
	 * @return Collection|Cpt[]
	 */
	public function getCpts(): Collection
	{
		return $this->cpts;
	}

	public function addCpt(Cpt $cpt): self
	{
		if (!$this->cpts->contains($cpt)) {
			$this->cpts[] = $cpt;
			$cpt->setCptStatus($this);
		}

		return $this;
	}

	public function removeCpt(Cpt $cpt): self
	{
		if ($this->cpts->contains($cpt)) {
			$this->cpts->removeElement($cpt);
			// set the owning side to null (unless already changed)
			if ($cpt->getCptStatus() === $this) {
				$cpt->setCptStatus(null);
			}
		}

		return $this;
	}
}
