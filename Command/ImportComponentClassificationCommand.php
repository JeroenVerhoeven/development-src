<?php
/**
 * Created by PhpStorm.
 * User: Jeroen
 * Date: 27-8-2019
 * Time: 04:43
 */

namespace App\Command;

use App\Entity\ComponentClassification;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportComponentClassificationCommand extends Command
{
	protected static $defaultName = 'medica:import-component-classification';

	private $entityManager;

	private $container;

	public function __construct(ContainerInterface $container)
	{
		parent::__construct();
		$this->container = $container;
	}

	protected function configure()
	{
		$this
			->setDescription("Import Component classification")
			->setHelp("This command allows you to import all Component classifications")
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$output->writeln("Start import import-component-classification");

		$InputPath = getcwd() . '/src/Command/ImportData/';

		$InputFile = "tbl_classificatie.csv";

		$InputPathFile = $InputPath . $InputFile;

		if(!file_exists($InputPathFile)) {

			$LogData = "ERROR: LET OP! Bestand " . $InputPathFile . " bestaat niet";

			$output->writeln("<info>". $LogData . "</info>");

			exit;
		}

		$this->entityManager = $this->container->get('doctrine')->getManager();

		$handle = fopen($InputPathFile, "r");

		if (empty($handle) === false) {

			while (($data = fgetcsv($handle, 999999, "|")) !== FALSE) {

				$aContent['id'] = $data[0];
				$aContent['description'] = $data[1];

				if(!$this->existsInDb($aContent['description']))
				{
					$ComponentClassification = new ComponentClassification();
					$ComponentClassification->setDescription($aContent['description']);

					$this->entityManager->persist($ComponentClassification);
				}
			}

			$this->entityManager->flush();
		}

		$output->writeln("Einde import import-component-classification");
	}

	private function existsInDb($description)
	{
		$result = $this->entityManager->getRepository(ComponentClassification::class)->findBy(
			['Description' => $description]
		);

		if($result) {

			return true;
		}
		return false;
	}
}