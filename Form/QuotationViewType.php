<?php

namespace App\Form;

use App\Entity\WtQuotation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class QuotationViewType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('QuotationNumber', TextType::class, [
                    'disabled' => true,
            ])
            ->add('Version', TextType::class, [
                    'disabled' => true,
            ])
            ->add('Date', DateType::class, [
                    'disabled' => true,
            ])
            ->add('Employee', TextType::class, [
                    'disabled' => true,
            ])
            ->add('Memo1', TextareaType::class, [
                    'disabled' => true,
            ])
            ->add('Memo2', TextareaType::class, [
                    'disabled' => true,
            ])
            ->add('Name', TextType::class, [
                    'disabled' => true,
            ])
            ->add('Customer', TextType::class, [
                    'disabled' => true,
            ])
            ->add('CompanyContacts', TextType::class, [
                    'disabled' => true,
            ])
//            ->add('Status')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WtQuotation::class,
        ]);
    }
}
