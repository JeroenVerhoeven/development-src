<?php

namespace App\Repository;

use App\Entity\Company;
use App\Entity\Cpt;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Cpt|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cpt|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cpt[]    findAll()
 * @method Cpt[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CptRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, Cpt::class);
	}

	/**
	 * @return Cpt[] Returns an array of Cpt objects
	 */
	public function findByFirst($limit)
	{
		return $this->createQueryBuilder('c')
			->andWhere('c.CptStatus IN (:statusses)')
			->setParameter(':statusses', [1,2])
			->orderBy('c.CptNumber', 'DESC')
			->setMaxResults($limit)
			->getQuery()
			->getResult();
	}

	public function findByCptNumber($cptNumber)
	{
		return $this->createQueryBuilder('c')
                        ->leftJoin('c.cptComponentens','cptco')
                        ->leftJoin('cptco.Component','co')
                        ->addSelect('cptco')
                        ->addSelect('co')
			->andWhere('c.CptNumber = :cptnumber')
			->setParameter(':cptnumber', $cptNumber)
			->orderBy('c.CptNumber', 'DESC')
//			->setMaxResults(1)
			->getQuery()
			->getOneOrNullResult();
	}

        public function findCptByCptNumber($cptNumber)
        {
                return $this->createQueryBuilder('c')
                        ->andWhere('c.CptNumber = :cptnumber')
                        ->setParameter(':cptnumber', $cptNumber)
			->setMaxResults(1)
                        ->getQuery()
                        ->getOneOrNullResult();
        }

        public function filterCpts($aSearchfield)
        {
		$QueryBuilder =  $this->createQueryBuilder('c');

		if(!empty($aSearchfield['searchCustomer'])) {
			$QueryBuilder->leftJoin('c.Company','b');
			$QueryBuilder->andWhere('b.CompanyName LIKE  :CompanyName');
			$QueryBuilder->setParameter("CompanyName", '%' . $aSearchfield['searchCustomer'] . '%');
		}

		if(!empty($aSearchfield['searchComponent'])) {
			$QueryBuilder->leftJoin('c.cptComponentens','co');
			$QueryBuilder->andWhere('co.ComponentNumber LIKE  :ComponentNumber');
			$QueryBuilder->setParameter("ComponentNumber", '%' . $aSearchfield['searchComponent'] . '%');
		}

		if(!empty($aSearchfield['searchNumber'])) {
			$QueryBuilder->andWhere('c.CptNumber LIKE  :CptNumber');
			$QueryBuilder->setParameter("CptNumber", '%' . $aSearchfield['searchNumber'] . '%');
		}

		if(!empty($aSearchfield['searchName'])) {
			$QueryBuilder->andWhere('c.ProductDescriptionCustomer LIKE  :ProductDescriptionCustomer');
			$QueryBuilder->setParameter("ProductDescriptionCustomer", '%' . $aSearchfield['searchName'] . '%');
		}

		if($aSearchfield['viewAllCpts'] == 'No') {
			$aCptStatus = array(1, 2);
			$QueryBuilder->andWhere('c.CptStatus IN (:statusses)');
			$QueryBuilder->setParameter(':statusses', $aCptStatus);
		}

		$QueryBuilder->setMaxResults(1000)
			->orderBy('c.CptNumber', 'ASC');

		return $QueryBuilder->getQuery()->getResult();

        }

        public function getNormtijdInkoopOld($cptNumber)
	{
		return $this->createQueryBuilder('c')
			->select('sum(co.NormTime * cptco.Quantity) as NormTime', 'sum(cptco.PurchasePrice * cptco.Quantity) as Inkoop')
			->leftJoin('c.cptComponentens','cptco')
			->leftJoin('cptco.Component','co')
			->andWhere('c.CptNumber =  :CptNumber')
			->andWhere('cptco.Status !=  :CptCompStatus')
			->setParameter("CptNumber", $cptNumber)
			->setParameter("CptCompStatus", 5)
			->getQuery()
			->getOneOrNullResult();
	}

	public function getNormtijdInkoop($cptNumber)
	{
		$sql = "SELECT 	SUM(c.norm_time * cpt.quantity) as NormTime 
		    	, 	SUM(c.puchase_price * cpt.quantity) as Inkoop 
			FROM cpt_componenten cpt
			LEFT JOIN component c ON c.component_number = cpt.component_number AND c.puchase_price > 0 
			WHERE c.component_number in ( select component_number FROM cpt_componenten WHERE cpt_number = :cpt_number AND IFNULL(status_id,0) <> 5 ) 
			AND cpt_number = :cpt_number";

		$conn = $this->getEntityManager()->getConnection();

		$stmt = $conn->prepare($sql);

		$stmt->execute(array('cpt_number' => $cptNumber));

		$result = $stmt->fetchAll();

		return $result[0];
	}

	// /**
	//  * @return Cpt[] Returns an array of Cpt objects
	//  */
	/*
	public function findByExampleField($value)
	{
	    return $this->createQueryBuilder('c')
		->andWhere('c.exampleField = :val')
		->setParameter('val', $value)
		->orderBy('c.id', 'ASC')
		->setMaxResults(10)
		->getQuery()
		->getResult()
	    ;
	}
	*/

	/*
	public function findOneBySomeField($value): ?Cpt
	{
	    return $this->createQueryBuilder('c')
		->andWhere('c.exampleField = :val')
		->setParameter('val', $value)
		->getQuery()
		->getOneOrNullResult()
	    ;
	}
	*/
}
