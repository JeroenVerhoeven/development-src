<?php

namespace App\Repository;

use App\Entity\ComponentClassification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ComponentClassification|null find($id, $lockMode = null, $lockVersion = null)
 * @method ComponentClassification|null findOneBy(array $criteria, array $orderBy = null)
 * @method ComponentClassification[]    findAll()
 * @method ComponentClassification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComponentClassificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ComponentClassification::class);
    }

    // /**
    //  * @return ComponentClassification[] Returns an array of ComponentClassification objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ComponentClassification
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
