<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191224103705 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE wt_version (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE wt_quotation ADD wt_version_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE wt_quotation ADD CONSTRAINT FK_8CA0CBB7ACCFE044 FOREIGN KEY (wt_version_id) REFERENCES wt_version (id)');
        $this->addSql('CREATE INDEX IDX_8CA0CBB7ACCFE044 ON wt_quotation (wt_version_id)');
        $this->addSql('DROP INDEX component_number ON component');
        $this->addSql('CREATE INDEX component_number ON component (component_number)');
        $this->addSql('DROP INDEX cpt_number ON cpt');
        $this->addSql('CREATE INDEX cpt_number ON cpt (cpt_number)');
        $this->addSql('DROP INDEX component_number ON cpt_componenten');
        $this->addSql('DROP INDEX cpt_Component_number ON cpt_componenten');
        $this->addSql('CREATE INDEX component_number ON cpt_componenten (component_number)');
        $this->addSql('CREATE INDEX cpt_Component_number ON cpt_componenten (cpt_number, component_number)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE wt_quotation DROP FOREIGN KEY FK_8CA0CBB7ACCFE044');
        $this->addSql('DROP TABLE wt_version');
        $this->addSql('DROP INDEX component_number ON component');
        $this->addSql('CREATE INDEX component_number ON component (component_number(191))');
        $this->addSql('DROP INDEX cpt_number ON cpt');
        $this->addSql('CREATE INDEX cpt_number ON cpt (cpt_number(191))');
        $this->addSql('DROP INDEX cpt_Component_number ON cpt_componenten');
        $this->addSql('DROP INDEX component_number ON cpt_componenten');
        $this->addSql('CREATE INDEX cpt_Component_number ON cpt_componenten (cpt_number(191), component_number(191))');
        $this->addSql('CREATE INDEX component_number ON cpt_componenten (component_number(191))');
        $this->addSql('DROP INDEX IDX_8CA0CBB7ACCFE044 ON wt_quotation');
        $this->addSql('ALTER TABLE wt_quotation DROP wt_version_id');
    }
}
