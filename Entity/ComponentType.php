<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ComponentTypeRepository")
 */
class ComponentType
{
	public function __toString()
	{
		return $this->getDescription();
	}

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $Description;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\CptTempComponenten", mappedBy="ComponentType")
	 */
	private $cptTempComponentens;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\CptComponenten", mappedBy="ComponentType")
	 */
	private $cptComponentens;

	public function __construct()
	{
		$this->cptTempComponentens = new ArrayCollection();
		$this->cptComponentens = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getDescription(): ?string
	{
		return $this->Description;
	}

	public function setDescription(string $Description): self
	{
		$this->Description = $Description;

		return $this;
	}

	/**
	 * @return Collection|CptTempComponenten[]
	 */
	public function getCptTempComponentens(): Collection
	{
		return $this->cptTempComponentens;
	}

	public function addCptTempComponenten(CptTempComponenten $cptTempComponenten): self
	{
		if (!$this->cptTempComponentens->contains($cptTempComponenten)) {
			$this->cptTempComponentens[] = $cptTempComponenten;
			$cptTempComponenten->setComponentType($this);
		}

		return $this;
	}

	public function removeCptTempComponenten(CptTempComponenten $cptTempComponenten): self
	{
		if ($this->cptTempComponentens->contains($cptTempComponenten)) {
			$this->cptTempComponentens->removeElement($cptTempComponenten);
			// set the owning side to null (unless already changed)
			if ($cptTempComponenten->getComponentType() === $this) {
				$cptTempComponenten->setComponentType(null);
			}
		}

		return $this;
	}

	/**
	 * @return Collection|CptComponenten[]
	 */
	public function getCptComponentens(): Collection
	{
		return $this->cptComponentens;
	}

	public function addCptComponenten(CptComponenten $cptComponenten): self
	{
		if (!$this->cptComponentens->contains($cptComponenten)) {
			$this->cptComponentens[] = $cptComponenten;
			$cptComponenten->setComponentType($this);
		}

		return $this;
	}

	public function removeCptComponenten(CptComponenten $cptComponenten): self
	{
		if ($this->cptComponentens->contains($cptComponenten)) {
			$this->cptComponentens->removeElement($cptComponenten);
			// set the owning side to null (unless already changed)
			if ($cptComponenten->getComponentType() === $this) {
				$cptComponenten->setComponentType(null);
			}
		}

		return $this;
	}
}
