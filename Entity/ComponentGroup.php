<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ComponentGroupRepository")
 */
class ComponentGroup
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ComponentGroupId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Component", mappedBy="ComponentGroup")
     */
    private $components;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CptComponenten", mappedBy="ComponentGroup")
     */
    private $cptComponentens;

    public function __construct()
    {
        $this->components = new ArrayCollection();
        $this->cptComponentens = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComponentGroupId(): ?int
    {
        return $this->ComponentGroupId;
    }

    public function setComponentGroupId(?int $ComponentGroupId): self
    {
        $this->ComponentGroupId = $ComponentGroupId;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    /**
     * @return Collection|Component[]
     */
    public function getComponents(): Collection
    {
        return $this->components;
    }

    public function addComponent(Component $component): self
    {
        if (!$this->components->contains($component)) {
            $this->components[] = $component;
            $component->setComponentGroup($this);
        }

        return $this;
    }

    public function removeComponent(Component $component): self
    {
        if ($this->components->contains($component)) {
            $this->components->removeElement($component);
            // set the owning side to null (unless already changed)
            if ($component->getComponentGroup() === $this) {
                $component->setComponentGroup(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CptComponenten[]
     */
    public function getCptComponentens(): Collection
    {
        return $this->cptComponentens;
    }

    public function addCptComponenten(CptComponenten $cptComponenten): self
    {
        if (!$this->cptComponentens->contains($cptComponenten)) {
            $this->cptComponentens[] = $cptComponenten;
            $cptComponenten->setComponentGroup($this);
        }

        return $this;
    }

    public function removeCptComponenten(CptComponenten $cptComponenten): self
    {
        if ($this->cptComponentens->contains($cptComponenten)) {
            $this->cptComponentens->removeElement($cptComponenten);
            // set the owning side to null (unless already changed)
            if ($cptComponenten->getComponentGroup() === $this) {
                $cptComponenten->setComponentGroup(null);
            }
        }

        return $this;
    }
}
