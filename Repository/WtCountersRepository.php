<?php

namespace App\Repository;

use App\Entity\WtCounters;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method WtCounters|null find($id, $lockMode = null, $lockVersion = null)
 * @method WtCounters|null findOneBy(array $criteria, array $orderBy = null)
 * @method WtCounters[]    findAll()
 * @method WtCounters[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WtCountersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WtCounters::class);
    }

    // /**
    //  * @return WtCounters[] Returns an array of WtCounters objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WtCounters
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
