<?php

namespace App\Repository;

use App\Entity\WtHelpArticle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method WtHelpArticle|null find($id, $lockMode = null, $lockVersion = null)
 * @method WtHelpArticle|null findOneBy(array $criteria, array $orderBy = null)
 * @method WtHelpArticle[]    findAll()
 * @method WtHelpArticle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WtHelpArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WtHelpArticle::class);
    }

    // /**
    //  * @return WtHelpArticle[] Returns an array of WtHelpArticle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WtHelpArticle
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
