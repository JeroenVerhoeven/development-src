<?php

namespace App\Repository;

use App\Entity\Component;
use App\Entity\ComponentSupplier;
use App\Entity\ComponentClassification;
use App\Entity\ComponentStatus;
use App\Entity\ComponentGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * @method Component|null find($id, $lockMode = null, $lockVersion = null)
 * @method Component|null findOneBy(array $criteria, array $orderBy = null)
 * @method Component[]    findAll()
 * @method Component[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComponentRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, Component::class);
	}
	/**
	 * @return Components[]
	 */
	public function findByExampleField()
	{
		return $this->createQueryBuilder('c')
			->setMaxResults(30)
			->getQuery()
			->getResult()
			;
	}

	public function findFirstRowsComponent($MaxResults)
	{
		return $this->createQueryBuilder('c')
			->leftJoin('c.Supplier', 'co')
			->leftJoin('c.ComponentGroup', 'b')
			->leftJoin('c.Status', 'd')
			->leftJoin('c.Classification', 'e')
			->orderBy('co.Name', 'ASC')
			->andWhere('co.Name IS NOT NULL')
			->andWhere("c.Supplier != ''")
			->setMaxResults($MaxResults)
			->getQuery()
			->getResult();
	}

	public function searchComponents($aSearchFields)
	{
		$QueryBuilder = $this->createQueryBuilder('c')
			->select(
				'c.id',
				'c.ComponentNumber',
				'c.ComponentNumberSupplier',
				'c.PuchasePrice',
				'c.Description as ComponentDescription',
				'co.Name',
				'b.Description as ComponentGroup',
				'd.Description as Status',
				'e.Description as Classification',
				'c.Image'
			)
			->leftJoin('c.Supplier', 'co')
			->leftJoin('c.ComponentGroup', 'b')
			->leftJoin('c.Status', 'd')
			->leftJoin('c.Classification', 'e');


		if ($aSearchFields['searchSupplier'] != '-1') {
			$QueryBuilder->andWhere('co.id = :SupplierId')
				->setParameter('SupplierId', $aSearchFields['searchSupplier']);
		}

		if ($aSearchFields['searchCompGroup'] != '-1') {
			$QueryBuilder->andWhere('b.id = :CompGroupId')
				->setParameter('CompGroupId', $aSearchFields['searchCompGroup']);
		}

		if (!empty($aSearchFields['searchCompNumber'])) {
			$QueryBuilder->andWhere('c.ComponentNumberSupplier LIKE :ComponentNumberSupplier')
				->setParameter('ComponentNumberSupplier', '%' . $aSearchFields['searchCompNumber'] . '%');;
		}

		if (!empty($aSearchFields['searchDescription'])) {
			$QueryBuilder->andWhere('c.Description LIKE :Description')
				->setParameter('Description', '%' . $aSearchFields['searchDescription'] . '%');;
		}

		if (!empty($aSearchFields['searchCompNumberMedica'])) {
			$QueryBuilder->andWhere('c.ComponentNumber LIKE :CompNumberMedica')
				->setParameter('CompNumberMedica', '%' . $aSearchFields['searchCompNumberMedica'] . '%');;
		}

		return $QueryBuilder->orderBy('c.ComponentNumber', 'ASC')
			->getQuery()
			->getResult();
	}

	public function getComponentByComponentNumber($ComponentID)
	{

		return $this->createQueryBuilder('c')
			->andWhere('c.ComponentNumber = :ComponentNumber')
			->setParameter('ComponentNumber', $ComponentID)
			->getQuery()
			->getOneOrNullResult();
	}

	public function getPackageComponents($ComponentGroupId)
	{
		return $this->createQueryBuilder('c')
			->select(
				'trim(c.ComponentNumber) as id',
				'c.Description as Packaging',
				'c.PuchasePrice as Price'
			)
			->andWhere('c.ComponentGroup = :ComponentGroupId')
			->setParameter('ComponentGroupId', $ComponentGroupId)
			->getQuery()
			->getResult();
	}

	// /**
	//  * @return Component[] Returns an array of Component objects
	//  */
	/*
	public function findByExampleField($value)
	{
	    return $this->createQueryBuilder('c')
		->andWhere('c.exampleField = :val')
		->setParameter('val', $value)
		->orderBy('c.id', 'ASC')
		->setMaxResults(10)
		->getQuery()
		->getResult()
	    ;
	}
	*/

	/*
	public function findOneBySomeField($value): ?Component
	{
	    return $this->createQueryBuilder('c')
		->andWhere('c.exampleField = :val')
		->setParameter('val', $value)
		->getQuery()
		->getOneOrNullResult()
	    ;
	}
	*/
}
