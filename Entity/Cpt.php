<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Table(indexes={@Index(name="cpt_number", columns={"cpt_number"})})
 * @ORM\Entity(repositoryClass="App\Repository\CptRepository")
 */
class Cpt
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $CptNumber;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ProductDescriptionCustomer;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Employee;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $YearUsage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Memo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Packaging;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $NumberInPackage;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $Margin;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $Date;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=3, nullable=true)
     */
    private $SalesPrice;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=3, nullable=true)
     */
    private $OriginalSalesPrice;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $OriginalMargin;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $CreationDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CptSurgeryType", inversedBy="cpts")
     */
    private $TypeOfSurgery;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="cpts")
     */
    private $Company;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CompanyContacts", inversedBy="Cpts")
     */
    private $CompanyContact;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CptQuote", inversedBy="cpts")
     */
    private $Quote;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CptBurden", inversedBy="cpts")
     */
    private $Burden;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CptStatus", inversedBy="cpts")
     */
    private $CptStatus;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CptComponenten", mappedBy="Cpt")
     * @ORM\OrderBy({"ComponentNumber" = "ASC"})
     */
    private $cptComponentens;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $EndUser;

     public function __construct()
    {
        $this->cptComponentens = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCptNumber(): ?string
    {
        return $this->CptNumber;
    }

    public function setCptNumber(string $CptNumber): self
    {
        $this->CptNumber = $CptNumber;

        return $this;
    }

    public function getProductDescriptionCustomer(): ?string
    {
        return $this->ProductDescriptionCustomer;
    }

    public function setProductDescriptionCustomer(string $ProductDescriptionCustomer): self
    {
        $this->ProductDescriptionCustomer = $ProductDescriptionCustomer;

        return $this;
    }

    public function getEmployee(): ?string
    {
        return $this->Employee;
    }

    public function setEmployee(string $Employee): self
    {
        $this->Employee = $Employee;

        return $this;
    }

    public function getYearUsage(): ?int
    {
        return $this->YearUsage;
    }

    public function setYearUsage(?int $YearUsage): self
    {
        $this->YearUsage = $YearUsage;

        return $this;
    }

    public function getMemo(): ?string
    {
        return $this->Memo;
    }

    public function setMemo(?string $Memo): self
    {
        $this->Memo = $Memo;

        return $this;
    }

    public function getPackaging(): ?string
    {
        return $this->Packaging;
    }

    public function setPackaging(?string $Packaging): self
    {
        $this->Packaging = $Packaging;

        return $this;
    }

    public function getNumberInPackage(): ?int
    {
        return $this->NumberInPackage;
    }

    public function setNumberInPackage(?int $NumberInPackage): self
    {
        $this->NumberInPackage = $NumberInPackage;

        return $this;
    }

    public function getMargin(): ?string
    {
        return $this->Margin;
    }

    public function setMargin(?string $Margin): self
    {
        $this->Margin = $Margin;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->Date;
    }

    public function setDate(?\DateTimeInterface $Date): self
    {
        $this->Date = $Date;

        return $this;
    }

    public function getSalesPrice(): ?string
    {
        return $this->SalesPrice;
    }

    public function setSalesPrice(?string $SalesPrice): self
    {
        $this->SalesPrice = $SalesPrice;

        return $this;
    }

    public function getOriginalSalesPrice(): ?string
    {
        return $this->OriginalSalesPrice;
    }

    public function setOriginalSalesPrice(?string $OriginalSalesPrice): self
    {
        $this->OriginalSalesPrice = $OriginalSalesPrice;

        return $this;
    }

    public function getOriginalMargin(): ?string
    {
        return $this->OriginalMargin;
    }

    public function setOriginalMargin(?string $OriginalMargin): self
    {
        $this->OriginalMargin = $OriginalMargin;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->CreationDate;
    }

    public function setCreationDate(?\DateTimeInterface $CreationDate): self
    {
        $this->CreationDate = $CreationDate;

        return $this;
    }

    public function getTypeOfSurgery(): ?CptSurgeryType
    {
        return $this->TypeOfSurgery;
    }

    public function setTypeOfSurgery(?CptSurgeryType $TypeOfSurgery): self
    {
        $this->TypeOfSurgery = $TypeOfSurgery;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->Company;
    }

    public function setCompany(?Company $Company): self
    {
        $this->Company = $Company;

        return $this;
    }

    public function getCompanyContact(): ?CompanyContacts
    {
        return $this->CompanyContact;
    }

    public function setCompanyContact(?CompanyContacts $CompanyContact): self
    {
        $this->CompanyContact = $CompanyContact;

        return $this;
    }

    public function getQuote(): ?CptQuote
    {
        return $this->Quote;
    }

    public function setQuote(?CptQuote $Quote): self
    {
        $this->Quote = $Quote;

        return $this;
    }

    public function getBurden(): ?CptBurden
    {
        return $this->Burden;
    }

    public function setBurden(?CptBurden $Burden): self
    {
        $this->Burden = $Burden;

        return $this;
    }

    public function getCptStatus(): ?CptStatus
    {
        return $this->CptStatus;
    }

    public function setCptStatus(?CptStatus $CptStatus): self
    {
        $this->CptStatus = $CptStatus;

        return $this;
    }

    /**
     * @return Collection|CptComponenten[]
     */
    public function getCptComponentens(): Collection
    {
        return $this->cptComponentens;
    }

    public function addCptComponenten(CptComponenten $cptComponenten): self
    {
        if (!$this->cptComponentens->contains($cptComponenten)) {
            $this->cptComponentens[] = $cptComponenten;
            $cptComponenten->setCpt($this);
        }

        return $this;
    }

    public function removeCptComponenten(CptComponenten $cptComponenten): self
    {
        if ($this->cptComponentens->contains($cptComponenten)) {
            $this->cptComponentens->removeElement($cptComponenten);
            // set the owning side to null (unless already changed)
            if ($cptComponenten->getCpt() === $this) {
                $cptComponenten->setCpt(null);
            }
        }

        return $this;
    }

    public function getEndUser(): ?string
    {
        return $this->EndUser;
    }

    public function setEndUser(?string $EndUser): self
    {
        $this->EndUser = $EndUser;

        return $this;
    }
}
