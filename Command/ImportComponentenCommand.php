<?php
/**
 * Created by PhpStorm.
 * User: Jeroen
 * Date: 27-8-2019
 * Time: 04:43
 */

namespace App\Command;

use App\Entity\Component;
use App\Entity\ComponentClassification;
use App\Entity\ComponentGroup;
use App\Entity\ComponentStatus;
use App\Entity\ComponentSupplier;
use App\Entity\Cpt;
use App\Repository\ComponentGroupRepository;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportComponentenCommand extends Command
{
	protected static $defaultName = 'medica:import-componenten';

	private $entityManager;

	private $container;

	private $aStatussen;

	private $aClasificaties;

	public function __construct(ContainerInterface $container)
	{
		parent::__construct();
		$this->container = $container;
	}

	protected function configure()
	{
		$this
			->setDescription("Import Componenten")
			->setHelp("This command allows you to import all Componenten")
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$output->writeln("Start import import-componenten");

		$InputPath = getcwd() . '/src/Command/ImportData/';

		$InputFile = "wincalc-basis.csv";

		$InputPathFile = $InputPath . $InputFile;

		if(!file_exists($InputPathFile)) {

			$LogData = "ERROR: LET OP! Bestand " . $InputPathFile . " bestaat niet";

			$output->writeln("<info>". $LogData . "</info>");

			exit;
		}

		$this->entityManager = $this->container->get('doctrine')->getManager();

		$time_start 	= microtime(true);

		$fp = file($InputPathFile);
		$AantalRegels =  count($fp);

		$handle = fopen($InputPathFile, "r");

		if (empty($handle) === false) {

			$teller = 0;

			while (($data = fgetcsv($handle, 999999, ";")) !== FALSE) {

				$teller++;

				if($teller < 2) {
				    continue;
				}

//				if(trim($data[0]) != '1301578') {
//					continue;
//				}

				if ($teller % 100 == 0) {

					$time_end 	= microtime(true);
					$time 		= sprintf("%.3f", $time_end - $time_start);

					$output->writeln("rows: " . $teller . "/" . $AantalRegels . ", time spend: ". $time);

					$time_start = microtime(true);
				}

				$aContent['component_nummer'] = $data[0];
				$aContent['component_nummer_leverancier'] = $data[1];
				$aContent['leverancier_id'] = $data[2];
				$aContent['naam_leverancier'] = $data[3];
				$aContent['omschrijving'] = $data[4];
				$aContent['omschrijving_nl'] = $data[5];
				$aContent['omschrijving_fr'] = $data[6];
				$aContent['omschrijving_ge'] = $data[7];
				$aContent['omschrijving_es'] = $data[8];
				$aContent['omschrijving_it'] = $data[9];
				$aContent['componentgroep'] = $data[10];
				$aContent['componentgroep_omschrijving'] = $data[11];
				$aContent['classificatie'] = $data[12];
				$aContent['inkoopprijs'] = $data[13];
				$aContent['normtijd'] = $data[14];
				$aContent['status_component'] = $data[15];
				$aContent['NL_BTW_CODE'] = $data[16];
				$aContent['NL_BTW_PERC'] = (float) $data[17];
				$aContent['BE_BTW_CODE'] = $data[18];
				$aContent['BE_BTW_PERC'] = (float) $data[19];

				if(!$this->existsInDb($aContent['component_nummer']))
				{
					$this->addComponent($aContent);
				}
				else {
					$this->updateComponent($aContent);
				}
			}

			$this->entityManager->flush();
		}

		$output->writeln("Einde import import-componenten");
	}

	private function existsInDb($component_nummer)
	{
		$result = $this->entityManager->getRepository(Component::class)->findBy(
			['ComponentNumber' => $component_nummer]
		);

		if($result) {

			return true;
		}
		return false;
	}

	private function addComponent($aContent)
	{
//		echo "Add component: " . $aContent['component_nummer'] . PHP_EOL;

		$oComponent = new Component();

		$Inkoopprijs =  $input = str_replace(',', '.', $aContent['inkoopprijs']);
		$Normtijd =  $input = str_replace(',', '.', $aContent['normtijd']);
		$BTW_perc_nl =  $input = str_replace(',', '.', $aContent['NL_BTW_PERC']);
		$BTW_per_be =  $input = str_replace(',', '.', $aContent['BE_BTW_PERC']);

		$oComponent->setComponentNumber($aContent['component_nummer']);
		$oComponent->setComponentNumberSupplier($aContent['component_nummer_leverancier']);
		$oComponent->setDescription($aContent['omschrijving']);
		$oComponent->setDescriptionNl($aContent['omschrijving_nl']);
		$oComponent->setDescriptionFr($aContent['omschrijving_fr']);
		$oComponent->setDescriptionDE($aContent['omschrijving_ge']);
		$oComponent->setDescriptionEs($aContent['omschrijving_es']);
		$oComponent->setDescriptionIt($aContent['omschrijving_it']);
		$oComponent->setNLVATCODE($aContent['NL_BTW_CODE']);
		$oComponent->setNLVATPERC($BTW_perc_nl);
		$oComponent->setBEVATCODE($aContent['BE_BTW_CODE']);
		$oComponent->setBEVATPERC($BTW_per_be);
		$oComponent->setPuchasePrice($Inkoopprijs);
		$oComponent->setNormTime($Normtijd);

		//		$oComponent->setImage($image);

		$oComponentStatus = $this->getComponentStatus($aContent['status_component']);
		if($oComponentStatus) {
			$oComponent->setStatus($oComponentStatus);
		}

		$oClassification = $this->getClassification($aContent['classificatie']);
		if($oClassification) {
			$oComponent->setClassification($oClassification);
		}

		$oComponentGroup = $this->getComponentGroup($aContent['componentgroep']);
		if($oComponentGroup) {
			$oComponent->setComponentGroup($oComponentGroup);
		}

		$ComponentLeverancier = $this->getSupplier($aContent['leverancier_id']);
		if($ComponentLeverancier) {
			$oComponent->setSupplier($ComponentLeverancier);
		}

		$this->entityManager->persist($oComponent);

		$this->entityManager->flush();

		$this->entityManager->clear();
	}

	private function updateComponent($aContent)
	{
//		echo "Update component: " . $aContent['component_nummer'] . PHP_EOL;

		$aComponent = $this->entityManager->getRepository(Component::class)->findBy(
			['ComponentNumber' => $aContent['component_nummer'] ]
		);

		$oComponent = $aComponent[0];

		$Inkoopprijs =  $input = str_replace(',', '.', $aContent['inkoopprijs']);
		$Normtijd =  $input = str_replace(',', '.', $aContent['normtijd']);
		$BTW_perc_nl =  $input = str_replace(',', '.', $aContent['NL_BTW_PERC']);
		$BTW_per_be =  $input = str_replace(',', '.', $aContent['BE_BTW_PERC']);

		$oComponent->setComponentNumber($aContent['component_nummer']);
		$oComponent->setComponentNumberSupplier($aContent['component_nummer_leverancier']);
		$oComponent->setDescription($aContent['omschrijving']);
		$oComponent->setDescriptionNl($aContent['omschrijving_nl']);
		$oComponent->setDescriptionFr($aContent['omschrijving_fr']);
		$oComponent->setDescriptionDE($aContent['omschrijving_ge']);
		$oComponent->setDescriptionEs($aContent['omschrijving_es']);
		$oComponent->setDescriptionIt($aContent['omschrijving_it']);
		$oComponent->setNLVATCODE($aContent['NL_BTW_CODE']);
		$oComponent->setNLVATPERC($BTW_perc_nl);
		$oComponent->setBEVATCODE($aContent['BE_BTW_CODE']);
		$oComponent->setBEVATPERC($BTW_per_be);
		$oComponent->setPuchasePrice($Inkoopprijs);
		$oComponent->setNormTime($Normtijd);

		$oComponentStatus = $this->getComponentStatus($aContent['status_component']);
		if($oComponentStatus) {
			$oComponent->setStatus($oComponentStatus);
		}

		$oClassification = $this->getClassification($aContent['classificatie']);
		if($oClassification) {
			$oComponent->setClassification($oClassification);
		}

		$oComponentGroup = $this->getComponentGroup($aContent['componentgroep']);
		if($oComponentGroup) {
			$oComponent->setComponentGroup($oComponentGroup);
		}

		$ComponentLeverancier = $this->getSupplier($aContent['leverancier_id']);
		if($ComponentLeverancier) {
			$oComponent->setSupplier($ComponentLeverancier);
		}

		$this->entityManager->persist($oComponent);

		$this->entityManager->flush();

		$this->entityManager->clear();

		return;
	}


	private function getClassification($classificatie)
	{
		$aClassification = $this->entityManager->getRepository(ComponentClassification::class)->findBy(
			['Description' => $classificatie]
		);

		if (sizeof($aClassification) > 0) {
			return $aClassification[0];
		}

		return;
	}

	private function getSupplier($leverancier_id)
	{
		$aComponentSupplier = $this->entityManager->getRepository(ComponentSupplier::class)->findBy(
			['SupplierId' => $leverancier_id]
		);

		if (sizeof($aComponentSupplier) > 0) {
			return $aComponentSupplier[0];
		}

		return;
	}

	private function getComponentGroup($component_group_id)
	{
		$aComponentGroup = $this->entityManager->getRepository(ComponentGroup::class)->findBy(
			['ComponentGroupId' => $component_group_id]
		);

		if (sizeof($aComponentGroup) > 0) {
			return $aComponentGroup[0];
		}

		return;
	}

	private function getStatusFromFile($InputPathFile)
	{
		$handle = fopen($InputPathFile, "r");

		if (empty($handle) === false) {

			while (($data = fgetcsv($handle, 999999, "|")) !== FALSE) {

				$this->aStatussen[$data[1]] = $data[0];
			}
		}

		return;
	}

	private function getComponentStatus($status)
	{
		$oComponentStatus = $this->entityManager->getRepository(ComponentStatus::class)->findBy(
			['Description' => $status]
		);

		if(!$oComponentStatus) {

			$oComponentStatus = new ComponentStatus();
			$oComponentStatus->setDescription($status);

			$this->entityManager->persist($oComponentStatus);
			$this->entityManager->flush();

			return $oComponentStatus;

		}
		return $oComponentStatus[0];
	}

	private function getClassificatiesFromFile($InputPathFile)
	{
		$handle = fopen($InputPathFile, "r");

		if (empty($handle) === false) {

			while (($data = fgetcsv($handle, 999999, "|")) !== FALSE) {

				$this->aClasaificaties[$data[0]] = $data[1];
			}
		}

		return;
	}
}