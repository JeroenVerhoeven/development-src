<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Table(indexes={@Index(name="component_number", columns={"component_number"})})
 * @ORM\Entity(repositoryClass="App\Repository\ComponentRepository")
 */
class Component
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ComponentNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ComponentNumberSupplier;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Description_NL;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Description_FR;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Description_DE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Description_ES;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Description_IT;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5, nullable=true)
     */
    private $PuchasePrice;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=4, nullable=true)
     */
    private $NormTime;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $NL_VAT_CODE;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $NL_VAT_PERC;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $BE_VAT_CODE;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $BE_VAT_PERC;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ComponentSupplier", inversedBy="components")
     */
    private $Supplier;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ComponentGroup", inversedBy="components")
     */
    private $ComponentGroup;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ComponentClassification", inversedBy="components")
     */
    private $Classification;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ComponentStatus", inversedBy="components")
     */
    private $Status;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CptComponenten", mappedBy="Component")
     */
    private $cptComponentens;

    public function __construct()
    {
        $this->cptComponentens = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComponentNumber(): ?string
    {
        return $this->ComponentNumber;
    }

    public function setComponentNumber(string $ComponentNumber): self
    {
        $this->ComponentNumber = $ComponentNumber;

        return $this;
    }

    public function getComponentNumberSupplier(): ?string
    {
        return $this->ComponentNumberSupplier;
    }

    public function setComponentNumberSupplier(?string $ComponentNumberSupplier): self
    {
        $this->ComponentNumberSupplier = $ComponentNumberSupplier;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getDescriptionNL(): ?string
    {
        return $this->Description_NL;
    }

    public function setDescriptionNL(?string $Description_NL): self
    {
        $this->Description_NL = $Description_NL;

        return $this;
    }

    public function getDescriptionFR(): ?string
    {
        return $this->Description_FR;
    }

    public function setDescriptionFR(?string $Description_FR): self
    {
        $this->Description_FR = $Description_FR;

        return $this;
    }

    public function getDescriptionDE(): ?string
    {
        return $this->Description_DE;
    }

    public function setDescriptionDE(?string $Description_DE): self
    {
        $this->Description_DE = $Description_DE;

        return $this;
    }

    public function getDescriptionES(): ?string
    {
        return $this->Description_ES;
    }

    public function setDescriptionES(?string $Description_ES): self
    {
        $this->Description_ES = $Description_ES;

        return $this;
    }

    public function getDescriptionIT(): ?string
    {
        return $this->Description_IT;
    }

    public function setDescriptionIT(?string $Description_IT): self
    {
        $this->Description_IT = $Description_IT;

        return $this;
    }

    public function getPuchasePrice(): ?string
    {
        return $this->PuchasePrice;
    }

    public function setPuchasePrice(?string $PuchasePrice): self
    {
        $this->PuchasePrice = $PuchasePrice;

        return $this;
    }

    public function getNormTime(): ?string
    {
        return $this->NormTime;
    }

    public function setNormTime(?string $NormTime): self
    {
        $this->NormTime = $NormTime;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->Image;
    }

    public function setImage(?string $Image): self
    {
        $this->Image = $Image;

        return $this;
    }

    public function getNLVATCODE(): ?string
    {
        return $this->NL_VAT_CODE;
    }

    public function setNLVATCODE(?string $NL_VAT_CODE): self
    {
        $this->NL_VAT_CODE = $NL_VAT_CODE;

        return $this;
    }

    public function getNLVATPERC(): ?string
    {
        return $this->NL_VAT_PERC;
    }

    public function setNLVATPERC(?string $NL_VAT_PERC): self
    {
        $this->NL_VAT_PERC = $NL_VAT_PERC;

        return $this;
    }

    public function getBEVATCODE(): ?string
    {
        return $this->BE_VAT_CODE;
    }

    public function setBEVATCODE(?string $BE_VAT_CODE): self
    {
        $this->BE_VAT_CODE = $BE_VAT_CODE;

        return $this;
    }

    public function getBEVATPERC(): ?string
    {
        return $this->BE_VAT_PERC;
    }

    public function setBEVATPERC(?string $BE_VAT_PERC): self
    {
        $this->BE_VAT_PERC = $BE_VAT_PERC;

        return $this;
    }

    public function getSupplier(): ?ComponentSupplier
    {
        return $this->Supplier;
    }

    public function setSupplier(?ComponentSupplier $Supplier): self
    {
        $this->Supplier = $Supplier;

        return $this;
    }

    public function getComponentGroup(): ?ComponentGroup
    {
        return $this->ComponentGroup;
    }

    public function setComponentGroup(?ComponentGroup $ComponentGroup): self
    {
        $this->ComponentGroup = $ComponentGroup;

        return $this;
    }

    public function getClassification(): ?ComponentClassification
    {
        return $this->Classification;
    }

    public function setClassification(?ComponentClassification $Classification): self
    {
        $this->Classification = $Classification;

        return $this;
    }

    public function getStatus(): ?ComponentStatus
    {
        return $this->Status;
    }

    public function setStatus(?ComponentStatus $Status): self
    {
        $this->Status = $Status;

        return $this;
    }

    /**
     * @return Collection|CptComponenten[]
     */
    public function getCptComponentens(): Collection
    {
        return $this->cptComponentens;
    }

    public function addCptComponenten(CptComponenten $cptComponenten): self
    {
        if (!$this->cptComponentens->contains($cptComponenten)) {
            $this->cptComponentens[] = $cptComponenten;
            $cptComponenten->setComponent($this);
        }

        return $this;
    }

    public function removeCptComponenten(CptComponenten $cptComponenten): self
    {
        if ($this->cptComponentens->contains($cptComponenten)) {
            $this->cptComponentens->removeElement($cptComponenten);
            // set the owning side to null (unless already changed)
            if ($cptComponenten->getComponent() === $this) {
                $cptComponenten->setComponent(null);
            }
        }

        return $this;
    }
}
