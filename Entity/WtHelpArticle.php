<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WtHelpArticleRepository")
 */
class WtHelpArticle
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ArticleNumber;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Description;

    /**
     * @ORM\Column(type="integer")
     */
    private $PackingInit;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $NameSupplier;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\WtClassification", inversedBy="wtHelpArticles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Classification;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\WtProductLine", inversedBy="wtHelpArticles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Productline;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5)
     */
    private $PurchasePrice;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=1)
     */
    private $MinMargin;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=1)
     */
    private $PreferredMargin;

    /**
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $MinimalPrice;

    /**
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $PreferredPrice;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ArticleNumberSupplier;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ArticleType;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $VAT;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArticleNumber(): ?string
    {
        return $this->ArticleNumber;
    }

    public function setArticleNumber(string $ArticleNumber): self
    {
        $this->ArticleNumber = $ArticleNumber;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getPackingInit(): ?int
    {
        return $this->PackingInit;
    }

    public function setPackingInit(int $PackingInit): self
    {
        $this->PackingInit = $PackingInit;

        return $this;
    }

    public function getNameSupplier(): ?string
    {
        return $this->NameSupplier;
    }

    public function setNameSupplier(string $NameSupplier): self
    {
        $this->NameSupplier = $NameSupplier;

        return $this;
    }

    public function getClassification(): ?WtClassification
    {
        return $this->Classification;
    }

    public function setClassification(?WtClassification $Classification): self
    {
        $this->Classification = $Classification;

        return $this;
    }

    public function getProductline(): ?WtProductLine
    {
        return $this->Productline;
    }

    public function setProductline(?WtProductLine $Productline): self
    {
        $this->Productline = $Productline;

        return $this;
    }

    public function getPurchasePrice(): ?string
    {
        return $this->PurchasePrice;
    }

    public function setPurchasePrice(string $PurchasePrice): self
    {
        $this->PurchasePrice = $PurchasePrice;

        return $this;
    }

    public function getMinMargin(): ?string
    {
        return $this->MinMargin;
    }

    public function setMinMargin(string $MinMargin): self
    {
        $this->MinMargin = $MinMargin;

        return $this;
    }

    public function getPreferredMargin(): ?string
    {
        return $this->PreferredMargin;
    }

    public function setPreferredMargin(string $PreferredMargin): self
    {
        $this->PreferredMargin = $PreferredMargin;

        return $this;
    }

    public function getMinimalPrice(): ?string
    {
        return $this->MinimalPrice;
    }

    public function setMinimalPrice(string $MinimalPrice): self
    {
        $this->MinimalPrice = $MinimalPrice;

        return $this;
    }

    public function getPreferredPrice(): ?string
    {
        return $this->PreferredPrice;
    }

    public function setPreferredPrice(string $PreferredPrice): self
    {
        $this->PreferredPrice = $PreferredPrice;

        return $this;
    }

    public function getArticleNumberSupplier(): ?string
    {
        return $this->ArticleNumberSupplier;
    }

    public function setArticleNumberSupplier(string $ArticleNumberSupplier): self
    {
        $this->ArticleNumberSupplier = $ArticleNumberSupplier;

        return $this;
    }

    public function getArticleType(): ?string
    {
        return $this->ArticleType;
    }

    public function setArticleType(string $ArticleType): self
    {
        $this->ArticleType = $ArticleType;

        return $this;
    }

    public function getVAT(): ?string
    {
        return $this->VAT;
    }

    public function setVAT(string $VAT): self
    {
        $this->VAT = $VAT;

        return $this;
    }
}
