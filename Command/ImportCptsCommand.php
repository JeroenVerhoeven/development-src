<?php
/**
 * Created by PhpStorm.
 * User: Jeroen
 * Date: 27-8-2019
 * Time: 04:43
 */

namespace App\Command;

use App\Entity\Company;
use App\Entity\CompanyContacts;
use App\Entity\Cpt;
use App\Entity\CptBurden;
use App\Entity\CptQuote;
use App\Entity\CptSurgeryType;
use App\Entity\CptStatus;
use App\Entity\Component;
//use App\Repository\ComponentGroupRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportCptsCommand extends Command
{
	protected static $defaultName = 'medica:import-cpts';

	private $entityManager;

	private $container;

	public function __construct(ContainerInterface $container)
	{
		parent::__construct();
		$this->container = $container;
	}

	protected function configure()
	{
		$this
			->setDescription("Import Cpts")
			->setHelp("This command allows you to import all Cpts")
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$output->writeln("Start import import-cpts");

		$InputPath = getcwd() . '/src/Command/ImportData/';

		$InputFile = "tbl_cpt.csv";

		$InputPathFile = $InputPath . $InputFile;

		if(!file_exists($InputPathFile)) {

			$LogData = "ERROR: LET OP! Bestand " . $InputPathFile . " bestaat niet";

			$output->writeln("<info>". $LogData . "</info>");

			exit;
		}

		$this->entityManager = $this->container->get('doctrine')->getManager();

		$time_start 	= microtime(true);

		$fp = file($InputPathFile);
		$AantalRegels =  count($fp);

		$handle = fopen($InputPathFile, "r");

		if (empty($handle) === false) {

			$teller = 0;

			while (($data = fgetcsv($handle, 999999, "|")) !== FALSE) {

				$teller++;

//				if($teller < 30700) {
//					continue;
//				}

				if ($teller % 100 == 0) {

					$time_end 	= microtime(true);
					$time 		= sprintf("%.3f", $time_end - $time_start);

					$output->writeln("rows: " . $teller . "/" . $AantalRegels . ", time spend: ". $time);

					$time_start = microtime(true);
				}

				$aContent['CptNumber'] = $data[0];
				$aContent['IdCustomer'] = $data[1];
				$aContent['IdContact'] = $data[2];
				$aContent['EndUser'] = $data[3];
				$aContent['EndUserPlace'] = $data[4];
				$aContent['Employee'] = $data[5];
				$aContent['YearUsage'] = $data[6];
				$aContent['TypeOfSurgery'] = $data[7];
				$aContent['ProductDescriptionCustomer'] = $data[8];
				$aContent['Quote'] = $data[9];
				$aContent['Memo'] = $data[10];
				$aContent['Packaging'] = $data[11];
				$aContent['NumberInPackage'] = $data[12];
				$aContent['Margin'] = $data[13];
				$aContent['Burden'] = $data[14];
				$aContent['CptStatus'] = $data[15];
				$aContent['Date'] = $data[16];
				$aContent['SalesPrice'] = $data[17];
				$aContent['OriginalSalesPrice'] = $data[18];
				$aContent['OriginalMargin'] = $data[19];
				$aContent['CreationDate'] = $data[20];

//				echo $aContent['CptNumber'] . PHP_EOL;

				if(!$this->existsInDb($aContent['CptNumber']))
				{
					$this->addCpt($aContent);
				}

				$this->entityManager->flush();
				$this->entityManager->clear();
			}
		}

		$output->writeln("Einde import import-cpts");
	}

	private function addCpt($aContent)
	{
		$oCpt = new Cpt();

		$margin =  $input = str_replace(',', '.', $aContent['Margin']);
		$salesprice =  $input = str_replace(',', '.', $aContent['SalesPrice']);
		$originalsalesprice =  $input = str_replace(',', '.', $aContent['OriginalSalesPrice']);
		$originalMargin =  $input = str_replace(',', '.', $aContent['OriginalMargin']);

		$oCpt->setCptNumber($aContent['CptNumber']);
		$oCpt->setEmployee($aContent['Employee']);
		$oCpt->setYearUsage($aContent['YearUsage']);
		$oCpt->setProductDescriptionCustomer($aContent['ProductDescriptionCustomer']);
		$oCpt->setMemo($aContent['Memo']);
		$oCpt->setPackaging($aContent['Packaging']);
		$oCpt->setMargin($margin);
		$oCpt->setOriginalSalesPrice($originalsalesprice);
		$oCpt->setOriginalMargin($originalMargin);
		$oCpt->setSalesPrice($salesprice);
		$oCpt->setEndUser($aContent['EndUser']);

		if($aContent['NumberInPackage']) {
			$oCpt->setNumberInPackage($aContent['NumberInPackage']);
		}

		$oBurden = $this->getburden($aContent['Burden']);
		if($oBurden) {
			$oCpt->setBurden($oBurden);
		}

		$oQuote = $this->getQuote($aContent['Quote']);
		if($oQuote) {
			$oCpt->setQuote($oQuote);
		}

		$oTypeOfSurgery = $this->getTypeOfSurgery($aContent['TypeOfSurgery']);
		if($oTypeOfSurgery) {
			$oCpt->setTypeOfSurgery($oTypeOfSurgery);
		}

		$oCptStatus = $this->getCptStatus($aContent['CptStatus']);
		if($oCptStatus) {
			$oCpt->setCptStatus($oCptStatus);
		}

		$oCustomer = $this->getCompany($aContent['IdCustomer']);
		if($oCustomer) {
			$oCpt->setCompany($oCustomer);
		}

		$oContact = $this->getContact($aContent['IdContact']);
		if($oContact){
			$oCpt->setCompanyContact($oContact);
		}

		$this->entityManager->persist($oCpt);
	}

	private function getTypeOfSurgery($typeSurgeryId)
	{
		$oTypeOfSurgery = $this->entityManager->getRepository(CptSurgeryType::class)->findBy(
			['TosId' => $typeSurgeryId]
		);

		if($oTypeOfSurgery) {
			return $oTypeOfSurgery[0];
		}
		return;
	}

	private function getCptStatus($status)
	{
		$oCptStatus = $this->entityManager->getRepository(CptStatus::class)->findBy(
			['id' => $status]
		);

		if($oCptStatus) {
			return $oCptStatus[0];
		}

		return;
	}

	private function existsInDb($CptNumber)
	{
		$result = $this->entityManager->getRepository(Cpt::class)->findBy(
			['CptNumber' => $CptNumber]
		);

		if($result) {

			return true;
		}
		return false;
	}

	private function getCompany($IdCustomer)
	{
		$oCompany = $this->entityManager->getRepository(Company::class)->findBy(
			['CompanyId' => $IdCustomer]
		);

		if($oCompany) {
			return $oCompany[0];
		}
		return;
	}

	private function getContact($IdContact)
	{
		$oContact = $this->entityManager->getRepository(CompanyContacts::class)->findBy(
			['ContactId' => $IdContact]
		);

		if($oContact) {
			return $oContact[0];
		}
		return;
	}

	private function getburden($Burden)
	{
		$oBurden = $this->entityManager->getRepository(CptBurden::class)->findBy(
			['BurdenId' => $Burden]
		);

		if($oBurden) {
			return $oBurden[0];
		}
		return;
	}

	private function getQuote($Quote)
	{
		$oQuote = $this->entityManager->getRepository(CptQuote::class)->findBy(
			['QuoteId' => $Quote]
		);

		if($oQuote) {
			return $oQuote[0];
		}
		return;
	}
}