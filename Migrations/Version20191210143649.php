<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191210143649 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE cpt_templates (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, document_name VARCHAR(255) NOT NULL, document_type VARCHAR(255) NOT NULL, language VARCHAR(2) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wt_quotation (id INT AUTO_INCREMENT NOT NULL, customer_id INT NOT NULL, company_contacts_id INT NOT NULL, status_id INT NOT NULL, quotation_number VARCHAR(255) NOT NULL, version VARCHAR(2) DEFAULT NULL, date DATE NOT NULL, employee VARCHAR(255) NOT NULL, memo1 LONGTEXT DEFAULT NULL, memo2 LONGTEXT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, INDEX IDX_8CA0CBB79395C3F3 (customer_id), INDEX IDX_8CA0CBB7684E0C16 (company_contacts_id), INDEX IDX_8CA0CBB76BF700BD (status_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE component_classification (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cpt_surgery_type (id INT AUTO_INCREMENT NOT NULL, tos_id INT NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE component_type (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wt_quotation_line (id INT AUTO_INCREMENT NOT NULL, quotation_id INT NOT NULL, classification_id INT NOT NULL, product_line_id INT NOT NULL, article_number VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, purchase_price NUMERIC(10, 5) NOT NULL, min_margin NUMERIC(5, 1) NOT NULL, preferred_margin NUMERIC(5, 1) NOT NULL, preferred_price NUMERIC(7, 2) NOT NULL, min_price NUMERIC(7, 2) NOT NULL, selling_unit INT NOT NULL, quantity INT NOT NULL, selling_price NUMERIC(7, 2) NOT NULL, name_supplier VARCHAR(255) NOT NULL, article_number_supplier VARCHAR(255) DEFAULT NULL, vat NUMERIC(5, 2) DEFAULT NULL, article_type VARCHAR(255) DEFAULT NULL, INDEX IDX_C1D4F3C9B4EA4E60 (quotation_id), INDEX IDX_C1D4F3C92A86559F (classification_id), INDEX IDX_C1D4F3C99CA26EF2 (product_line_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE security_user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_52825A88E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE component (id INT AUTO_INCREMENT NOT NULL, supplier_id INT DEFAULT NULL, component_group_id INT DEFAULT NULL, classification_id INT DEFAULT NULL, status_id INT DEFAULT NULL, component_number VARCHAR(255) NOT NULL, component_number_supplier VARCHAR(255) DEFAULT NULL, description VARCHAR(255) NOT NULL, description_nl VARCHAR(255) DEFAULT NULL, description_fr VARCHAR(255) DEFAULT NULL, description_de VARCHAR(255) DEFAULT NULL, description_es VARCHAR(255) DEFAULT NULL, description_it VARCHAR(255) DEFAULT NULL, puchase_price NUMERIC(10, 5) DEFAULT NULL, norm_time NUMERIC(10, 4) DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, nl_vat_code VARCHAR(255) DEFAULT NULL, nl_vat_perc NUMERIC(10, 2) DEFAULT NULL, be_vat_code VARCHAR(255) DEFAULT NULL, be_vat_perc NUMERIC(10, 2) DEFAULT NULL, INDEX IDX_49FEA1572ADD6D8C (supplier_id), INDEX IDX_49FEA157BC564DAA (component_group_id), INDEX IDX_49FEA1572A86559F (classification_id), INDEX IDX_49FEA1576BF700BD (status_id), INDEX component_number (component_number), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wt_status (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cpt_quote (id INT AUTO_INCREMENT NOT NULL, quote_id INT NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cpt (id INT AUTO_INCREMENT NOT NULL, type_of_surgery_id INT DEFAULT NULL, company_id INT DEFAULT NULL, company_contact_id INT DEFAULT NULL, quote_id INT DEFAULT NULL, burden_id INT DEFAULT NULL, cpt_status_id INT DEFAULT NULL, cpt_number VARCHAR(255) NOT NULL, product_description_customer VARCHAR(255) NOT NULL, employee VARCHAR(255) NOT NULL, year_usage INT DEFAULT NULL, memo VARCHAR(255) DEFAULT NULL, packaging VARCHAR(255) DEFAULT NULL, number_in_package INT DEFAULT NULL, margin NUMERIC(10, 2) DEFAULT NULL, date DATETIME DEFAULT NULL, sales_price NUMERIC(20, 3) DEFAULT NULL, original_sales_price NUMERIC(20, 3) DEFAULT NULL, original_margin NUMERIC(10, 2) DEFAULT NULL, creation_date DATETIME DEFAULT NULL, end_user VARCHAR(255) DEFAULT NULL, INDEX IDX_CD8760B829019C23 (type_of_surgery_id), INDEX IDX_CD8760B8979B1AD6 (company_id), INDEX IDX_CD8760B85A2FCCDC (company_contact_id), INDEX IDX_CD8760B8DB805178 (quote_id), INDEX IDX_CD8760B88B95CA19 (burden_id), INDEX IDX_CD8760B8131D9941 (cpt_status_id), INDEX cpt_number (cpt_number), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cpt_status (id INT AUTO_INCREMENT NOT NULL, cpt_status_id INT NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wt_product_line (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) NOT NULL, min_margin NUMERIC(5, 1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wt_counters (id INT AUTO_INCREMENT NOT NULL, max_quotation_number INT NOT NULL, max_article INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE component_status (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company (id INT AUTO_INCREMENT NOT NULL, company_id INT NOT NULL, company_name VARCHAR(255) NOT NULL, company_street VARCHAR(255) NOT NULL, copmpany_zipcode VARCHAR(255) DEFAULT NULL, company_city VARCHAR(255) NOT NULL, company_phone VARCHAR(255) NOT NULL, INDEX company_id (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE component_supplier (id INT AUTO_INCREMENT NOT NULL, supplier_id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cpt_componenten (id INT AUTO_INCREMENT NOT NULL, cpt_id INT NOT NULL, cpt_component_status_id INT DEFAULT NULL, component_group_id INT DEFAULT NULL, component_classification_id INT DEFAULT NULL, status_id INT DEFAULT NULL, component_type_id INT DEFAULT NULL, component_id INT DEFAULT NULL, component_supplier_id INT DEFAULT NULL, cpt_number VARCHAR(255) NOT NULL, component_number VARCHAR(255) NOT NULL, quantity INT NOT NULL, purchase_price NUMERIC(10, 5) DEFAULT NULL, sample_supplied VARCHAR(1) DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, norm_time NUMERIC(10, 4) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, component_number_supplier VARCHAR(255) DEFAULT NULL, INDEX IDX_96A82DEABB2D1771 (cpt_id), INDEX IDX_96A82DEA14DFA37B (cpt_component_status_id), INDEX IDX_96A82DEABC564DAA (component_group_id), INDEX IDX_96A82DEA9F3A3366 (component_classification_id), INDEX IDX_96A82DEA6BF700BD (status_id), INDEX IDX_96A82DEA75400DBA (component_type_id), INDEX IDX_96A82DEAE2ABAFFF (component_id), INDEX IDX_96A82DEA70C4000F (component_supplier_id), INDEX cpt_Component_number (cpt_number, component_number), INDEX component_number (component_number), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE component_group (id INT AUTO_INCREMENT NOT NULL, component_group_id INT DEFAULT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wt_classification (id INT AUTO_INCREMENT NOT NULL, classification_id VARCHAR(2) NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wt_article (id INT AUTO_INCREMENT NOT NULL, classification_id INT NOT NULL, productline_id INT NOT NULL, article_number VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, packing_unit INT NOT NULL, name_supplier VARCHAR(255) NOT NULL, purchase_price NUMERIC(10, 5) NOT NULL, min_margin NUMERIC(5, 1) NOT NULL, preferred_margin NUMERIC(5, 1) NOT NULL, minimal_price NUMERIC(7, 2) NOT NULL, preferred_price NUMERIC(7, 2) NOT NULL, article_number_supplier VARCHAR(255) NOT NULL, article_type VARCHAR(255) NOT NULL, vat NUMERIC(5, 2) NOT NULL, INDEX IDX_4900543C2A86559F (classification_id), INDEX IDX_4900543CD634D365 (productline_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company_contacts (id INT AUTO_INCREMENT NOT NULL, company_id INT NOT NULL, contact_id INT NOT NULL, contact_name VARCHAR(255) NOT NULL, contact_attn VARCHAR(255) NOT NULL, contact_job_description VARCHAR(255) DEFAULT NULL, INDEX IDX_2BD7001E979B1AD6 (company_id), INDEX contact_id (contact_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE max_cpt_num (id INT AUTO_INCREMENT NOT NULL, max_cpt_num INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wt_help_article (id INT AUTO_INCREMENT NOT NULL, classification_id INT NOT NULL, productline_id INT NOT NULL, article_number VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, packing_init INT NOT NULL, name_supplier VARCHAR(255) NOT NULL, purchase_price NUMERIC(10, 5) NOT NULL, min_margin NUMERIC(5, 1) NOT NULL, preferred_margin NUMERIC(5, 1) NOT NULL, minimal_price NUMERIC(7, 2) NOT NULL, preferred_price NUMERIC(7, 2) NOT NULL, article_number_supplier VARCHAR(255) NOT NULL, article_type VARCHAR(255) NOT NULL, vat NUMERIC(5, 2) NOT NULL, INDEX IDX_B0423F9E2A86559F (classification_id), INDEX IDX_B0423F9ED634D365 (productline_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cpt_temp_componenten (id INT AUTO_INCREMENT NOT NULL, classification_id INT DEFAULT NULL, component_type_id INT DEFAULT NULL, status_id INT DEFAULT NULL, supplier_component_number VARCHAR(255) NOT NULL, supplier VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, sample_supplied TINYINT(1) DEFAULT NULL, purchase_price NUMERIC(15, 5) DEFAULT NULL, norm_time NUMERIC(15, 4) DEFAULT NULL, INDEX IDX_C0C428BE2A86559F (classification_id), INDEX IDX_C0C428BE75400DBA (component_type_id), INDEX IDX_C0C428BE6BF700BD (status_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cpt_burden (id INT AUTO_INCREMENT NOT NULL, burden_id INT DEFAULT NULL, description VARCHAR(255) NOT NULL, rate NUMERIC(10, 2) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE wt_quotation ADD CONSTRAINT FK_8CA0CBB79395C3F3 FOREIGN KEY (customer_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE wt_quotation ADD CONSTRAINT FK_8CA0CBB7684E0C16 FOREIGN KEY (company_contacts_id) REFERENCES company_contacts (id)');
        $this->addSql('ALTER TABLE wt_quotation ADD CONSTRAINT FK_8CA0CBB76BF700BD FOREIGN KEY (status_id) REFERENCES wt_status (id)');
        $this->addSql('ALTER TABLE wt_quotation_line ADD CONSTRAINT FK_C1D4F3C9B4EA4E60 FOREIGN KEY (quotation_id) REFERENCES wt_quotation (id)');
        $this->addSql('ALTER TABLE wt_quotation_line ADD CONSTRAINT FK_C1D4F3C92A86559F FOREIGN KEY (classification_id) REFERENCES wt_classification (id)');
        $this->addSql('ALTER TABLE wt_quotation_line ADD CONSTRAINT FK_C1D4F3C99CA26EF2 FOREIGN KEY (product_line_id) REFERENCES wt_product_line (id)');
        $this->addSql('ALTER TABLE component ADD CONSTRAINT FK_49FEA1572ADD6D8C FOREIGN KEY (supplier_id) REFERENCES component_supplier (id)');
        $this->addSql('ALTER TABLE component ADD CONSTRAINT FK_49FEA157BC564DAA FOREIGN KEY (component_group_id) REFERENCES component_group (id)');
        $this->addSql('ALTER TABLE component ADD CONSTRAINT FK_49FEA1572A86559F FOREIGN KEY (classification_id) REFERENCES component_classification (id)');
        $this->addSql('ALTER TABLE component ADD CONSTRAINT FK_49FEA1576BF700BD FOREIGN KEY (status_id) REFERENCES component_status (id)');
        $this->addSql('ALTER TABLE cpt ADD CONSTRAINT FK_CD8760B829019C23 FOREIGN KEY (type_of_surgery_id) REFERENCES cpt_surgery_type (id)');
        $this->addSql('ALTER TABLE cpt ADD CONSTRAINT FK_CD8760B8979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE cpt ADD CONSTRAINT FK_CD8760B85A2FCCDC FOREIGN KEY (company_contact_id) REFERENCES company_contacts (id)');
        $this->addSql('ALTER TABLE cpt ADD CONSTRAINT FK_CD8760B8DB805178 FOREIGN KEY (quote_id) REFERENCES cpt_quote (id)');
        $this->addSql('ALTER TABLE cpt ADD CONSTRAINT FK_CD8760B88B95CA19 FOREIGN KEY (burden_id) REFERENCES cpt_burden (id)');
        $this->addSql('ALTER TABLE cpt ADD CONSTRAINT FK_CD8760B8131D9941 FOREIGN KEY (cpt_status_id) REFERENCES cpt_status (id)');
        $this->addSql('ALTER TABLE cpt_componenten ADD CONSTRAINT FK_96A82DEABB2D1771 FOREIGN KEY (cpt_id) REFERENCES cpt (id)');
        $this->addSql('ALTER TABLE cpt_componenten ADD CONSTRAINT FK_96A82DEA14DFA37B FOREIGN KEY (cpt_component_status_id) REFERENCES component_status (id)');
        $this->addSql('ALTER TABLE cpt_componenten ADD CONSTRAINT FK_96A82DEABC564DAA FOREIGN KEY (component_group_id) REFERENCES component_group (id)');
        $this->addSql('ALTER TABLE cpt_componenten ADD CONSTRAINT FK_96A82DEA9F3A3366 FOREIGN KEY (component_classification_id) REFERENCES component_classification (id)');
        $this->addSql('ALTER TABLE cpt_componenten ADD CONSTRAINT FK_96A82DEA6BF700BD FOREIGN KEY (status_id) REFERENCES component_status (id)');
        $this->addSql('ALTER TABLE cpt_componenten ADD CONSTRAINT FK_96A82DEA75400DBA FOREIGN KEY (component_type_id) REFERENCES component_type (id)');
        $this->addSql('ALTER TABLE cpt_componenten ADD CONSTRAINT FK_96A82DEAE2ABAFFF FOREIGN KEY (component_id) REFERENCES component (id)');
        $this->addSql('ALTER TABLE cpt_componenten ADD CONSTRAINT FK_96A82DEA70C4000F FOREIGN KEY (component_supplier_id) REFERENCES component_supplier (id)');
        $this->addSql('ALTER TABLE wt_article ADD CONSTRAINT FK_4900543C2A86559F FOREIGN KEY (classification_id) REFERENCES wt_classification (id)');
        $this->addSql('ALTER TABLE wt_article ADD CONSTRAINT FK_4900543CD634D365 FOREIGN KEY (productline_id) REFERENCES wt_product_line (id)');
        $this->addSql('ALTER TABLE company_contacts ADD CONSTRAINT FK_2BD7001E979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE wt_help_article ADD CONSTRAINT FK_B0423F9E2A86559F FOREIGN KEY (classification_id) REFERENCES wt_classification (id)');
        $this->addSql('ALTER TABLE wt_help_article ADD CONSTRAINT FK_B0423F9ED634D365 FOREIGN KEY (productline_id) REFERENCES wt_product_line (id)');
        $this->addSql('ALTER TABLE cpt_temp_componenten ADD CONSTRAINT FK_C0C428BE2A86559F FOREIGN KEY (classification_id) REFERENCES component_classification (id)');
        $this->addSql('ALTER TABLE cpt_temp_componenten ADD CONSTRAINT FK_C0C428BE75400DBA FOREIGN KEY (component_type_id) REFERENCES component_type (id)');
        $this->addSql('ALTER TABLE cpt_temp_componenten ADD CONSTRAINT FK_C0C428BE6BF700BD FOREIGN KEY (status_id) REFERENCES component_status (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE wt_quotation_line DROP FOREIGN KEY FK_C1D4F3C9B4EA4E60');
        $this->addSql('ALTER TABLE component DROP FOREIGN KEY FK_49FEA1572A86559F');
        $this->addSql('ALTER TABLE cpt_componenten DROP FOREIGN KEY FK_96A82DEA9F3A3366');
        $this->addSql('ALTER TABLE cpt_temp_componenten DROP FOREIGN KEY FK_C0C428BE2A86559F');
        $this->addSql('ALTER TABLE cpt DROP FOREIGN KEY FK_CD8760B829019C23');
        $this->addSql('ALTER TABLE cpt_componenten DROP FOREIGN KEY FK_96A82DEA75400DBA');
        $this->addSql('ALTER TABLE cpt_temp_componenten DROP FOREIGN KEY FK_C0C428BE75400DBA');
        $this->addSql('ALTER TABLE cpt_componenten DROP FOREIGN KEY FK_96A82DEAE2ABAFFF');
        $this->addSql('ALTER TABLE wt_quotation DROP FOREIGN KEY FK_8CA0CBB76BF700BD');
        $this->addSql('ALTER TABLE cpt DROP FOREIGN KEY FK_CD8760B8DB805178');
        $this->addSql('ALTER TABLE cpt_componenten DROP FOREIGN KEY FK_96A82DEABB2D1771');
        $this->addSql('ALTER TABLE cpt DROP FOREIGN KEY FK_CD8760B8131D9941');
        $this->addSql('ALTER TABLE wt_quotation_line DROP FOREIGN KEY FK_C1D4F3C99CA26EF2');
        $this->addSql('ALTER TABLE wt_article DROP FOREIGN KEY FK_4900543CD634D365');
        $this->addSql('ALTER TABLE wt_help_article DROP FOREIGN KEY FK_B0423F9ED634D365');
        $this->addSql('ALTER TABLE component DROP FOREIGN KEY FK_49FEA1576BF700BD');
        $this->addSql('ALTER TABLE cpt_componenten DROP FOREIGN KEY FK_96A82DEA14DFA37B');
        $this->addSql('ALTER TABLE cpt_componenten DROP FOREIGN KEY FK_96A82DEA6BF700BD');
        $this->addSql('ALTER TABLE cpt_temp_componenten DROP FOREIGN KEY FK_C0C428BE6BF700BD');
        $this->addSql('ALTER TABLE wt_quotation DROP FOREIGN KEY FK_8CA0CBB79395C3F3');
        $this->addSql('ALTER TABLE cpt DROP FOREIGN KEY FK_CD8760B8979B1AD6');
        $this->addSql('ALTER TABLE company_contacts DROP FOREIGN KEY FK_2BD7001E979B1AD6');
        $this->addSql('ALTER TABLE component DROP FOREIGN KEY FK_49FEA1572ADD6D8C');
        $this->addSql('ALTER TABLE cpt_componenten DROP FOREIGN KEY FK_96A82DEA70C4000F');
        $this->addSql('ALTER TABLE component DROP FOREIGN KEY FK_49FEA157BC564DAA');
        $this->addSql('ALTER TABLE cpt_componenten DROP FOREIGN KEY FK_96A82DEABC564DAA');
        $this->addSql('ALTER TABLE wt_quotation_line DROP FOREIGN KEY FK_C1D4F3C92A86559F');
        $this->addSql('ALTER TABLE wt_article DROP FOREIGN KEY FK_4900543C2A86559F');
        $this->addSql('ALTER TABLE wt_help_article DROP FOREIGN KEY FK_B0423F9E2A86559F');
        $this->addSql('ALTER TABLE wt_quotation DROP FOREIGN KEY FK_8CA0CBB7684E0C16');
        $this->addSql('ALTER TABLE cpt DROP FOREIGN KEY FK_CD8760B85A2FCCDC');
        $this->addSql('ALTER TABLE cpt DROP FOREIGN KEY FK_CD8760B88B95CA19');
        $this->addSql('DROP TABLE cpt_templates');
        $this->addSql('DROP TABLE wt_quotation');
        $this->addSql('DROP TABLE component_classification');
        $this->addSql('DROP TABLE cpt_surgery_type');
        $this->addSql('DROP TABLE component_type');
        $this->addSql('DROP TABLE wt_quotation_line');
        $this->addSql('DROP TABLE security_user');
        $this->addSql('DROP TABLE component');
        $this->addSql('DROP TABLE wt_status');
        $this->addSql('DROP TABLE cpt_quote');
        $this->addSql('DROP TABLE cpt');
        $this->addSql('DROP TABLE cpt_status');
        $this->addSql('DROP TABLE wt_product_line');
        $this->addSql('DROP TABLE wt_counters');
        $this->addSql('DROP TABLE component_status');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE component_supplier');
        $this->addSql('DROP TABLE cpt_componenten');
        $this->addSql('DROP TABLE component_group');
        $this->addSql('DROP TABLE wt_classification');
        $this->addSql('DROP TABLE wt_article');
        $this->addSql('DROP TABLE company_contacts');
        $this->addSql('DROP TABLE max_cpt_num');
        $this->addSql('DROP TABLE wt_help_article');
        $this->addSql('DROP TABLE cpt_temp_componenten');
        $this->addSql('DROP TABLE cpt_burden');
    }
}
