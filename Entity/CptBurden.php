<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CptBurdenRepository")
 */
class CptBurden
{
	public function __toString()
	{
		return $this->getDescription();
	}

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $BurdenId;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $Description;

	/**
	 * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
	 */
	private $Rate;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Cpt", mappedBy="Burden")
	 */
	private $cpts;

	public function __construct()
	{
		$this->cpts = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getBurdenId(): ?int
	{
		return $this->BurdenId;
	}

	public function setBurdenId(?int $BurdenId): self
	{
		$this->BurdenId = $BurdenId;

		return $this;
	}

	public function getDescription(): ?string
	{
		return $this->Description;
	}

	public function setDescription(string $Description): self
	{
		$this->Description = $Description;

		return $this;
	}

	public function getRate(): ?string
	{
		return $this->Rate;
	}

	public function setRate(?string $Rate): self
	{
		$this->Rate = $Rate;

		return $this;
	}

	/**
	 * @return Collection|Cpt[]
	 */
	public function getCpts(): Collection
	{
		return $this->cpts;
	}

	public function addCpt(Cpt $cpt): self
	{
		if (!$this->cpts->contains($cpt)) {
			$this->cpts[] = $cpt;
			$cpt->setBurden($this);
		}

		return $this;
	}

	public function removeCpt(Cpt $cpt): self
	{
		if ($this->cpts->contains($cpt)) {
			$this->cpts->removeElement($cpt);
			// set the owning side to null (unless already changed)
			if ($cpt->getBurden() === $this) {
				$cpt->setBurden(null);
			}
		}

		return $this;
	}
}
