<?php

namespace App\Repository;

use App\Entity\ComponentStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ComponentStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method ComponentStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method ComponentStatus[]    findAll()
 * @method ComponentStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComponentStatusRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, ComponentStatus::class);
	}

	public function findWithDescription()
	{
		return $this->createQueryBuilder('c')
			->andWhere('c.Description IS NOT NULL')
			->andWhere("c.Description !=''")
			->orderBy('c.Description', 'ASC')
			->getQuery()
			->getResult();
	}

	public function findComponentStatus()
	{
		return $this->createQueryBuilder('c')
			->andWhere('c.CptStatusId IN  :Componentstatus')
			->setParameter("Componentstatus", array('','',''))
			->orderBy('c.Description', 'ASC')
			->getQuery()
			->getResult();
	}

	// /**
	//  * @return ComponentStatus[] Returns an array of ComponentStatus objects
	//  */
	/*
	public function findByExampleField($value)
	{
	    return $this->createQueryBuilder('c')
		->andWhere('c.exampleField = :val')
		->setParameter('val', $value)
		->orderBy('c.id', 'ASC')
		->setMaxResults(10)
		->getQuery()
		->getResult()
	    ;
	}
	*/

	/*
	public function findOneBySomeField($value): ?ComponentStatus
	{
	    return $this->createQueryBuilder('c')
		->andWhere('c.exampleField = :val')
		->setParameter('val', $value)
		->getQuery()
		->getOneOrNullResult()
	    ;
	}
	*/
}
