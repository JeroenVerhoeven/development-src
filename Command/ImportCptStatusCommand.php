<?php
/**
 * Created by PhpStorm.
 * User: Jeroen
 * Date: 27-8-2019
 * Time: 04:43
 */

namespace App\Command;

use App\Entity\CptStatus;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportCptStatusCommand extends Command
{
	protected static $defaultName = 'medica:import-cpt-status';

	private $entityManager;

	private $container;

	public function __construct(ContainerInterface $container)
	{
		parent::__construct();
		$this->container = $container;
	}

	protected function configure()
	{
		$this
			->setDescription("Import cpt status")
			->setHelp("This command allows you to import all cpt statuses")
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$output->writeln("Start import import-cpt-status");

		$InputPath = getcwd() . '/src/Command/ImportData/';

		$InputFile = "tbl_status_cpt.csv";

		$InputPathFile = $InputPath . $InputFile;

		if(!file_exists($InputPathFile)) {

			$LogData = "ERROR: LET OP! Bestand " . $InputPathFile . " bestaat niet";

			$output->writeln("<info>". $LogData . "</info>");

			exit;
		}

		$this->entityManager = $this->container->get('doctrine')->getManager();

		$handle = fopen($InputPathFile, "r");

		if (empty($handle) === false) {

			while (($data = fgetcsv($handle, 999999, "|")) !== FALSE) {

				$aContent['id'] = (integer) $data[0];
				$aContent['omschrijving'] = $data[1];

				if(!$this->existsInDb($aContent['omschrijving']))
				{
					$CptStatus = new CptStatus();
					$CptStatus->setCptStatusId($aContent['id']);
					$CptStatus->setDescription($aContent['omschrijving']);

					$this->entityManager->persist($CptStatus);
				}
			}

			$this->entityManager->flush();
		}

		$output->writeln("Einde import import-component-status");
	}

	private function existsInDb($description)
	{
		$result = $this->entityManager->getRepository(CptStatus::class)->findBy(
			['Description' => $description]
		);

		if($result) {

			return true;
		}
		return false;
	}
}