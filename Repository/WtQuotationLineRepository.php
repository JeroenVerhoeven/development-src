<?php

namespace App\Repository;

use App\Entity\WtQuotationLine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method WtQuotationLine|null find($id, $lockMode = null, $lockVersion = null)
 * @method WtQuotationLine|null findOneBy(array $criteria, array $orderBy = null)
 * @method WtQuotationLine[]    findAll()
 * @method WtQuotationLine[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WtQuotationLineRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, WtQuotationLine::class);
	}


	public function findQuotationLine_test($QuotationNumber, $ArticleNumber)
	{
		return $this->createQueryBuilder('w')
			->leftJoin('w.Quotation', 'q')
			->andWhere('w.ArticleNumber = :article_number')
			->setParameter('article_number', $ArticleNumber)
			->andWhere('q.QuotationNumber = :quotation_number')
			->setParameter('quotation_number', $QuotationNumber)
			//->andWhere('q.Version = :quotation_version')
			//->setParameter('quotation_version', $Version)
			->setMaxResults(10)
			->getQuery()
			->getResult();
	}



	public function findQuotationLine($QuotationNumber, $Version, $ArticleNumber)
	{
		return $this->createQueryBuilder('w')
			->leftJoin('w.Quotation', 'q')
			->andWhere('w.ArticleNumber = :article_number')
			->setParameter('article_number', $ArticleNumber)
			->andWhere('q.QuotationNumber = :quotation_number')
			->setParameter('quotation_number', $QuotationNumber)
			->andWhere('q.Version = :quotation_version')
			->setParameter('quotation_version', $Version)
			->setMaxResults(10)
			->getQuery()
			->getResult();
	}

	public function findQuotationLines($QuotationNumber)
	{
		return $this->createQueryBuilder('w')
			->leftJoin('w.Quotation', 'r')
			->andWhere('r.QuotationNumber = :quotation_number')
			->setParameter('quotation_number', $QuotationNumber)
			->setMaxResults(10)
			->getQuery()
			->getResult();
	}

	// /**
	//  * @return WtQuotationLine[] Returns an array of WtQuotationLine objects
	//  */
	/*
	public function findByExampleField($value)
	{
	    return $this->createQueryBuilder('w')
		->andWhere('w.exampleField = :val')
		->setParameter('val', $value)
		->orderBy('w.id', 'ASC')
		->setMaxResults(10)
		->getQuery()
		->getResult()
	    ;
	}
	*/

	/*
	public function findOneBySomeField($value): ?WtQuotationLine
	{
	    return $this->createQueryBuilder('w')
		->andWhere('w.exampleField = :val')
		->setParameter('val', $value)
		->getQuery()
		->getOneOrNullResult()
	    ;
	}
	*/
}
