<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WtProductLineRepository")
 */
class WtProductLine
{

        public function __toString()
        {
                return $this->getDescription();
        }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Description;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=1)
     */
    private $MinMargin;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WtHelpArticle", mappedBy="Productline")
     */
    private $wtHelpArticles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WtQuotationLine", mappedBy="ProductLine")
     */
    private $wtQuotationLines;

    /**
     * @ORM\Column(type="integer")
     */
    private $productline_id;

    public function __construct()
    {
        $this->wtHelpArticles = new ArrayCollection();
        $this->wtQuotationLines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getMinMargin(): ?string
    {
        return $this->MinMargin;
    }

    public function setMinMargin(string $MinMargin): self
    {
        $this->MinMargin = $MinMargin;

        return $this;
    }

    /**
     * @return Collection|WtHelpArticle[]
     */
    public function getWtHelpArticles(): Collection
    {
        return $this->wtHelpArticles;
    }

    public function addWtHelpArticle(WtHelpArticle $wtHelpArticle): self
    {
        if (!$this->wtHelpArticles->contains($wtHelpArticle)) {
            $this->wtHelpArticles[] = $wtHelpArticle;
            $wtHelpArticle->setProductline($this);
        }

        return $this;
    }

    public function removeWtHelpArticle(WtHelpArticle $wtHelpArticle): self
    {
        if ($this->wtHelpArticles->contains($wtHelpArticle)) {
            $this->wtHelpArticles->removeElement($wtHelpArticle);
            // set the owning side to null (unless already changed)
            if ($wtHelpArticle->getProductline() === $this) {
                $wtHelpArticle->setProductline(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|WtQuotationLine[]
     */
    public function getWtQuotationLines(): Collection
    {
        return $this->wtQuotationLines;
    }

    public function addWtQuotationLine(WtQuotationLine $wtQuotationLine): self
    {
        if (!$this->wtQuotationLines->contains($wtQuotationLine)) {
            $this->wtQuotationLines[] = $wtQuotationLine;
            $wtQuotationLine->setProductLine($this);
        }

        return $this;
    }

    public function removeWtQuotationLine(WtQuotationLine $wtQuotationLine): self
    {
        if ($this->wtQuotationLines->contains($wtQuotationLine)) {
            $this->wtQuotationLines->removeElement($wtQuotationLine);
            // set the owning side to null (unless already changed)
            if ($wtQuotationLine->getProductLine() === $this) {
                $wtQuotationLine->setProductLine(null);
            }
        }

        return $this;
    }

    public function getProductlineId(): ?int
    {
        return $this->productline_id;
    }

    public function setProductlineId(int $productline_id): self
    {
        $this->productline_id = $productline_id;

        return $this;
    }
}
