<?php

namespace App\Form;

use App\Entity\Cpt;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class CptChangeFormType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('ProductDescriptionCustomer', TextType::class, [
				'label' => 'Cpt Name'
			])
			->add('YearUsage')
			->add('Memo', TextareaType::class, [
                                'attr' => ['class' => 'tinymce'],
                                'required' => false,
                        ])
			->add('EndUser')
			->add('TypeOfSurgery')
			->add('Company')
			->add('CompanyContact')
			->add('Quote')
			->add('Save', SubmitType::class, ['label' => 'Save cpt'])
		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => Cpt::class,
		]);
	}
}
