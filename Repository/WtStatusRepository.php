<?php

namespace App\Repository;

use App\Entity\WtStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method WtStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method WtStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method WtStatus[]    findAll()
 * @method WtStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WtStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WtStatus::class);
    }

        public function findWithDescription()
        {
                return $this->createQueryBuilder('w')
                        ->andWhere('w.Description IS NOT NULL')
                        ->andWhere("w.Description !=''")
                        ->orderBy('w.Description', 'ASC')
                        ->getQuery()
                        ->getResult();
        }

    // /**
    //  * @return WtStatus[] Returns an array of WtStatus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WtStatus
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
