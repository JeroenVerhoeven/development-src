<?php

namespace App\Controller;

use App\Entity\ComponentClassification;
use App\Entity\ComponentGroup;
use App\Entity\ComponentStatus;
use App\Entity\ComponentSupplier;
use App\Entity\ComponentType;
use App\Entity\Cpt;
use App\Entity\Company;
use App\Entity\CptBurden;
use App\Entity\CptComponenten;
use App\Entity\CptStatus;
use App\Entity\CptTempComponenten;
use App\Entity\CptTemplates;
use App\Entity\MaxCptNum;
use App\Entity\Component;
use App\Entity\WtArticle;
use App\Entity\WtQuotationLine;
use App\Form\CptCalcType;
use App\Form\CptChangeCalcType;
use App\Form\CptChangeFormType;
use App\Form\CptViewCalcType;
use App\Form\CptViewDetailType;

use App\Form\CreateTempComponentType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\SecurityUser;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;

// Include the BinaryFileResponse and the ResponseHeaderBag
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

// Include the requires classes of Phpword
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Shared\Converter;
use PhpOffice\PhpWord\TemplateProcessor;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Style\Font;

class MedicaCalcController extends AbstractController
{
        public function __construct(EventDispatcherInterface $dispatcher)
        {
                $this->dispatcher = $dispatcher;
        }

        /**
         * @Route("/", name="home")
         */
        public function index(Request $request)
        {
                return $this->render('medica_calc/index.html.twig', [
                        'controller_name' => 'MedicaCalc',
                ]);
        }

	/**
         * @Route("/GenerateOffer", name="GenerateOffer")
         */
        public function GenerateOffer(Request $request)
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

                $SelectOfferTemplate = $request->query->get('SelectOfferTemplate');

                $aSelectedOfferTemplate = explode("|", $SelectOfferTemplate);

                $Template = $aSelectedOfferTemplate[0];
                $Language = $aSelectedOfferTemplate[1];

                $pathInfo = pathinfo($Template);
                $fileName = $pathInfo['filename'];
                $docName = str_replace(' ', '_' , $fileName);

		$CptNum = $request->query->get('CptNum');

                $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('Templates/' . $Template);

                $entityManager = $this->getDoctrine()->getManager();

                $oCpt = $entityManager->getRepository(Cpt::class)->findByCptNumber($CptNum);

		$BurdenId = '';

                if($oCpt->getBurden()) {
                        $BurdenId = $oCpt->getBurden()->getId();
                }

                $aCalculation = $this->CalcCptCostsCalcOnly($CptNum, $BurdenId, $oCpt->getMargin(), $oCpt->getPackaging());

                $SalesPrice = floatval($aCalculation['cSalesPrice']);

                if ($oCpt->getCompanyContact()) {

                        $CompanyContact = $oCpt->getCompanyContact()->getContactAttn();
                        $ContactJobDescription = $oCpt->getCompanyContact()->getContactJobDescription();

                        $templateProcessor->setValue('CompanyContact', htmlspecialchars($CompanyContact));
                        $templateProcessor->setValue('ContactJobDescription', htmlspecialchars($ContactJobDescription));
                } else {
                        $templateProcessor->setValue('CompanyContact', '');
                        $templateProcessor->setValue('ContactJobDescription', '');
                }

                if ($oCpt->getCompany()) {

                        $CompanyName = $oCpt->getCompany()->getCompanyName();

                        $templateProcessor->setValue('CompanyName', htmlspecialchars($CompanyName));
                } else {
                        $templateProcessor->setValue('CompanyName', '');
                }

                if ($oCpt->getCptStatus()) {

                        $CptStatus = $oCpt->getCptStatus()->getDescription();

                        $templateProcessor->setValue('CptStatus', htmlspecialchars($CptStatus));
                } else {
                        $templateProcessor->setValue('CptStatus', '');
                }

                $Date = date('d-m-Y');
                $TrayName = $oCpt->getProductDescriptionCustomer();
                $YearUsage = $oCpt->getYearUsage();

                // Cpt gegevens
                $templateProcessor->setValue('CptNum', htmlspecialchars($CptNum));
                $templateProcessor->setValue('TrayName', htmlspecialchars($TrayName));
                $templateProcessor->setValue('Date', htmlspecialchars($Date));
                $templateProcessor->setValue('YearUsage', htmlspecialchars($YearUsage));
                $templateProcessor->setValue('sales_price', htmlspecialchars($SalesPrice));

		$oCptComponents = $oCpt->getCptComponentens();

                // Tel aantal niet deleted componenten en clone aantal rijen
		
		$AantalComponenten = sizeof($oCptComponents);

                $templateProcessor->cloneRow('componentnum', $AantalComponenten);

                $teller = 1;

                $endprice = 0;

		$totalComponentsVatHighNL = 0;
		$totalComponentsVatLowNL = 0;
		$totalComponentsVatHighBE = 0;
		$totalComponentsVatLowBE = 0;
		$totalComponentsVatNL = 0;
		$totalComponentsVatBE = 0;

                foreach ($oCptComponents as $oCptComponent) {

                        if ($oCptComponent->getComponent()) {
                                $Description = $this->TranslateDescription($Language, $oCptComponent->getComponent());
                        } else {
                                $Description = $oCptComponent->getDescription();
                        }

                        $templateProcessor->setValue('componentnum#' . $teller, htmlspecialchars($oCptComponent->getComponentNumber()));
                        $templateProcessor->setValue('description#' . $teller, htmlspecialchars($Description));
                        $templateProcessor->setValue('QTY#' . $teller, htmlspecialchars($oCptComponent->getQuantity()));
                        $templateProcessor->setValue('Supplier#' . $teller, htmlspecialchars($oCptComponent->getComponentSupplier()->getName()));
                        $templateProcessor->setValue('Status#' . $teller, htmlspecialchars($oCptComponent->getStatus()));


			if($oCptComponent->getStatus() != 'Deleted') {

				$NLVATPERC = 0;
				$NLVATCODE = 0;
				$BEVATCODE = 0;

				if ($oCptComponent->getComponent()) {
					$NLVATPERC = trim($oCptComponent->getComponent()->getNLVATPERC());
					$NLVATCODE = trim($oCptComponent->getComponent()->getNLVATCODE());
					$BEVATCODE = trim($oCptComponent->getComponent()->getBEVATCODE());
				}

				$PurchasePriceTotal = $oCptComponent->getPurchasePrice() * $oCptComponent->getQuantity();

				// NL
				if ($NLVATPERC > 0) {
					if ($NLVATCODE == 'H' || $NLVATCODE == 'NLBTW21') {
						$totalComponentsVatHighNL += $PurchasePriceTotal;
						$totalComponentsVatNL += $PurchasePriceTotal;
					} elseif ($NLVATCODE == 'L' || $NLVATCODE == 'NLBTW9') {
						$totalComponentsVatLowNL += $PurchasePriceTotal;
						$totalComponentsVatNL += $PurchasePriceTotal;
					}
				}

				if ($BEVATCODE == 'H' || $BEVATCODE == 'BEBTW21') {
					$totalComponentsVatHighBE += $PurchasePriceTotal;
					$totalComponentsVatBE += $PurchasePriceTotal;
				} elseif ($BEVATCODE == 'L' || $BEVATCODE == 'BEBTW6') {
					$totalComponentsVatLowBE += $PurchasePriceTotal;
					$totalComponentsVatBE += $PurchasePriceTotal;
				}
			}

                        $teller++;
                }

                $percentageVatNLHigh = $totalComponentsVatHighNL/$totalComponentsVatNL * 100;
                $percentageVatNLLow = $totalComponentsVatLowNL/$totalComponentsVatNL * 100;

		$percentageVatBEHigh = $totalComponentsVatHighBE/$totalComponentsVatBE * 100;
		$percentageVatBELow = $totalComponentsVatLowBE/$totalComponentsVatBE * 100;

		$BrutoPriceBE = $SalesPrice;
		$ExclBtw21BE = $percentageVatBEHigh * $SalesPrice / 100;
		$ExclBtw6BE = $percentageVatBELow * $SalesPrice / 100;

		$InclBtw21BE =  $ExclBtw21BE * 1.21;
		$InclBtw6BE = $ExclBtw6BE * 1.06;

		$NettoPriceBEHigh = ($SalesPrice * $percentageVatBEHigh / 100) * 0.21;
		$NettoPriceBELow = ($SalesPrice * $percentageVatBELow / 100) * 0.06;
		$TotalNettoBE = $NettoPriceBEHigh + $NettoPriceBELow + $SalesPrice;

		$NettoPriceNLHigh = ($SalesPrice * $percentageVatNLHigh / 100) * 0.21;
		$NettoPriceNLLow = ($SalesPrice * $percentageVatNLLow / 100) * 0.09;
		$TotalNettoNL = $NettoPriceNLHigh + $NettoPriceNLLow + $SalesPrice;

		$InclBtw21NL =  $ExclBtw21BE * 1.21;
		$InclBtw6NL = $ExclBtw6BE * 1.09;

//		dump($totalComponentsVatHighNL);
//		dump($totalComponentsVatLowNL);
//		dump($totalComponentsVatHighBE);
//		dump($totalComponentsVatLowBE);
//
//		dump($totalComponentsVatNL);
//		dump($totalComponentsVatBE);
//
//		dump($percentageVatNLHigh);
//		dump($percentageVatNLLow);
//		dump($percentageVatBEHigh);
//		dump($percentageVatBELow);
//		dump($aCalculation);
//		dump($endprice);
//		dump($SalesPrice);
//
//		dump($percentageVatNLHigh);
//		dump($percentageVatNLLow);
//
//		dump($TotalNettoNL);
//
//		dump($NettoPriceNLHigh);
//		dump($NettoPriceNLLow);
//
//		dump($InclBtw6NL);
//		dump($InclBtw21NL);
//
//		dump($percentageVatBEHigh);
//		dump($percentageVatBELow);
//
//		dump($TotalNettoBE);
//
//		dump($NettoPriceBEHigh);
//		dump($NettoPriceBELow);
//
//		dump($ExclBtw21BE);
//		dump($ExclBtw6BE);
//
//		dump($InclBtw6BE);
//		dump($InclBtw21BE);
//
//		exit;

                //BTW Resultaten BE
                $templateProcessor->setValue('NettoPrice', htmlspecialchars(number_format($SalesPrice, 2,",",".")));
                $templateProcessor->setValue('BrutoPrice', htmlspecialchars(number_format($TotalNettoBE, 2,",",".")));

                $templateProcessor->setValue('NettoPriceEx21', htmlspecialchars(number_format($ExclBtw21BE, 2,",",".")));
                $templateProcessor->setValue('NettoPriceEx6', htmlspecialchars(number_format($ExclBtw6BE, 2,",",".")));
                $templateProcessor->setValue('InclBtw21', htmlspecialchars(number_format($InclBtw21BE, 2,",",".")));
                $templateProcessor->setValue('InclBtw6', htmlspecialchars(number_format($InclBtw6BE, 2,",",".")));
                $templateProcessor->setValue('LowBtw6', htmlspecialchars(number_format($percentageVatBELow, 2,",",".")));
                $templateProcessor->setValue('HighBtw21BE', htmlspecialchars(number_format($percentageVatBEHigh, 2,",",".")));

//              //BTW Resultaten NL
                $templateProcessor->setValue('NettoPriceNL', htmlspecialchars(number_format($SalesPrice, 2,",",".")));
                $templateProcessor->setValue('BrutoPriceNL', htmlspecialchars(number_format($TotalNettoNL, 2,",",".")));
                $templateProcessor->setValue('LowBtw9', htmlspecialchars(number_format($percentageVatNLLow, 2,",",".")));
                $templateProcessor->setValue('HighBtw21NL', htmlspecialchars(number_format($percentageVatNLHigh, 2,",",".")));

                $fileName = $CptNum . '_' . $docName . '_' . $Language . '_' . date('YmdHi') . '.docx';

                $templateProcessor->saveAs('Documents/' . $fileName);

                // Send file as response (as an attachment)
                $response = new BinaryFileResponse('Documents/' . $fileName);
                $response->setContentDisposition(
                        ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                        $fileName
                );

                return $response;
        }

        /**
         * @Route("/ReportCalculation", name="ReportCalculation")
         */
        public function ReportCalculation(Request $request)
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

                $CptNum = $request->query->get('CptNum');

                $CptName = $request->query->get('CptName');

                $Language = $request->query->get('SelectLanguage');

                $entityManager = $this->getDoctrine()->getManager();

                $oCpt = $entityManager->getRepository(Cpt::class)->findByCptNumber($CptNum);

                $BurdenId = '';

                if($oCpt->getBurden()) {
                        $BurdenId = $oCpt->getBurden()->getId();
                }

                $aCalculation = $this->CalcCptCostsCalcOnly($CptNum, $BurdenId, $oCpt->getMargin(), $oCpt->getPackaging());

                $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('Templates/cpt-calculation.docx');

                // Table with components
                $oCptComponents = $oCpt->getCptComponentens();

                $aCalcBTW = $this->calcBTWCosts($oCptComponents);

                if($oCpt->getCompany()) {
                        $company = $oCpt->getCompany()->getCompanyName();
                }
                else {
                        $company = '';
                }

//                dump($aCalculation);
//                dump($oCpt);
//                exit;

                // Cpt gegevens
                $templateProcessor->setValue('CptNum', htmlspecialchars($CptNum));
                $templateProcessor->setValue('CptName', htmlspecialchars($oCpt->getProductDescriptionCustomer()));
                $templateProcessor->setValue('packaging', htmlspecialchars($oCpt->getPackaging()));
                $templateProcessor->setValue('margin', htmlspecialchars($oCpt->getMargin()));
                $templateProcessor->setValue('burden', htmlspecialchars($oCpt->getBurden()));
                $templateProcessor->setValue('calc_burden', htmlspecialchars(number_format($aCalculation['cCalcBurden'], 2,",",".")));
                $templateProcessor->setValue('materials', htmlspecialchars(number_format($aCalculation['cMaterials'], 2,",",".")));
                $templateProcessor->setValue('cost_price', htmlspecialchars(number_format($aCalculation['cCostPrice'], 2,",",".")));
                $templateProcessor->setValue('sales_price', htmlspecialchars(number_format($aCalculation['cSalesPrice'], 2,",",".")));
                $templateProcessor->setValue('original_cost_price', htmlspecialchars(number_format($aCalculation['cOrigCostPrice'], 2,",",".")));
                $templateProcessor->setValue('company', htmlspecialchars($company));
                $templateProcessor->setValue('contact', htmlspecialchars($oCpt->getCompanyContact()));
                $templateProcessor->setValue('year_usage', htmlspecialchars($oCpt->getYearUsage()));
                $templateProcessor->setValue('original_margin', htmlspecialchars($oCpt->getOriginalMargin()));
                $templateProcessor->setValue('original_sales_price', htmlspecialchars(number_format($oCpt->getOriginalSalesPrice(), 2,",",".")));
                $templateProcessor->setValue('NL_H', htmlspecialchars(number_format($aCalcBTW['dBtwNlHoog'], 2,",",".")));
                $templateProcessor->setValue('NL_L', htmlspecialchars(number_format($aCalcBTW['dBtwNlLaag'], 2,",",".")));
                $templateProcessor->setValue('BE_H', htmlspecialchars(number_format($aCalcBTW['dBtwBeHoog'], 2,",",".")));
                $templateProcessor->setValue('BE_L', htmlspecialchars(number_format($aCalcBTW['dBtwBeLaag'], 2,",",".")));

                $AantalComponenten = sizeof($oCptComponents);

                $templateProcessor->cloneRow('componentnum', $AantalComponenten);

                $teller = 1;

                foreach ($oCptComponents as $oCptComponent) {

                        if ($oCptComponent->getComponent()) {

				$ComponentNLVATCODE = $oCptComponent->getComponent()->getNLVATCODE();
				$ComponentBEVATCODE = $oCptComponent->getComponent()->getBEVATCODE();
                                $PurchasePrice = $oCptComponent->getComponent()->getPuchasePrice();

				// Change NL VAT CODE
				if ($ComponentNLVATCODE == 'NLBTW6') {
					$ComponentNLVATCODE = "L";
				}
				else if ($ComponentNLVATCODE == "NLBTW21") {
					$ComponentNLVATCODE = "H";
				}

				// Change BE VAT CODE
				if ($ComponentBEVATCODE == 'BEBTW6') {
					$ComponentBEVATCODE = "L";
				}
				else if ($ComponentBEVATCODE == "BEBTW21") {
					$ComponentBEVATCODE = "H";
				}

                                $Description = $this->TranslateDescription($Language, $oCptComponent->getComponent());
                                $templateProcessor->setValue('NL#' . $teller, htmlspecialchars($ComponentNLVATCODE));
                                $templateProcessor->setValue('BE#' . $teller, htmlspecialchars($ComponentBEVATCODE));

                        }
                        else {
                                $PurchasePrice = $oCptComponent->getPurchasePrice();
                                $Description = $oCptComponent->getDescription();
                                $templateProcessor->setValue('NL#' . $teller, '');
                                $templateProcessor->setValue('BE#' . $teller, '');
                        }

                        $templateProcessor->setValue('componentnum#' . $teller, htmlspecialchars($oCptComponent->getComponentNumber()));
                        $templateProcessor->setValue('description#' . $teller, htmlspecialchars($Description));
                        $templateProcessor->setValue('QTY#' . $teller, htmlspecialchars($oCptComponent->getQuantity()));
                        $templateProcessor->setValue('purchase_date#' . $teller, htmlspecialchars(number_format($PurchasePrice,5,",",".")));
                        $templateProcessor->setValue('comp_status#' . $teller, htmlspecialchars($oCptComponent->getStatus()));

                        $teller++;
                }

                $fileName = $CptNum . '_' . $Language . '_report_' .  date('YmdHi') . '.docx';

                $templateProcessor->saveAs('Documents/' . $fileName);

                $response = new BinaryFileResponse('Documents/' . $fileName);
                $response->setContentDisposition(
                        ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                        $fileName
                );

                return $response;
        }

        protected function getBodyBlock($string)
        {
                if (preg_match('%(?i)(?<=<w:body>)[\s|\S]*?(?=</w:body>)%', $string, $regs)) {
                        return $regs[0];
                } else {
                        return '';
                }
        }

        /**
         * @Route("/cpt", name="CptHme")
         */
        public function CptHome()
        {
		$this->denyAccessUnlessGranted('ROLE_USER');

		$entityManager = $this->getDoctrine()->getManager();

                $request = Request::createFromGlobals();

                $aSearchfield = array();

                $aSearchfield['searchName'] = $request->query->get('searchName');
                $aSearchfield['searchNumber'] = $request->query->get('searchNumber');
                $aSearchfield['searchCustomer'] = $request->query->get('searchCustomer');
                $aSearchfield['searchComponent'] = $request->query->get('searchComponent');
                $aSearchfield['viewAllCpts'] = $request->query->get('viewAllCpts');

                if(	empty($aSearchfield['searchName']) &&
                	empty($aSearchfield['searchNumber']) &&
                 	empty($aSearchfield['searchCustomer']) &&
			empty($aSearchfield['searchName']) &&
                	empty($aSearchfield['viewAllCpts'])
		) {
			$oCpts = $entityManager->getRepository(Cpt::class)->findByFirst(100, $aSearchfield);
                }
                else {
			$oCpts = $entityManager->getRepository(Cpt::class)->filterCpts($aSearchfield);
                }

                return $this->render('medica_calc/cpt-search.html.twig', [
                        'controller_name' => 'MedicaCalc',
                        'oCpts' => $oCpts,
                        'Searchfield' => $aSearchfield,
                ]);
        }

        /**
         * @Route("/login", name="login")
         */
        public function login(AuthenticationUtils $authenticationUtils, UserPasswordEncoderInterface $passwordEncoder)
        {

                $entityManager = $this->getDoctrine()->getManager();

                $users = $entityManager->getRepository(SecurityUser::class)->findAll();

                $error = $authenticationUtils->getLastAuthenticationError();
                $lastUsername = $authenticationUtils->getLastUsername();

                return $this->render('security/login.html.twig', array(
                        'last_username' => $lastUsername,
                        'error' => $error,
                ));
        }

        /**
         * @Route("/cpt-detail/{cptnumber}", name="DetailCpt")
         */
        public function getCptByNumber($cptnumber)
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

                $entityManager = $this->getDoctrine()->getManager();

		$this->UpdateComponent($cptnumber);

		$oCpt = $entityManager->getRepository(Cpt::class)->findByCptNumber($cptnumber);

                $form = $this->createForm(CptViewDetailType::class, $oCpt);

                $formCalc = $this->createForm(CptViewCalcType::class, $oCpt);

                $oTemplates = $entityManager->getRepository(CptTemplates::class)->findAll();

                $oCptComponenten = $oCpt->getCptComponentens();

                $BurdenId = '';

                if($oCpt->getBurden()) {
                	$BurdenId = $oCpt->getBurden()->getId();
		}

                $aCalculation = $this->CalcCptCostsCalcOnly($cptnumber, $BurdenId, $oCpt->getMargin(),  $oCpt->getPackaging());

		$oPackagingComponents = $entityManager->getRepository(Component::class)->getPackageComponents(115);

//                dump($aCalculation);

                return $this->render('medica_calc/cpt-view.html.twig', array(
                        'Cpt' => $oCpt,
                        'form' => $form->createView(),
                        'formCalc' => $formCalc->createView(),
                        'formTemplates' => $oTemplates,
                        'ButtonTextCalculation' => 'Show calculation',
                        'ButtonTextOffer' => 'Generate offer',
                        'ButtonTextComponent' => 'Add component',
                        'Components' => $oCptComponenten,
                        'SaveButton' => false,
                        'Calculation' => $aCalculation,
			'Packagings' => $oPackagingComponents,
                ));
        }

	/**
	 * @Route("/exportxls", name="Exportxls")
	 */
	public function exportCptToXls(Request $request)
	{
                $this->denyAccessUnlessGranted('ROLE_USER');

		$entityManager = $this->getDoctrine()->getManager();

		$CptNum = $request->query->get('CptNumber');
		$Language = $request->query->get('SelectLanguage');

		$oCpt = $entityManager->getRepository(Cpt::class)->findByCptNumber($CptNum);

		$aPackage = $entityManager->getRepository(Component::class)->findBy(
			['ComponentNumber' => $oCpt->getPackaging()]
		);

//		dump($aPackage);
//		exit;

		if(!empty($aPackage)) {
                        $oPackage = $aPackage[0];
                }
		else {
		        $oPackage = 0;
                }

		$oCptComponenten = $oCpt->getCptComponentens();

		$spreadsheet = new Spreadsheet();

		$aColumns = range('A','L');

		$BurdenId = '';

		if($oCpt->getBurden()) {
			$BurdenId = $oCpt->getBurden()->getId();
		}

		$aCalculation = $this->CalcCptCostsCalcOnly($CptNum, $BurdenId, $oCpt->getMargin(), $oCpt->getPackaging());

		foreach ($aColumns as $aColumn) {
			$spreadsheet->getActiveSheet()->getColumnDimension($aColumn)->setAutoSize(true);
			$spreadsheet->getActiveSheet()->getStyle($aColumn . '1')->getFont()->setBold($aColumn . '1');
		}

		$sheet = $spreadsheet->getActiveSheet();

		$sheet->setCellValue('A1', 'Yearusage');
		$sheet->setCellValue('B1', 'Traynumber');
		$sheet->setCellValue('C1', 'Articlecode');
		$sheet->setCellValue('D1', 'Amount');
		$sheet->setCellValue('E1', 'Description');
		$sheet->setCellValue('F1', 'Price');
		$sheet->setCellValue('G1', 'Supplier');
		$sheet->setCellValue('H1', 'Number Supplier');
		$sheet->setCellValue('I1', 'BTW NL');
		$sheet->setCellValue('J1', 'BTW NL %');
		$sheet->setCellValue('K1', 'BTW BE');
		$sheet->setCellValue('L1', 'BTW BE %');

                $sheet->setCellValue('A2', $oCpt->getYearUsage());
                $sheet->setCellValue('B2', $CptNum);
                $sheet->setCellValue('C2', "Name CPT");
                $sheet->setCellValue('D2', $oCpt->getProductDescriptionCustomer());

		$teller = 3;

		foreach ($oCptComponenten as $oCptComponent) {

			if($oCptComponent->getStatus() == 'Deleted'){
				continue;
			}

			$sheet->setCellValue('A' . $teller, $oCpt->getYearUsage());
			$sheet->setCellValue('B' . $teller, $CptNum);
			$sheet->setCellValue('C' . $teller, $oCptComponent->getComponentNumber());
			$sheet->setCellValue('D' . $teller, $oCptComponent->getQuantity());
			$sheet->setCellValue('F' . $teller, $oCptComponent->getPurchasePrice());

			if ($oCptComponent->getComponent()) {

				$sheet->setCellValue('E' . $teller, $this->TranslateDescription($Language, $oCptComponent->getComponent()));
				$sheet->setCellValue('I' . $teller, $oCptComponent->getComponent()->getNLVATCODE());
				$sheet->setCellValue('J' . $teller, $oCptComponent->getComponent()->getNLVATPERC());
				$sheet->setCellValue('K' . $teller, $oCptComponent->getComponent()->getBEVATCODE());
				$sheet->setCellValue('L' . $teller, $oCptComponent->getComponent()->getBEVATPERC());
			}

			if ($oCptComponent->getComponentSupplier()) {

				$sheet->setCellValue('G' . $teller, $oCptComponent->getComponentSupplier()->getName());
				$sheet->setCellValue('H' . $teller, $oCptComponent->getComponentSupplier()->getSupplierId());
			}

			$teller++;

		}

		$sheet->setCellValue('A' . $teller, $oCpt->getYearUsage());
		$sheet->setCellValue('B' . $teller, $CptNum);
		$sheet->setCellValue('C' . $teller, $oCpt->getPackaging());
		$sheet->setCellValue('D' . $teller, $oCpt->getNumberInPackage());

		if($oPackage) {
			$sheet->setCellValue('E' . $teller, $oPackage->getDescription());
			$sheet->setCellValue('F' . $teller, $oPackage->getPuchasePrice());
		}
		else {
			$sheet->setCellValue('E' . $teller, '');
			$sheet->setCellValue('F' . $teller, '');
		}

		$teller++;

		$sheet->setCellValue('A' . $teller, $oCpt->getYearUsage());
		$sheet->setCellValue('B' . $teller, $CptNum);
		$sheet->setCellValue('C' . $teller, "Material");
		$sheet->setCellValue('D' . $teller, $aCalculation['cMaterials']);

		$teller++;

		$sheet->setCellValue('A' . $teller, $oCpt->getYearUsage());
		$sheet->setCellValue('B' . $teller, $CptNum);
		$sheet->setCellValue('C' . $teller, "Burden");
		$sheet->setCellValue('D' . $teller, $aCalculation['cCalcBurden']);

		$teller++;

		$sheet->setCellValue('A' . $teller, $oCpt->getYearUsage());
		$sheet->setCellValue('B' . $teller, $CptNum);
		$sheet->setCellValue('C' . $teller, "Cost price");
		$sheet->setCellValue('D' . $teller, $aCalculation['cCostPrice']);

		$teller++;

		$sheet->setCellValue('A' . $teller, $oCpt->getYearUsage());
		$sheet->setCellValue('B' . $teller, $CptNum);
		$sheet->setCellValue('C' . $teller, "Sales price");
		$sheet->setCellValue('D' . $teller, $aCalculation['cSalesPrice']);

		$writer = new Xlsx($spreadsheet);

		$fileName = $CptNum . '_' . $Language . '_exportCPT_' . '.xlsx';

		$FilePath = $_SERVER['DOCUMENT_ROOT'] . '/Sheets/' . $fileName;

		$writer->save($FilePath);

		$response = new BinaryFileResponse($FilePath);

		$response->setContentDisposition(
			ResponseHeaderBag::DISPOSITION_ATTACHMENT,
			$fileName
		);

		return $response;

	}

        /**
         * @Route("/cpt-change/{cptnumber}/{label?}", name="ChangeCpt")
         */
        public function changeCpt($cptnumber, $label, Request $request)
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

                $entityManager = $this->getDoctrine()->getManager();

		$this->UpdateComponent($cptnumber);

                $oCpt = $entityManager->getRepository(Cpt::class)->findByCptNumber($cptnumber);

                $aComponentGroups = $entityManager->getRepository(ComponentGroup::class)->findWithDescription();

                $aComponentStatuses = $entityManager->getRepository(ComponentStatus::class)->findWithDescription();

                $aComponentSupplier = $entityManager->getRepository(ComponentSupplier::class)->findWithSupplierDescription();

                $oComponents = $entityManager->getRepository(Component::class)->findFirstRowsComponent(1);

                $oTempComponents = $entityManager->getRepository(CptTempComponenten::class)->findAll();

                $oCptComponenten = $oCpt->getCptComponentens();

                $oCompanys = $entityManager->getRepository(Company::class)->findFirstRowsCompany(10);

                $oPackagingComponents = $entityManager->getRepository(Component::class)->getPackageComponents(115);

                $form = $this->createForm(CptChangeFormType::class, $oCpt);

                $formCalc = $this->createForm(CptChangeCalcType::class, $oCpt);

                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {

                        $this->addFlash('success', 'Cpt saved succesful !');

                        $entityManager = $this->getDoctrine()->getManager();

                        $entityManager->persist($oCpt);

                        $entityManager->flush();
                }

                $oNewTempComponent = New CptTempComponenten();

                $formComponent = $this->createForm(CreateTempComponentType::class, $oNewTempComponent);

                $formComponent->handleRequest($request);

                if ($formComponent->isSubmitted() && $formComponent->isValid()) {

                        $entityManager = $this->getDoctrine()->getManager();

                        $entityManager->persist($oNewTempComponent);

                        $entityManager->flush();
                }

                $CptCalcBurden = '';

		if ($oCpt->getBurden()) {
                        $CptCalcBurden = $oCpt->getBurden()->getId();
                }

                $Margin = $oCpt->getMargin();

		$aCalculation = array(
			'cCalcBurden' => '',
			'cMaterials' => '',
			'cCostPrice' => '',
			'cSalesPrice' => '',
			'cNormTime' => '',
			'cOrigCostPrice' => '',
			'CptPackaging' => ''
		);

		if(!empty($CptCalcBurden) and !empty($oCpt->getPackaging())) {
			$aCalculation = $this->CalcCptCostsCalcOnly($cptnumber, $CptCalcBurden, $Margin, $oCpt->getPackaging());
		}

		$oTemplates = $entityManager->getRepository(CptTemplates::class)->findAll();

		if(empty($label)) {
			$label = "Change";
		}

//		dd($oCptComponenten);

		return $this->render('medica_calc/cpt-change.html.twig', array(
                        'Cpt' => $oCpt,
                        'form' => $form->createView(),
                        'formCalc' => $formCalc->createView(),
                        'formComponent' => $formComponent->createView(),
			'formTemplates' => $oTemplates,
                        'ButtonTextCalculation' => 'Show calculation',
                        'oCompanys' => $oCompanys,
                        'Components' => $oCptComponenten,
                        'TempComponents' => $oTempComponents,
                        'oComponents' => $oComponents,
                        'ComponentGroups' => $aComponentGroups,
                        'ComponentStatuses' => $aComponentStatuses,
                        'ComponentSuppliers' => $aComponentSupplier,
                        'SaveButton' => true,
                        'Calculation' => $aCalculation,
			'ButtonTextOffer' => 'Generate offer',
			'Packagings' => $oPackagingComponents,
			'label' => $label,
                ));
        }

        /**
         * @Route("/add-components-cpt", name="Add components to cpt")
         */
        public function AddComponentsCpt(Request $request)
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

                $entityManager = $this->getDoctrine()->getManager();

                $cptnumber = $request->query->get('CptNum');

                $oCpt = $entityManager->getRepository(Cpt::class)->findByCptNumber($cptnumber);

                $AllFormFields = $request->query->all();

                foreach ($AllFormFields as $ComponentId => $Quantity) {

                        if(substr($ComponentId,0,3) == 'id_') {

				if ($Quantity > 0) {

					$OnlyComponentId = substr($ComponentId, 3);

                                        $this->AddComponent2Cpt($cptnumber, $oCpt, $OnlyComponentId, $Quantity);
				}
			}
                }

                return $this->redirect($this->generateUrl('ChangeCpt', ['cptnumber' => $cptnumber]));
        }

	/**
	 * @Route("/add-new-temp-components-cpt", name="Add temp components to cpt")
	 */
	public function AddTNewTempComponentsCpt(Request $request)
	{
                $this->denyAccessUnlessGranted('ROLE_USER');

		$entityManager = $this->getDoctrine()->getManager();

		$cptnumber = $request->query->get('cptnumber');

		$FormFields = $request->query->get('create_temp_component');

		if(!array_key_exists("SampleSupplied", $FormFields)) {
		        $FormFields['SampleSupplied'] = "";
                }

		$SupplierComponentNumber = $FormFields['SupplierComponentNumber'];
		$Supplier = $FormFields['Supplier'];
		$Description = $FormFields['Description'];
		$ComponentType = $FormFields['ComponentType'];
		$PurchasePrice = $FormFields['PurchasePrice'];
		$NormTime = $FormFields['NormTime'];
		$Classification = $FormFields['Classification'];
		$SampleSupplied = $FormFields['SampleSupplied'];
		$Quantity = $FormFields['Quantity'];

		$oSupplier = $entityManager->getRepository(ComponentSupplier::class)->find(1);

		$oCpt = $entityManager->getRepository(Cpt::class)->findByCptNumber($cptnumber);

		$oComponentType = $entityManager->getRepository(ComponentType::class)->find($ComponentType);

		$oClassification = $entityManager->getRepository(ComponentClassification::class)->find($Classification);

		$oComponentStatusNew = $entityManager->getRepository(ComponentStatus::class)->find(1);

		$oNewTempComponent = new CptTempComponenten();
		$oNewTempComponent->setDescription($Description);
		$oNewTempComponent->setSupplier($oSupplier->getName());
		$oNewTempComponent->setClassification($oClassification);
		$oNewTempComponent->setNormTime($NormTime);
		$oNewTempComponent->setPurchasePrice($PurchasePrice);
		$oNewTempComponent->setSampleSupplied($SampleSupplied);
		$oNewTempComponent->setComponentType($oComponentType);
		$oNewTempComponent->setSupplierComponentNumber($SupplierComponentNumber);

		$entityManager->persist($oNewTempComponent);

		$oCptComponent = new CptComponenten();

		$oCptComponent->setCptNumber($cptnumber);
		$oCptComponent->setCpt($oCpt);
		$oCptComponent->setComponentNumber($SupplierComponentNumber);
		$oCptComponent->setPurchasePrice($PurchasePrice);
		$oCptComponent->setNormTime($NormTime);
		$oCptComponent->setComponentClassification($oClassification);
		$oCptComponent->setStatus($oComponentStatusNew);
		$oCptComponent->setComponentSupplier($oSupplier);
		$oCptComponent->setDescription($Description);
		$oCptComponent->setQuantity($Quantity);

		$entityManager->persist($oCptComponent);
		$entityManager->flush();

		return $this->redirect($this->generateUrl('ChangeCpt', ['cptnumber' => $cptnumber]));

	}

        /**
	 * @Route("/cpt-update-component/{cptnumber}", name="ChangeCptComponent")
	 */
	public function changeCptComponent($cptnumber, Request $request)
	{
                $this->denyAccessUnlessGranted('ROLE_USER');

		$entityManager = $this->getDoctrine()->getManager();

		$SelectStatus = $request->query->get('SelectStatus');

		$Quantity = $request->query->get('Quantity');

		$CompId = $request->query->get('CompNumber');

                $oStatus = $entityManager->getRepository(ComponentStatus::class)->find($SelectStatus);

		$oCptComponent = $entityManager->getRepository(CptComponenten::class)->find($CompId);

		$oCptComponent->setQuantity($Quantity);
		$oCptComponent->setStatus($oStatus);

		$entityManager->persist($oCptComponent);
		$entityManager->flush();
		
		return $this->redirect($this->generateUrl('ChangeCpt', ['cptnumber' => $cptnumber]));
	}

	/**
	 * @Route("/add-temp-components", name="AddTempComponents")
	 */
        public function AddTempComponentsCpt(Request $request)
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

                $entityManager = $this->getDoctrine()->getManager();

                $cptnumber = $request->query->get('cptnumber');

                $oCpt = $entityManager->getRepository(Cpt::class)->findByCptNumber($cptnumber);

                $oComponentStatusNew = $entityManager->getRepository(ComponentStatus::class)->find(1);

                $AllFormFields = $request->query->all();

//		dump($AllFormFields);

                foreach ($AllFormFields as $FormField => $Quantity) {

                        if ($Quantity > 0 && substr($FormField, 0,3) == 'id_') {

                                $ComponentId = substr($FormField, 3);

                                echo  $ComponentId . " => " . $Quantity . '<br/>';

				$oComponent = $entityManager->getRepository(CptTempComponenten::class)->find($ComponentId);

				$oSupplier = $entityManager->getRepository(ComponentSupplier::class)->findBy(
					['Name' => $oComponent->getSupplier()]
				);

				$aComponentCpt = $entityManager->getRepository(CptComponenten::class)->findBy(
					[
						'ComponentNumber' => $oComponent->getSupplierComponentNumber(),
						'CptNumber' => $cptnumber
					]
				);

				if(sizeof($aComponentCpt) > 0) {

					$NewComponent = $aComponentCpt[0];

					$OldQuantity = $NewComponent->getQuantity($Quantity);

					$NewComponent->setQuantity($OldQuantity + $Quantity);
				}
				else {
					$NewComponent = new CptComponenten();

					$NewComponent->setCptNumber($cptnumber);
					$NewComponent->setCpt($oCpt);
					$NewComponent->setComponentNumber($oComponent->getSupplierComponentNumber());
					$NewComponent->setPurchasePrice($oComponent->getPurchasePrice());
					$NewComponent->setNormTime($oComponent->getNormTime());
					$NewComponent->setComponentClassification($oComponent->getClassification());
					$NewComponent->setStatus($oComponentStatusNew);
					$NewComponent->setComponentSupplier($oSupplier[0]);
					$NewComponent->setDescription($oComponent->getDescription());
					$NewComponent->setQuantity($Quantity);
				}

                                $entityManager->persist($NewComponent);
                                $entityManager->flush();
                        }
                }

                return $this->redirect($this->generateUrl('ChangeCpt', ['cptnumber' => $cptnumber]));
        }

        /**
         * @Route("/cpt-duplicate/{OldCptNum}", name="Duplicate cpt")
         */
        public function duplicateCpt($OldCptNum)
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

                $entityManager = $this->getDoctrine()->getManager();

                $NewCptNum = $this->getmaxCptNum();

                $NewCptNum .= "AA";

                $oComponentStatusNew = $entityManager->getRepository(ComponentStatus::class)->find(1);

                $oNewCpt = $entityManager->getRepository(Cpt::class)->findByCptNumber($OldCptNum);

                $oCptComponenten = $oNewCpt->getCptComponentens();

                $NewCpt = $this->copyCpt($OldCptNum, $NewCptNum, $oComponentStatusNew);

		$this->UpdateComponent($NewCptNum);

                $form = $this->createForm(CptViewDetailType::class, $NewCpt);

                $formCalc = $this->createForm(CptViewCalcType::class, $NewCpt);

                $oTemplates = $entityManager->getRepository(CptTemplates::class)->findAll();

                $CptCalcBurden = '';

                if($NewCpt->getBurden()) {
                        $CptCalcBurden = $NewCpt->getBurden()->getId();
                }

                $Margin = $NewCpt->getMargin();

//                dump($CptCalcBurden);

                $aCalculation = $this->CalcCptCostsCalcOnly($NewCptNum, $CptCalcBurden ,$Margin, $NewCpt->getPackaging());

                return $this->redirect($this->generateUrl('ChangeCpt', ['cptnumber' => $NewCptNum, 'label' => 'Duplicate']));

        }

        private function copyCpt($OldCptNum, $NewCptNum, $oComponentStatusNew)
        {

                $entityManager = $this->getDoctrine()->getManager();

                $oCpt = $entityManager->getRepository(Cpt::class)->findCptByCptNumber($OldCptNum);

                $CptStatusNew = $entityManager->getRepository(CptStatus::class)->find(1);

                $oCptComponenten = $oCpt->getCptComponentens();

                $ProductDescriptionCustomer = $oCpt->getProductDescriptionCustomer();
                $Employee = $oCpt->getEmployee();
                $yearUsage = $oCpt->getYearUsage();
                $memo = $oCpt->getMemo();
                $Packaging = $oCpt->getPackaging();
                $NumberInPackage = $oCpt->getNumberInPackage();
                $Margin = $oCpt->getMargin();
                $Date = $oCpt->getDate();
                $SalesPrice = $oCpt->getSalesPrice();
                $OriginalSalesPrice = $oCpt->getOriginalSalesPrice();
                $OriginalMargin = $oCpt->getOriginalMargin();
                $CreationDate = $oCpt->getCreationDate();
                $TypeOfSurgery = $oCpt->getTypeOfSurgery();
                $Company = $oCpt->getCompany();
                $CompanyContact = $oCpt->getCompanyContact();
                $Quote = $oCpt->getQuote();
                $Burden = $oCpt->getBurden();
                $CptStatus = $CptStatusNew;
                $EndUser = $oCpt->getEndUser();

                // Aanmaken van de nieuwe CPT
                $oNewCpt = new Cpt();

                $oNewCpt->setCptNumber($NewCptNum);

                $oNewCpt->setProductDescriptionCustomer($ProductDescriptionCustomer);
                $oNewCpt->setEmployee($Employee);
                $oNewCpt->setYearUsage($yearUsage);
                $oNewCpt->setNumberInPackage($NumberInPackage);
                $oNewCpt->setMemo($memo);
                $oNewCpt->setDate($Date);
                $oNewCpt->setPackaging($Packaging);
                $oNewCpt->setMargin($Margin);
                $oNewCpt->setSalesPrice($SalesPrice);
                $oNewCpt->setOriginalSalesPrice($OriginalSalesPrice);
                $oNewCpt->setOriginalMargin($OriginalMargin);
                $oNewCpt->setCreationDate($CreationDate);
                $oNewCpt->setTypeOfSurgery($TypeOfSurgery);
                $oNewCpt->setProductDescriptionCustomer($ProductDescriptionCustomer);
                $oNewCpt->setCompany($Company);
                $oNewCpt->setCompanyContact($CompanyContact);
                $oNewCpt->setQuote($Quote);
                $oNewCpt->setBurden($Burden);
                $oNewCpt->setCptStatus($CptStatusNew);
                $oNewCpt->setEndUser($EndUser);

                foreach ($oCptComponenten as $oComponent) {

			if($oComponent->getStatus()) {
				if($oComponent->getStatus()->getDescription() == 'Deleted') {
					continue;
				}
			}

                        $NewComponent = new CptComponenten();

                        $NewComponent->setCptNumber($NewCptNum);
                        $NewComponent->setCpt($oNewCpt);
                        $NewComponent->setComponentNumber($oComponent->getComponentNumber());
                        $NewComponent->setQuantity($oComponent->getQuantity());
                        $NewComponent->setPurchasePrice($oComponent->getPurchasePrice());
                        $NewComponent->setNormTime($oComponent->getNormTime());
                        $NewComponent->setCptComponentStatus($oComponent->getCptComponentStatus());
                        $NewComponent->setComponentGroup($oComponent->getComponentGroup());
                        $NewComponent->setComponentClassification($oComponent->getComponentClassification());
                        $NewComponent->setComponent($oComponent->getComponent());
                        $NewComponent->setComponentSupplier($oComponent->getComponentSupplier());
                        $NewComponent->setDescription($oComponent->getDescription());
                        $NewComponent->setComponentNumberSupplier($oComponent->getComponentNumberSupplier());

                        $entityManager->persist($NewComponent);
                }

                $entityManager->persist($oNewCpt);

                $entityManager->flush();

                return $oNewCpt;
        }

	/**
	 * @Route("/cpt-revise/{OldCptNum}", name="Revise cpt")
	 */
	public function reviseCpt($OldCptNum)
	{
                $this->denyAccessUnlessGranted('ROLE_USER');

		$entityManager = $this->getDoctrine()->getManager();

		$OldCptNumBare = substr($OldCptNum, 0, -2);

		// Nieuw volgnummer AA -> AB -> AC
		$NewCptNum = $OldCptNumBare . $this->getNextLetter($OldCptNum);

		$oComponentStatusNew = $entityManager->getRepository(ComponentStatus::class)->find(1);

		$oCptStatusNew = $entityManager->getRepository(CptStatus::class)->find(1);

		$oOldCpt = $entityManager->getRepository(Cpt::class)->findCptByCptNumber($OldCptNum);

		$RevisedCpt = $this->copyCpt($OldCptNum, $NewCptNum, $oComponentStatusNew);

		$RevisedCpt->setCptStatus($oCptStatusNew);

		$this->UpdateComponent($NewCptNum);

		$entityManager->persist($RevisedCpt);

		$entityManager->flush();

		$CptCalcBurden = '';

		if($RevisedCpt->getBurden()) {
			$CptCalcBurden = $RevisedCpt->getBurden()->getId();
		}

		$Margin = $RevisedCpt->getMargin();

		$ReviseCptStatus = $entityManager->getRepository(CptStatus::class)->find(3);

		$oOldCpt->setCptStatus($ReviseCptStatus);

		$entityManager->persist($oOldCpt);

		$entityManager->flush();

                return $this->redirect($this->generateUrl('ChangeCpt', ['cptnumber' => $NewCptNum, 'label' => 'Revise']));

	}

        /**
         * @Route("/cpt-confirm-changes/{cptnumber}", name="CptConfirmChanges")
         */
        public function CptConfirmChanges($cptnumber, Request $request)
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

                $entityManager = $this->getDoctrine()->getManager();

                $oCpt = $entityManager->getRepository(Cpt::class)->findByCptNumber($cptnumber);

                $oCptComponenten = $oCpt->getCptComponentens();

                foreach ($oCptComponenten as $oCptComponent) {

                        if ($oCptComponent->getStatus()) {

                                $oCptComponentStatus = $oCptComponent->getStatus()->getDescription();

                                if ($oCptComponentStatus == "Deleted") {

                                        $entityManager->remove($oCptComponent);

                                }
                                else if ($oCptComponentStatus == "New" || $oCptComponentStatus == "Changed") {

                                        $aComponentStatus = $entityManager->getRepository(ComponentStatus::class)->findBy([
                                                'Description' => ''
                                        ]);

                                        $oCptComponent->setStatus($aComponentStatus[0]);

                                        $entityManager->persist($oCptComponent);
                                }
                        }
                }

                $entityManager->flush();

                return $this->redirect($this->generateUrl('ChangeCpt', ['cptnumber' => $cptnumber]));
        }


        /**
         * @Route("/cpt-new", name="New cpt")
         */
        public function newCpt(Request $request)
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

                $entityManager = $this->getDoctrine()->getManager();

                $oCompanys = $entityManager->getRepository(Company::class)->findFirstRowsCompany(10);

                $user = $this->getUser();

                $oCpt = new Cpt();

                $form = $this->createForm(CptChangeFormType::class, $oCpt);

                $formCalc = $this->createForm(CptChangeCalcType::class, $oCpt);

                $form->handleRequest($request);

                $cptStatus = 'New';

                if ($form->isSubmitted() && $form->isValid()) {

                        $this->addFlash('success', 'Cpt saved succesfull!');

                        $FormData = $form->getData();

                        $CptNum = $this->getmaxCptNum();

                        $CptNum .= "AA";

                        $oCpt->setCptNumber($CptNum);

                        $oCpt->setCreationDate(new \DateTime());

                        if($user) {
                                $oCpt->setEmployee($user->getName());
                        } else {
                                $oCpt->setEmployee("Anonymous user");
                        }

                        $CptStatusNew = $entityManager->getRepository(CptStatus::class)->find(1);

                        $oCpt->setCptStatus($CptStatusNew);

                        $entityManager->persist($oCpt);

                        $entityManager->flush();

                        $cptStatus = 'Generated';

                        return $this->redirect($this->generateUrl('ChangeCpt', ['cptnumber' => $CptNum, 'label' => 'New']));

                }

                return $this->render('medica_calc/cpt-new.html.twig', array(
                        'Cpt' => $oCpt,
                        'form' => $form->createView(),
                        'formCalc' => $formCalc->createView(),
                        'ButtonTextCalculation' => 'Show calculation',
                        'oCompanys' => $oCompanys,
                        'SaveButton' => true,
                ));

        }

        /**
         * @Route("/delete_cpt/{id}", name="DeleteCPT")
         */
        public function deleteCPT($id)
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

                $entityManager = $this->getDoctrine()->getManager();

                $oCpt = $entityManager->getRepository(Cpt::class)->find($id);

                foreach ($oCpt->getCptComponentens() as $CptComponent) {

                        $entityManager->remove($CptComponent);

                }

                $entityManager->remove($oCpt);

                $entityManager->flush();

                return $this->redirectToRoute('CptHme');
        }

        /**
         * @Route("/delete_component/{component_id}/{CptNum}", name="DeleteComponent")
         */
        public function DeleteComponent($component_id, $CptNum)
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

                $entityManager = $this->getDoctrine()->getManager();

                $oComponent = $entityManager->getRepository(CptComponenten::class)->find($component_id);
                $oComponentStatus = $entityManager->getRepository(ComponentStatus::class)->findBy([
                        'Description' => 'Deleted'
                ]);

                $oComponent->setStatus($oComponentStatus[0]);

                if (!$oComponent) {
                        throw $this->createNotFoundException('No component found for id '. $component_id);
                }

                $entityManager->persist($oComponent);

                $entityManager->flush();

                return $this->redirect($this->generateUrl('ChangeCpt', ['cptnumber' => $CptNum]));
        }

	/**
         * @Route("/update-component/{cptnumber}/{label?}", name="UpdateComponent")
         */
        public function UpdateComponent($cptnumber, $label = "Change")
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

        	$entityManager = $this->getDoctrine()->getManager();

                $oCpt = $entityManager->getRepository(Cpt::class)->findByCptNumber($cptnumber);

                $oCptComponenten = $oCpt->getCptComponentens();

                foreach($oCptComponenten as $oCptComponent) {
                    
			$oComponent = $oCptComponent->getComponent();

			if ($oCptComponent->getStatus()) {
				$oCptComponentStatus = $oCptComponent->getStatus()->getDescription();

				if ($oCptComponentStatus == "Not found") {

					$ComponentStatusObsolete = $entityManager->getRepository(ComponentStatus::class)->find(6);

					$oCptComponent->setCptComponentStatus($ComponentStatusObsolete);
					$oCptComponent->setPurchasePrice(0);

				}
			}

			if($oComponent) {

				$oCptComponent->setCptComponentStatus($oComponent->getStatus());
				$oCptComponent->setPurchasePrice($oComponent->getPuchasePrice());
				$oCptComponent->setComponentNumber($oComponent->getComponentNumber());
				$oCptComponent->setComponentNumberSupplier($oComponent->getComponentNumberSupplier());
				$oCptComponent->setComponentGroup($oComponent->getComponentGroup());
				$oCptComponent->setComponentClassification($oComponent->getClassification());
				$oCptComponent->setComponentSupplier($oComponent->getSupplier());
				$oCptComponent->setDescription($oComponent->getDescription());

			}

                        $entityManager->persist($oCptComponent);
                }

                $entityManager->flush();

                if(empty($label)) {
                	$label = "Change";
		}

		return $this->redirect($this->generateUrl('ChangeCpt', ['cptnumber' => $cptnumber, 'label' => $label]));
        }

	/**
	 * @Route("/print-tray/{CptNum}/{Language}", name="PrintTray")
	 */
	public function exportTrayToXls($CptNum, $Language)
	{
                $this->denyAccessUnlessGranted('ROLE_USER');

		$entityManager = $this->getDoctrine()->getManager();

		$oCpt = $entityManager->getRepository(Cpt::class)->findByCptNumber($CptNum);

		$oCptComponenten = $oCpt->getCptComponentens();

		$spreadsheet = new Spreadsheet();

		$sheet = $spreadsheet->getActiveSheet();

		$sheet->setCellValue('D1', 'REF    ' . $CptNum);
		$sheet->setCellValue('A2', $oCpt->getProductDescriptionCustomer());
		$sheet->setCellValue('A4', 'AMOUNT');

		$sheet->getStyle("A1:D4")->getFont()->setBold( true );
		$sheet->getStyle("A1:D2")->getFont()->setSize(20);
		$sheet->getStyle("A4")->getFont()->setSize(28);
		$sheet->getRowDimension('1')->setRowHeight(150);

		$sheet->getColumnDimension('B')->setWidth(26);
		$sheet->getColumnDimension('D')->setWidth(26);

		$sheeti = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$sheeti->setName('name');
		$sheeti->setDescription('description');
		$sheeti->setPath('./images/Ref_border.png');
		$sheeti->setHeight(50);
		$sheeti->setCoordinates("D1");
		$sheeti->setOffsetX(0);
		$sheeti->setOffsetY(150);
		$sheeti->setWorksheet($sheet);

		$teller = 6;

		$iRegels = sizeof($oCptComponenten);

		$Col1 = 'A';
		$Col2 = 'B';

		$Regels = 1;

		foreach ($oCptComponenten as $oCptComponent) {

			if ($oCptComponent->getComponent()) {

				if($oCptComponent->getStatus() == 'Deleted'){
					continue;
				}

				$ComponentDescription = $this->TranslateDescription($Language, $oCptComponent->getComponent());

				$ComponentDescription = substr($ComponentDescription, 0, 26);

				$sheet->setCellValue($Col1 . $teller, $oCptComponent->getQuantity());
				$sheet->setCellValue($Col2. $teller, $ComponentDescription);

				$teller++;

				if($Regels > ($iRegels/2)) {
					$Col1 = 'C';
					$Col2 = 'D';
					$Regels = 1;
					$teller = 6;
				}
				$Regels++;
			}
		}

		$sheet->setCellValue('A26', 'SAMPLE');
		$sheet->setCellValue('A28', 'NON-STERIEL');

		$sheet->getStyle("A26:A28")->getFont()->setBold( true );
		$sheet->getStyle("A26:A28")->getFont()->setSize(28);

		$writer = new Xlsx($spreadsheet);

		$fileName = $CptNum . '_print_tray.xlsx';

		$FilePath = $_SERVER['DOCUMENT_ROOT'] . '/Sheets/' . $fileName;

		$writer->save($FilePath);

		$response = new BinaryFileResponse($FilePath);

		$response->setContentDisposition(
			ResponseHeaderBag::DISPOSITION_ATTACHMENT,
			$fileName
		);

		return $response;

	}

	/**
	 * @Route("/add-component/ajax")
	 */
	public function addComponentAjax(Request $request)
	{
		if ($request->isXmlHttpRequest() || $request->query->get('showJson') == 1) {

                        $cptnumber = $request->request->get('cptnumber');

                        $componentnumber = $request->request->get('componentnumber');

                        $quantity = $request->request->get('quantity');

                        $componentid = $request->request->get('componentid');

                        $entityManager = $this->getDoctrine()->getManager();

                        $oCpt = $entityManager->getRepository(Cpt::class)->findByCptNumber($cptnumber);

                        $oComponentStatusChanged = $entityManager->getRepository(ComponentStatus::class)->find(3);

			$aCptComponents = $entityManager->getRepository(CptComponenten::class)->findBy(
				[
					'CptNumber' => $cptnumber,
					'ComponentNumber' => $componentnumber
				]
			);

			if(sizeof($aCptComponents) == 0)
			{
				$oCptComponent = $this->AddComponent2Cpt($cptnumber, $oCpt, $componentid, $quantity);
			}
                        else {

				$oCptComponent = $aCptComponents[0];

				$QuantityOld = $oCptComponent->getQuantity();

				$oCptComponent->setQuantity($QuantityOld + $quantity);

				$oCptComponent->setStatus($oComponentStatusChanged);

				$entityManager->persist($oCptComponent);

				$entityManager->flush();
                        }

                        $ComponentNumberSupplier = $oCptComponent->getComponentNumberSupplier();

                        $Description = $oCptComponent->getDescription();

                        $PurchasePrice = $oCptComponent->getPurchasePrice();

                        if($oCptComponent->getComponentGroup()) {
                                $ComponentGroup = $oCptComponent->getComponentGroup()->getDescription();
                        }
                        else {
                                $ComponentGroup = "";
                        }

                        if($oCptComponent->getComponentClassification()) {
                                $ComponentClassification = $oCptComponent->getComponentClassification()->getDescription();
                        }
                        else {
                                $ComponentClassification = "";
                        }

                        if($oCptComponent->getCptComponentStatus()) {
                                $CptComponentStatus = $oCptComponent->getCptComponentStatus()->getDescription();
                        }
                        else {
                                $CptComponentStatus = "";
                        }

                        if($oCptComponent->getComponent()) {
                                $Supplier = $oCptComponent->getComponent()->getSupplier()->getName();
                        }
                        else {
                                $Supplier = "";
                        }

                        if($oCptComponent->getComponent()) {
			        $Image = $oCptComponent->getComponent()->getImage();
                        }
                        else {
                                $Image = "";
                        }

                        $jsonData = array(
                                'cptnumber' => $cptnumber,
                                'componentnumber' => $componentnumber,
                                'componentid' => $componentid,
                                'quantity' => $quantity,
                                'ComponentNumberSupplier' => $ComponentNumberSupplier,
                                'Description' => $Description,
                                'Supplier' => $Supplier,
                                'ComponentGroup' => $ComponentGroup,
                                'PurchasePrice' => $PurchasePrice,
                                'ComponentClassification' => $ComponentClassification,
                                'CptComponentStatus' => $CptComponentStatus,
                                'Image' => $Image
                        );

                        return new JsonResponse($jsonData);
		}
	}

	private function existsInDb($ComponentNumber)
	{
		$entityManager = $this->getDoctrine()->getManager();
		$quantity = 0;

		$aComponents = $entityManager->getRepository(CptComponenten::class)->findBy(
			['ComponentNumber' => $ComponentNumber]
		);

		if($aComponents) {
			return true;
		}
		else
		{
			return false;
		}
	}

        /**
         * @Route("/validate-offer-file/ajax")
         */
        public function validateOfferFile(Request $request)
        {

                if ($request->isXmlHttpRequest() || $request->query->get('showJson') == 1) {

                        $offer_file_name = $request->request->get('offer_file_name');

                        $aSelectedOfferTemplate = explode("|", $offer_file_name);

                        $offer_file_name = $aSelectedOfferTemplate[0];

                        $jsonData = 'Exists';

                        if (!file_exists('Templates/' . $offer_file_name)) {
                                $jsonData = 'NotExists';
                        }

                        return new JsonResponse($jsonData);
                }
        }

        /**
         * @Route("/validate-report-calc/ajax")
         */
        public function validateReportCalc(Request $request)
        {

                if ($request->isXmlHttpRequest() || $request->query->get('showJson') == 1) {

                        $offer_file_name = $request->request->get('offer_file_name');

                        $jsonData = 'Exists';

                        return new JsonResponse($jsonData);
                }
        }

        /**
         * @Route("/find-companies/ajax")
         */
        public function findCompanies(Request $request)
        {

                if ($request->isXmlHttpRequest() || $request->query->get('showJson') == 1) {

                        $entityManager = $this->getDoctrine()->getManager();

                        $aSearchFields = array();

                        $aSearchFields['searchCompanyname'] = $request->request->get('searchCompanyname');
                        $aSearchFields['searchZipCode'] = $request->request->get('searchZipCode');
                        $aSearchFields['searchCity'] = $request->request->get('searchCity');
                        $aSearchFields['searchCompanyContact'] = $request->request->get('searchCompanyContact');

                        $oCompanies = $entityManager->getRepository(Company::class)->searchCompanies($aSearchFields);

                        return new JsonResponse($oCompanies);
                }
        }

        /**
         * @Route("/find-components/ajax")
         */
        public function findComponents(Request $request)
        {

                if ($request->isXmlHttpRequest() || $request->query->get('showJson') == 1) {

                        $entityManager = $this->getDoctrine()->getManager();

                        $aSearchFields = array();

                        $aSearchFields['searchCompNumber'] = $request->request->get('searchCompNumber');
                        $aSearchFields['searchDescription'] = $request->request->get('searchDescription');
                        $aSearchFields['searchSupplier'] = $request->request->get('searchSupplier');
                        $aSearchFields['searchCompGroup'] = $request->request->get('searchCompGroup');
                        $aSearchFields['searchCompNumberMedica'] = $request->request->get('searchCompNumberMedica');

                        $oComponent = $entityManager->getRepository(Component::class)->searchComponents($aSearchFields);

                        return new JsonResponse($oComponent);
                }
        }

	/**
	 * @Route("/calc-cpt/ajax")
	 */
	public function CalcCpt(Request $request)
	{
		if ($request->isXmlHttpRequest() || $request->query->get('showJson') == 1) {

			$CptNum = $request->request->get('cpt_num');

			$CptChangeCalcMargin = $request->request->get('cpt_change_calc_Margin');

			$CptChangeCalcMargin = str_replace(',', '.' , $CptChangeCalcMargin);

			$CptCalcBurden = $request->request->get('cpt_change_calc_Burden');

			$CptPackaging = $request->request->get('cpt_change_calc_Packaging');

			$aCalculation = $this->CalcCptCosts($CptNum, $CptCalcBurden, $CptChangeCalcMargin, $CptPackaging);

			$aCalcResult = array();

			array_push($aCalcResult, array(
				'cSalesPrice' => $aCalculation['cSalesPrice'],
				'cCalcBurden' => $aCalculation['cCalcBurden'],
				'cMaterials' => $aCalculation['cMaterials'],
				'cNormTime' => $aCalculation['cNormTime'],
				'cCostPrice' => $aCalculation['cCostPrice'],
				'CptPackaging' => $aCalculation['CptPackaging']
			));

			return new JsonResponse($aCalcResult);
		}

		$aCalcResult = array();

		return new JsonResponse($aCalcResult);
	}

	private function getmaxCptNum()
	{
		$entityManager = $this->getDoctrine()->getManager();

		$oMaxCptNum = $entityManager->getRepository(MaxCptNum::class)->find(1);

		$MaxCptNum = $oMaxCptNum->getMaxCptNum();

		$oMaxCptNum->setMaxCptNum($MaxCptNum + 1);

		$entityManager->persist($oMaxCptNum);

		$entityManager->flush();

		return $MaxCptNum;
	}

	private function getNextLetter($CptNumber)
	{
		$LastLetters = substr($CptNumber, -2);

		$FirstLetter = substr($LastLetters, 0, 1);

		$LastLetter = substr($LastLetters, 1, 1);

		$aLetters = range('A', 'Z');

		if($LastLetter === 'Z') {

			$PositionFirstLetter = array_search($FirstLetter, $aLetters);

			$FirstLetter = $aLetters[$PositionFirstLetter + 1];

			$LastLetter = $aLetters[0];

		}
		else {
			$PositionLastLetter = array_search($LastLetter, $aLetters);

			$LastLetter = $aLetters[$PositionLastLetter + 1];
		}

		return  $FirstLetter . $LastLetter;
	}

	private function TranslateDescription($language, $Component)
	{
		$ComponentDescription = $Component->getDescription();

		$DescriptionES = str_replace(' ', '', $Component->getDescriptionES());
		$DescriptionNL = str_replace(' ', '', $Component->getDescriptionNL());
		$DescriptionDE = str_replace(' ', '', $Component->getDescriptionDE());
		$DescriptionFR = str_replace(' ', '', $Component->getDescriptionFR());
		$DescriptionIT = str_replace(' ', '', $Component->getDescriptionIT());

		if ($language == 'NL' && !empty($DescriptionNL)) {

			$ComponentDescription = $Component->getDescriptionNL();
		}

		if ($language == 'DE' && !empty($DescriptionDE)) {

			$ComponentDescription = $Component->getDescriptionDE();
		}

		if ($language == 'FR' && !empty($DescriptionFR)) {

			$ComponentDescription = $Component->getDescriptionFR();
		}

		if ($language == 'ES' && !empty($DescriptionES)) {

			$ComponentDescription = $Component->getDescriptionES();
		}

		if ($language == 'IT' && !empty($DescriptionIT)) {

			$ComponentDescription = $Component->getDescriptionIT();
		}

		return $ComponentDescription;

	}


	/**
	 * @Route("/calc-test")
	 */
	public function CalcCptCostsTest()
	{
		$CptNumber = "101120AA";
		$BurdenId = "1";
		$MarginPerc = "4.55";
		$CptPackaging = "VERPAKKING D";

		$aCalculation = $this->CalcCptCostsCalcOnly($CptNumber, $BurdenId, $MarginPerc, $CptPackaging);
//		$aCalculation = $this->CalcCptCosts($CptNumber, $BurdenId, $MarginPerc, $CptPackaging);

		dump($aCalculation);

		exit;
	}

	private function CalcCptCostsCalcOnly($CptNumber, $BurdenId, $MarginPerc, $CptPackaging)
	{
		$entityManager = $this->getDoctrine()->getManager();

		$ComponentData = $entityManager->getRepository(Cpt::class)->getNormtijdInkoop($CptNumber);

//		dump($ComponentData);
//		exit;

		$NormTime = $ComponentData['NormTime'];

		$Inkoop = $ComponentData['Inkoop'];

		$oCpt = $entityManager->getRepository(Cpt::class)->findByCptNumber($CptNumber);

		$OriginalSalesPrice = $oCpt->getOriginalSalesPrice();

		$OriginalMargin = $oCpt->getOriginalMargin();

		$oPackaging = '';

		if($CptPackaging) {

			$oFindPackaging = $entityManager->getRepository(Component::class)->findBy(
				['ComponentNumber' => trim($CptPackaging)]
			);

			if($oFindPackaging) {
				$oPackaging = $oFindPackaging[0];
			}
		}

		$BurdenRate = $this->getBurdenRate($BurdenId);

//		dump($ComponentData);
//		dump($NormTime);
//		dump($BurdenId);
//		dump($BurdenRate);

		$iInkoopPackages = 0;

		if($oPackaging) {

			$iInkoopPackages = $oPackaging->getPuchasePrice();
		}

		$cCalcBurden = $BurdenRate * $NormTime;
		$cMaterials = $Inkoop + $iInkoopPackages;
		$cCostPrice = $Inkoop + $iInkoopPackages + $cCalcBurden;
		$cSalesPrice = $cCostPrice / (100 - $MarginPerc) * 100;
		$cOrigCostPrice = $OriginalSalesPrice * (100 - $OriginalMargin) / 100;

		$cCalcBurden = sprintf('%0.2f', $cCalcBurden);
		$cMaterials = sprintf('%0.2f', $cMaterials);
		$cCostPrice = sprintf('%0.2f', $cCostPrice);
		$cSalesPrice = sprintf('%0.2f', $cSalesPrice);
		$cOrigCostPrice = sprintf('%0.2f', $cOrigCostPrice);

		$aCalculation = array(
			'cCalcBurden' => $cCalcBurden,
			'cMaterials' => $cMaterials,
			'cCostPrice' => $cCostPrice,
			'cSalesPrice' => $cSalesPrice,
			'cNormTime' => $NormTime,
			'cOrigCostPrice' => $cOrigCostPrice,
			'CptPackaging' => $iInkoopPackages
		);

		return $aCalculation;
	}

	private function CalcCptCosts($CptNumber, $BurdenId, $MarginPerc, $CptPackaging)
	{
		$entityManager = $this->getDoctrine()->getManager();

		$ComponentData = $entityManager->getRepository(Cpt::class)->getNormtijdInkoop($CptNumber);

		$NormTime = $ComponentData['NormTime'];

		$Inkoop = $ComponentData['Inkoop'];

		$oCpt = $entityManager->getRepository(Cpt::class)->findByCptNumber($CptNumber);

                $OriginalSalesPrice = $oCpt->getOriginalSalesPrice();

                $OriginalMargin = $oCpt->getOriginalMargin();

                if($CptPackaging) {

			$oFindPackaging = $entityManager->getRepository(Component::class)->findBy(
				['ComponentNumber' => trim($CptPackaging)]
			);

			$oPackaging = $oFindPackaging[0];
		}

//                dump($oPackaging);

		$BurdenRate = $this->getBurdenRate($BurdenId);

		$iInkoopPackages = 0;
		
		if($oPackaging) {

			$iInkoopPackages = $oPackaging->getPuchasePrice();
		}


//		dump($oCpt);
//		dump($NormTime);
//		dump($Inkoop);
//		dump($oBurden);
//		dump($BurdenRate);
//		dump($Margin);
//		dump($CptPackaging);
//		dump($oPackaging);
//		dump($oPackaging->getComponentNumber());
//		dump($iInkoopPackages);
//		exit;

		$cCalcBurden = $BurdenRate * $NormTime;
		$cMaterials = $Inkoop + $iInkoopPackages;
		$cCostPrice = $Inkoop + $iInkoopPackages + $cCalcBurden;
		$cSalesPrice = $cCostPrice / (100 - $MarginPerc) * 100;
                $cOrigCostPrice = $OriginalSalesPrice * (100 - $OriginalMargin) / 100;

		$cCalcBurden = sprintf('%0.2f', $cCalcBurden);
		$cMaterials = sprintf('%0.2f', $cMaterials);
		$cCostPrice = sprintf('%0.2f', $cCostPrice);
		$cSalesPrice = sprintf('%0.2f', $cSalesPrice);
                $cOrigCostPrice = sprintf('%0.2f', $cOrigCostPrice);

		$aCalculation = array(
			'cCalcBurden' => $cCalcBurden,
			'cMaterials' => $cMaterials,
			'cCostPrice' => $cCostPrice,
			'cSalesPrice' => $cSalesPrice,
			'cNormTime' => $NormTime,
                        'cOrigCostPrice' => $cOrigCostPrice,
                        'CptPackaging' => $oPackaging->getComponentNumber()
		);

		if($oPackaging) {

			$oBurden = $entityManager->getRepository(CptBurden::class)->find($BurdenId);

			$oCpt->setOriginalSalesPrice($oCpt->getSalesPrice());

			$oCpt->setSalesPrice($cSalesPrice);

			$oCpt->setBurden($oBurden);

			$oCpt->setMargin($MarginPerc);

			$oCpt->setPackaging(trim($oPackaging->getComponentNumber()));

			$oCpt->setOriginalMargin($oCpt->GetMargin());

			$entityManager->persist($oCpt);

			$entityManager->flush();

			$entityManager->clear();

//			dump($oCpt);
		}

		return $aCalculation;
	}

	private function calcBTWCosts($oCptComponenten)
        {
                foreach($oCptComponenten as $oCptComponent) {

                        if(!$oCptComponent->getComponent()) {
                        	continue;
			}

                	$ComponentNLVATCODE = $oCptComponent->getComponent()->getNLVATCODE();
                        $ComponentBEVATCODE = $oCptComponent->getComponent()->getBEVATCODE();

                        // Change NL VAT CODE
                        if ($ComponentNLVATCODE == 'NLBTW6') {
                                $ComponentNLVATCODE = "L";
                        }
                        else if ($ComponentNLVATCODE == "NLBTW21") {
                                $ComponentNLVATCODE = "H";
                        }

                        // Change BE VAT CODE
                        if ($ComponentBEVATCODE == 'BEBTW6') {
                                $ComponentBEVATCODE = "L";
                        }
                        else if ($ComponentBEVATCODE == "BEBTW21") {
                                $ComponentBEVATCODE = "H";
                        }

                        $oComponentStatus = "";

                        if($oCptComponent->getStatus()) {
                                $oComponentStatus = $oCptComponent->getStatus()->getDescription();
                        }

                        // Berekenen BTW
                        $dBtwNlHoog = 0;
                        $dBtwNlLaag = 0;
                        $dBtwBeHoog = 0;
                        $dBtwBeLaag = 0;

                        if ($oComponentStatus != 'Deleted') {
                                if ($ComponentNLVATCODE = "H") {
                                        $dBtwNlHoog += $oCptComponent->getQuantity() * $oCptComponent->getPurchasePrice() * ( 21 / 100);
                                }
                                if ($ComponentNLVATCODE = "L") {
                                        $dBtwNlLaag += $oCptComponent->getQuantity() * $oCptComponent->getPurchasePrice() * ( 6 / 100);
                                }
                                if ($ComponentBEVATCODE = "H") {
                                        $dBtwBeHoog += $oCptComponent->getQuantity() * $oCptComponent->getPurchasePrice() * ( 21 / 100);
                                }
                                if ($ComponentNLVATCODE = "L") {
                                        $dBtwBeLaag += $oCptComponent->getQuantity() * $oCptComponent->getPurchasePrice() * ( 6 / 100);
                                }
                        }
                }

                $dBtwNlHoog = sprintf('%0.2f', $dBtwNlHoog);
                $dBtwNlLaag = sprintf('%0.2f', $dBtwNlLaag);
                $dBtwBeHoog = sprintf('%0.2f', $dBtwBeHoog);
                $dBtwBeLaag = sprintf('%0.2f', $dBtwBeLaag);

		$aBtwCosts = array(
			'dBtwNlHoog' => $dBtwNlHoog,
			'dBtwNlLaag' => $dBtwNlLaag,
			'dBtwBeHoog' => $dBtwBeHoog,
			'dBtwBeLaag' => $dBtwBeLaag,
		);

		return $aBtwCosts;
        }

	private function getBurdenRate($BurdenId)
	{
		$entityManager = $this->getDoctrine()->getManager();

		if(!empty($BurdenId)) {
			$oBurden = $entityManager->getRepository(CptBurden::class)->find($BurdenId);

			$BurdenRate = $oBurden->getRate();
		}
		else {
			$BurdenRate = 0;
		}

		return $BurdenRate;
	}

	private function AddComponent2Cpt($cptnumber, $oCpt, $ComponentId, $Quantity)
        {
                $entityManager = $this->getDoctrine()->getManager();

                $oComponentStatusNew = $entityManager->getRepository(ComponentStatus::class)->find(1);

                $oComponent = $entityManager->getRepository(Component::class)->find($ComponentId);

                $NewComponent = new CptComponenten();

                $NewComponent->setCptNumber($cptnumber);
                $NewComponent->setCpt($oCpt);
                $NewComponent->setComponentNumber($oComponent->getComponentNumber());
                $NewComponent->setQuantity($Quantity);
                $NewComponent->setPurchasePrice($oComponent->getPuchasePrice());
                $NewComponent->setNormTime($oComponent->getNormTime());
                $NewComponent->setCptComponentStatus($oComponent->getStatus());
                $NewComponent->setComponentGroup($oComponent->getComponentGroup());
                $NewComponent->setComponentClassification($oComponent->getClassification());
                $NewComponent->setStatus($oComponentStatusNew);
                $NewComponent->setComponent($oComponent);
                $NewComponent->setComponentSupplier($oComponent->getSupplier());
                $NewComponent->setDescription($oComponent->getDescription());
                $NewComponent->setComponentNumberSupplier($oComponent->getComponentNumberSupplier());

                $entityManager->persist($NewComponent);
                $entityManager->flush();

                return $NewComponent;
        }
}