<?php

namespace App\Form;

use App\Entity\Company;
use App\Entity\CompanyContacts;
use App\Entity\Cpt;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class CptViewDetailType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('Employee', TextType::class, [
				'label' => 'Employee Medica',
				'disabled' => true,
			])
			->add('EndUser', TextType::class, [
				'disabled' => true,
			])
//			->add('EndUser', EntityType::class, [
//				'class' => CompanyContacts::class,
//				'choice_label' => function ($Contact) {
//					return $Contact->getContactName();
//				},
//				'disabled' => true,
//			])
			->add('YearUsage', TextType::class, [
				'disabled' => true,
			])
                        ->add('Memo', TextareaType::class, [
                                'attr' => ['class' => 'tinymce'],
                                'disabled' => true,
                        ])
			->add('TypeOfSurgery', TextType::class, [
				'disabled' => true,
			])
			->add('Company', TextType::class, [
				'disabled' => true,
			])
			->add('CompanyContact', TextType::class, [
				'disabled' => true,
			])
			->add('Quote', TextType::class, [
				'disabled' => true,
			])
		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => Cpt::class,
		]);
	}
}
