<?php

namespace App\Repository;

use App\Entity\CptStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CptStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method CptStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method CptStatus[]    findAll()
 * @method CptStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CptStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CptStatus::class);
    }

    // /**
    //  * @return CptStatus[] Returns an array of CptStatus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CptStatus
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
