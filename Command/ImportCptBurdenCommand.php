<?php
/**
 * Created by PhpStorm.
 * User: Jeroen
 * Date: 27-8-2019
 * Time: 04:43
 */

namespace App\Command;

use App\Entity\CptBurden;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportCptBurdenCommand extends Command
{
	protected static $defaultName = 'medica:import-cpt-burden';

	private $entityManager;

	private $container;

	public function __construct(ContainerInterface $container)
	{
		parent::__construct();
		$this->container = $container;
	}

	protected function configure()
	{
		$this
			->setDescription("Import Cpt burden")
			->setHelp("This command allows you to import all Cpt burden")
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$output->writeln("Start import import-cpt-burden");

		$InputPath = getcwd() . '/src/Command/ImportData/';

		$InputFile = "tbl_burden.csv";

		$InputPathFile = $InputPath . $InputFile;

		if(!file_exists($InputPathFile)) {

			$LogData = "ERROR: LET OP! Bestand " . $InputPathFile . " bestaat niet";

			$output->writeln("<info>". $LogData . "</info>");

			exit;
		}

		$this->entityManager = $this->container->get('doctrine')->getManager();

		$handle = fopen($InputPathFile, "r");

		if (empty($handle) === false) {

			while (($data = fgetcsv($handle, 999999, "|")) !== FALSE) {

				$aContent['id'] = (int) $data[0];
				$aContent['description'] = $data[1];
				$aContent['rate'] = (float) $data[2];

				if(!$this->existsInDb($aContent['description']))
				{
					$CptBurden = new CptBurden();
					$CptBurden->setDescription($aContent['description']);
					$CptBurden->setBurdenId($aContent['id']);
					$CptBurden->setRate($aContent['rate']);

					$this->entityManager->persist($CptBurden);
				}
			}

			$this->entityManager->flush();
		}

		$output->writeln("Einde import import-cpt-burden");
	}

	private function existsInDb($description)
	{
		$result = $this->entityManager->getRepository(CptBurden::class)->findBy(
			['Description' => $description]
		);

		if($result) {

			return true;
		}
		return false;
	}
}