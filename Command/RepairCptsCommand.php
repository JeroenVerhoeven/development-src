<?php
/**
 * Created by PhpStorm.
 * User: Jeroen
 * Date: 27-8-2019
 * Time: 04:43
 */

namespace App\Command;

use App\Entity\Company;
use App\Entity\CompanyContacts;
use App\Entity\Cpt;
use App\Entity\CptBurden;
use App\Entity\CptQuote;
use App\Entity\CptSurgeryType;
use App\Entity\CptStatus;
use App\Entity\Component;
//use App\Repository\ComponentGroupRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RepairCptsCommand extends Command
{
	protected static $defaultName = 'medica:repair-cpts';

	private $entityManager;

	private $container;

	public function __construct(ContainerInterface $container)
	{
		parent::__construct();
		$this->container = $container;
	}

	protected function configure()
	{
		$this
			->setDescription("Repair Cpts")
			->setHelp("This command repair status CPT")
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$output->writeln("Start import repair-cpts");

		$InputPath = getcwd() . '/src/Command/ImportData/';

		$InputFile = "tbl_cpt.csv";

		$InputPathFile = $InputPath . $InputFile;

		if(!file_exists($InputPathFile)) {

			$LogData = "ERROR: LET OP! Bestand " . $InputPathFile . " bestaat niet";

			$output->writeln("<info>". $LogData . "</info>");

			exit;
		}

		$this->entityManager = $this->container->get('doctrine')->getManager();

		$time_start 	= microtime(true);

		$fp = file($InputPathFile);
		$AantalRegels =  count($fp);

		$handle = fopen($InputPathFile, "r");

		if (empty($handle) === false) {

			$teller = 0;

			while (($data = fgetcsv($handle, 999999, "|")) !== FALSE) {

				$teller++;

//				if($teller < 30700) {
//					continue;
//				}

				if ($teller % 100 == 0) {

					$time_end 	= microtime(true);
					$time 		= sprintf("%.3f", $time_end - $time_start);

					$output->writeln("rows: " . $teller . "/" . $AantalRegels . ", time spend: ". $time);

					$time_start = microtime(true);
				}

				$aContent['CptNumber'] = $data[0];
				$aContent['IdCustomer'] = $data[1];
				$aContent['IdContact'] = $data[2];
				$aContent['EndUser'] = $data[3];
				$aContent['EndUserPlace'] = $data[4];
				$aContent['Employee'] = $data[5];
				$aContent['YearUsage'] = $data[6];
				$aContent['TypeOfSurgery'] = $data[7];
				$aContent['ProductDescriptionCustomer'] = $data[8];
				$aContent['Quote'] = $data[9];
				$aContent['Memo'] = $data[10];
				$aContent['Packaging'] = $data[11];
				$aContent['NumberInPackage'] = $data[12];
				$aContent['Margin'] = $data[13];
				$aContent['Burden'] = $data[14];
				$aContent['CptStatus'] = $data[15];
				$aContent['Date'] = $data[16];
				$aContent['SalesPrice'] = $data[17];
				$aContent['OriginalSalesPrice'] = $data[18];
				$aContent['OriginalMargin'] = $data[19];
				$aContent['CreationDate'] = $data[20];

				$aFindCpt = $this->existsInDb($aContent['CptNumber']);
				if(sizeof($aFindCpt) > 0)
				{
					$this->repairCpt($aContent);
				}
			}
		}

		$this->entityManager->flush();
		$this->entityManager->clear();

		$output->writeln("Einde import repair-cpts");
	}

	private function repairCpt($aContent)
	{
		$oFindCpt = $this->entityManager->getRepository(Cpt::class)->findBy(
			['CptNumber' => $aContent['CptNumber']]
		);

		$oCpt = $oFindCpt[0];

		$oCptStatus = $this->getCptStatus($aContent['CptStatus']);
		if($oCptStatus) {
			$oCpt->setCptStatus($oCptStatus);
		}

		$this->entityManager->persist($oCpt);
		$this->entityManager->flush();
		$this->entityManager->clear();
	}

	private function getCptStatus($status)
	{
		$oCptStatus = $this->entityManager->getRepository(CptStatus::class)->findBy(
			['CptStatusId' => $status]
		);

		if($oCptStatus) {
			return $oCptStatus[0];
		}

		return;
	}

	private function existsInDb($CptNumber)
	{
		$result = $this->entityManager->getRepository(Cpt::class)->findBy(
			['CptNumber' => $CptNumber]
		);

		return $result;
	}
}