<?php
/**
 * Created by PhpStorm.
 * User: Jeroen
 * Date: 27-8-2019
 * Time: 04:43
 */

namespace App\Command;

use App\Entity\Company;
use App\Entity\CompanyContacts;
use App\Entity\Cpt;
use App\Entity\CptBurden;
use App\Entity\CptQuote;
use App\Entity\CptSurgeryType;
use App\Entity\CptStatus;
use App\Entity\Component;
//use App\Repository\ComponentGroupRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RepairCptsPackageCommand extends Command
{
	protected static $defaultName = 'medica:repair-cpt-package';

	private $entityManager;

	private $container;

	public function __construct(ContainerInterface $container)
	{
		parent::__construct();
		$this->container = $container;
	}

	protected function configure()
	{
		$this
			->setDescription("Repair Cpts")
			->setHelp("This command allows you to repair all Cpts")
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$output->writeln("Start repair package cpt's");

		$this->entityManager = $this->container->get('doctrine')->getManager();

		$time_start = microtime(true);

		$this->entityManager = $this->container->get('doctrine')->getManager();

		$oCpts  = $this->entityManager->getRepository(Cpt::class)->findAll();

		foreach ($oCpts as $oCpt) {

			$oPackage = $this->entityManager->getRepository(Component::class)->findBy(
				['ComponentNumber' => $oCpt->getPackaging()]
			);


			if($oPackage) {
				$oCpt->setPackaging($oPackage->getId());
				$this->entityManager->persist($oCpt);
			}

			$this->entityManager->flush();
			$this->entityManager->clear();
		}

		$output->writeln("Einde repair package cpt's");
	}
}