<?php

namespace App\Form;

use App\Entity\WtQuotation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddQuotationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Memo1', TextareaType::class, [
		    'attr' => ['class' => 'tinymce'],
		    'required' => false,
	    ])
	    ->add('Memo2', TextareaType::class, [
		   'attr' => ['class' => 'tinymce'],
		    'required' => false,
	    ])
            ->add('Name', TextType::class, [
            	'label' => 'Quotation name',
	    ])
	    ->add('Version', TextType::class, [
		'disabled' => true,
	    ])
            ->add('Customer')
            ->add('CompanyContacts')
	    ->add('Save', SubmitType::class, [
			'label' => 'Save quotation'
		])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WtQuotation::class,
        ]);
    }
}
