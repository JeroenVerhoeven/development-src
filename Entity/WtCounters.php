<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WtCountersRepository")
 */
class WtCounters
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $MaxQuotationNumber;

    /**
     * @ORM\Column(type="integer")
     */
    private $MaxArticle;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMaxQuotationNumber(): ?int
    {
        return $this->MaxQuotationNumber;
    }

    public function setMaxQuotationNumber(int $MaxQuotationNumber): self
    {
        $this->MaxQuotationNumber = $MaxQuotationNumber;

        return $this;
    }

    public function getMaxArticle(): ?int
    {
        return $this->MaxArticle;
    }

    public function setMaxArticle(int $MaxArticle): self
    {
        $this->MaxArticle = $MaxArticle;

        return $this;
    }
}
