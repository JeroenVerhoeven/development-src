<?php

namespace App\Controller;

use App\Entity\Company;
use App\Entity\CptTemplates;
use App\Entity\SecurityUser;
use App\Entity\WtArticle;
use App\Entity\WtClassification;
use App\Entity\WtCounters;
use App\Entity\WtProductLine;
use App\Entity\WtQuotation;
use App\Entity\WtQuotationLine;
use App\Entity\WtStatus;
use App\Form\AddQuotationType;
use App\Form\CptChangeFormType;
use App\Form\CptViewDetailType;
use App\Form\CreateTempArticleType;
use App\Form\CreateTempQuotationLineType;
use App\Form\QuotationCalculationType;
use App\Form\QuotationViewType;
use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

// Include the BinaryFileResponse and the ResponseHeaderBag
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\JsonResponse;

// Include the requires classes of Phpword
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Shared\Converter;
use PhpOffice\PhpWord\TemplateProcessor;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Style\Font;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class MedicaTradeController extends AbstractController
{
        /**
         * @Route("/quotation-view/{QuotationNumber}", name="View quotation")
         */
        public function quotationView($QuotationNumber)
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

                $entityManager = $this->getDoctrine()->getManager();

                $oQuotation = $entityManager->getRepository(WtQuotation::class)->findByQuotationNumber($QuotationNumber);

		$oQuotationLines = $oQuotation->getWtQuotationLines();

                $form = $this->createForm(QuotationViewType::class, $oQuotation);


                return $this->render('medica_wintrade/quotation-view.html.twig', [
                        'Quotation' => $oQuotation,
                        'form' => $form->createView(),
			'QuotationLines' => $oQuotationLines,
                ]);
        }

        /**
	 * @Route("/create-temp-quotationline", name="CreateTempQuotationLine")
	 */
	public function CreateTempQuotationLine(Request $request)
	{
		$this->denyAccessUnlessGranted('ROLE_USER');

		$entityManager = $this->getDoctrine()->getManager();

		$quotationnum = $request->query->get('quotationnum');

		$FormFields = $request->query->get('create_temp_quotation_line');

		$ArticleNumberSupplier = $FormFields['ArticleNumberSupplier'];
		$Supplier = $FormFields['NameSupplier'];
		$Description = $FormFields['Description'];
		$Classification = $FormFields['Classification'];
		$PurchasePrice = $FormFields['PurchasePrice'];
		$ProductLine = $FormFields['ProductLine'];
		$MinMargin = $FormFields['MinMargin'];
		$PreferredMargin = $FormFields['PreferredMargin'];
		$MinPrice = $FormFields['MinPrice'];
		$PreferredPrice = $FormFields['PreferredPrice'];
		$SellingUnit = $FormFields['SellingUnit'];
		$VAT = $FormFields['VAT'];
		$SellingPrice = $FormFields['SellingPrice'];
		$Quantity = $FormFields['Quantity'];

		$PurchasePrice = str_replace(',', '.', $PurchasePrice );

		$PreferredPrice = str_replace(',', '.', $PreferredPrice );

		$SellingPrice = str_replace(',', '.', $SellingPrice );

		$MinPrice = str_replace(',', '.', $MinPrice );

		$MinMargin = str_replace(',', '.', $MinMargin );

		$PreferredMargin = str_replace(',', '.', $PreferredMargin );

		$oQuotation = $entityManager->getRepository(WtQuotation::class)->findByQuotationNumber($quotationnum);

		$oProductLines = $entityManager->getRepository(WtProductLine::class)->find($ProductLine);

		$oClassification = $entityManager->getRepository(WtClassification::class)->find($Classification);

		$ArticleNumber = $this->getmaxArticleNum();

		$oNewTempQuotationLine = new WtQuotationLine();

		$oNewTempQuotationLine->setQuotation($oQuotation);
		$oNewTempQuotationLine->setDescription($Description);
		$oNewTempQuotationLine->setArticleNumber($ArticleNumber);
		$oNewTempQuotationLine->setNameSupplier($Supplier);
		$oNewTempQuotationLine->setPurchasePrice($PurchasePrice);
		$oNewTempQuotationLine->setArticleNumberSupplier($ArticleNumberSupplier);
		$oNewTempQuotationLine->setProductLine($oProductLines);
		$oNewTempQuotationLine->setClassification($oClassification);
		$oNewTempQuotationLine->setMinMargin($MinMargin);
		$oNewTempQuotationLine->setPreferredMargin($PreferredMargin);
		$oNewTempQuotationLine->setMinPrice($MinPrice);
		$oNewTempQuotationLine->setPreferredPrice($PreferredPrice);
		$oNewTempQuotationLine->setSellingUnit($SellingUnit);
		$oNewTempQuotationLine->setVAT($VAT);
		$oNewTempQuotationLine->setSellingPrice($SellingPrice);
		$oNewTempQuotationLine->setQuantity($Quantity);

		$entityManager->persist($oNewTempQuotationLine);

		$oArticle = new WtArticle();

		$oArticle->setArticleNumber($ArticleNumber);
		$oArticle->setPurchasePrice($PurchasePrice);
		$oArticle->setNameSupplier($Supplier);
		$oArticle->setClassification($oClassification);
		$oArticle->setProductline($oProductLines);
		$oArticle->setPurchasePrice($PurchasePrice);
		$oArticle->setDescription($Description);
		$oArticle->setMinMargin($MinMargin);
		$oArticle->setPreferredMargin($PreferredMargin);
		$oArticle->setPackingUnit(1);
		$oArticle->setMinimalPrice($MinPrice);
		$oArticle->setPreferredPrice($PreferredPrice);
		$oArticle->setArticleNumberSupplier($ArticleNumberSupplier);
		$oArticle->setVAT($VAT);

		$entityManager->persist($oArticle);
		$entityManager->flush();

		return $this->redirect($this->generateUrl('ChangeQuotation', ['quotationNumber' => $quotationnum]));
	}

	/**
	 * @Route("/quotation", name="quotation")
	 */
	public function quotation()
	{
		$this->denyAccessUnlessGranted('ROLE_USER');

		$entityManager = $this->getDoctrine()->getManager();

		$aQuotationStatusses = $entityManager->getRepository(WtStatus::class)->findWithDescription();

		$aProductLines = $entityManager->getRepository(WtProductLine::class)->findWithDescription();

		$request = Request::createFromGlobals();

		$aSearchfield = array();

		$aSearchfield['searchQuotation'] = $request->query->get('searchQuotation');
		$aSearchfield['searchName'] = $request->query->get('searchName');
		$aSearchfield['searchCustomer'] = $request->query->get('searchCustomer');
		$aSearchfield['searchDate'] = $request->query->get('searchDate');
		$aSearchfield['searchStatus'] = $request->query->get('searchStatus');
		$aSearchfield['searchArticleNumber'] = $request->query->get('searchArticleNumber');
		$aSearchfield['searchProductLine'] = $request->query->get('searchProductLine');

		if (!empty($aSearchfield['searchQuotation']) ||
			!empty($aSearchfield['searchName']) ||
			!empty($aSearchfield['searchCustomer']) ||
			!empty($aSearchfield['searchDate']) ||
			!empty($aSearchfield['searchStatus']) ||
			!empty($aSearchfield['searchArticleNumber']) ||
			!empty($aSearchfield['searchProductLine'])
		) {
			$oQuotations = $entityManager->getRepository(WtQuotation::class)->filterQuotations($aSearchfield);
		} else {
			$oQuotations = $entityManager->getRepository(WtQuotation::class)->findByFirst(100);
		}

		return $this->render('medica_wintrade/quotation-search.html.twig', [
			'oQuotations' => $oQuotations,
			'Searchfield' => $aSearchfield,
			'QuotationStatusses' => $aQuotationStatusses,
			'ProductLines' => $aProductLines,
		]);
	}

	/**
	 * @Route("/wtgenerateoffer", name="wtgenerateoffer")
	 */
	public function wtgenerateoffer(Request $request)
	{
		$this->denyAccessUnlessGranted('ROLE_USER');

		$QuotationNumber = $request->query->get('QuotationNum');

		$SelectOfferTemplate = $request->query->get('SelectOfferTemplate');

		$aSelectedOfferTemplate = explode("|", $SelectOfferTemplate);

		$Template = $aSelectedOfferTemplate[0];

		$Language = $aSelectedOfferTemplate[1];

		$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('Templates/' . $Template);

		$entityManager = $this->getDoctrine()->getManager();

		$oQuotation = $entityManager->getRepository(WtQuotation::class)->findByQuotationNumber($QuotationNumber);

		$Version = $oQuotation->getVersion();

		if ($oQuotation->getCustomer()) {

			$CompanyName = $oQuotation->getCustomer()->getCompanyName();
			$templateProcessor->setValue('CompanyName', htmlspecialchars($CompanyName));
		}

		if ($oQuotation->getCompanyContacts()) {
			$CompanyContact = $oQuotation->getCompanyContacts()->getContactName();
			$ContactJob = $oQuotation->getCompanyContacts()->getContactJobDescription();

			$templateProcessor->setValue('CompanyContact', htmlspecialchars($CompanyContact));
			$templateProcessor->setValue('ContactJob', htmlspecialchars($ContactJob));
		}

		$Date = date('d-m-Y');

		$Name = $oQuotation->getName();

		// Quotation gegevens
		$templateProcessor->setValue('Quotationnumber', htmlspecialchars($QuotationNumber));
		$templateProcessor->setValue('Version', htmlspecialchars($Version));
		$templateProcessor->setValue('CompanyName', htmlspecialchars($CompanyName));
		$templateProcessor->setValue('Date', htmlspecialchars($Date));
		$templateProcessor->setValue('Name', htmlspecialchars($Name));

		// Table with components
		$oQuotationlines = $oQuotation->getWtQuotationLines();

		$AantalComponenten = sizeof($oQuotationlines);

		$templateProcessor->cloneRow('Description', $AantalComponenten);

		$teller = 1;
		$endprice = 0;

                foreach ($oQuotationlines as $oQuotationline) {

                        $unitprice = $oQuotationline->getSellingPrice() / $oQuotationline->getSellingUnit();
                        $priceperpackage = $oQuotationline->getSellingUnit() * $unitprice;
                        $totalprice = $oQuotationline->getQuantity() * $priceperpackage;

                        $oArticle = $entityManager->getRepository(WtArticle::class)->findOneBy(
				['ArticleNumber' => $oQuotationline->getArticleNumber()]
			);

			$packing_unit = '';
                        if($oArticle) {
				$packing_unit = $oArticle->getPackingUnit();
			}

                        $templateProcessor->setValue('Description#' . $teller, htmlspecialchars($oQuotationline->getDescription()));
                        $templateProcessor->setValue('medicaordernr#' . $teller, htmlspecialchars($oQuotationline->getArticleNumber()));
                        $templateProcessor->setValue('verpeenheid#' . $teller, htmlspecialchars($oQuotationline->getSellingUnit()));
                        $templateProcessor->setValue('vat#' . $teller, htmlspecialchars(round($oQuotationline->getVAT(), 0)));
                        $templateProcessor->setValue('totalprice#' . $teller, htmlspecialchars('€ ' . number_format($totalprice, 2,",",".")));
                        $templateProcessor->setValue('unitprice#' . $teller, htmlspecialchars('€ ' . number_format($unitprice, 2,",",".")));
                        $templateProcessor->setValue('packingprice#' . $teller, htmlspecialchars('€ ' . number_format($priceperpackage, 2,",",".")));
                        $templateProcessor->setValue('yearusage#' . $teller, htmlspecialchars($oQuotationline->getQuantity()));
                        $templateProcessor->setValue('packingunit#' . $teller, $packing_unit);

                        $endprice += $totalprice;

                        $teller++;
                }

                $templateProcessor->setValue('endprice', htmlspecialchars('€ ' . number_format($endprice, 2,",",".")));

		$fileName = $QuotationNumber . '_offer' . $Language . '_' . date('YmdHi') . '.docx';

		$templateProcessor->saveAs('Documents/' . $fileName);

		// Send file as response (as an attachment)

		$response = new BinaryFileResponse('Documents/' . $fileName);

		$response->setContentDisposition(
			ResponseHeaderBag::DISPOSITION_ATTACHMENT,
			$fileName
		);

		return $response;
	}


	/**
         * @Route("/quotation-change/{quotationNumber}", name="ChangeQuotation")
         */
        public function quotationChange($quotationNumber, Request $request)
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

                $entityManager = $this->getDoctrine()->getManager();

                $oQuotation = $entityManager->getRepository(WtQuotation::class)->findByQuotationNumber($quotationNumber);

                $oArticles = $entityManager->getRepository(WtArticle::class)->findFirstArticleRows(15);

                $oCompanys = $entityManager->getRepository(Company::class)->findFirstRowsCompany(10);

                $aProductLines = $entityManager->getRepository(WtProductLine::class)->findWithDescription();

                $oQuotationLines = $oQuotation->getWtQuotationLines();

                $user = $this->getUser();

                $version = $oQuotation->getVersion();

                $nextVersion = $this->getNextVersion($version);

                $form = $this->createForm(QuotationCalculationType::class, $oQuotation);

                $formView = $this->createForm(QuotationViewType::class, $oQuotation);

                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                        $this->addFlash('success', 'Quotation saved succesfull!');

                        $FormData = $form->getData();

                        $oQuotation->setEmployee($user->getName());

                        $oQuotation->setVersion($nextVersion);

                        $entityManager->persist($oQuotation);

                        $entityManager->flush();

			$form = $this->createForm(QuotationCalculationType::class, $oQuotation);
                }

                $oTemplates = $entityManager->getRepository(CptTemplates::class)->findAll();

		$oNewQuotationLine = New WtQuotationLine();

		$formArticle = $this->createForm(CreateTempQuotationLineType::class, $oNewQuotationLine);

		$formArticle->handleRequest($request);
		
		if ($formArticle->isSubmitted() && $formArticle->isValid()) {

			$entityManager = $this->getDoctrine()->getManager();

			$entityManager->persist($oNewQuotationLine);

			$entityManager->flush();

		}
		
                return $this->render('medica_wintrade/quotation-change.html.twig', [
                        'Quotation' => $oQuotation,
                        'oCompanys' => $oCompanys,
                        'Articles' => $oArticles,
                        'ProductLines' => $aProductLines,
                        'QuotationLines' => $oQuotationLines,
                        'formTemplates' => $oTemplates,
                        'form' => $form->createView(),
                        'formView' => $formView->createView(),
			'formArticle' => $formArticle->createView(),
                        'SaveButton' => true,
                ]);

        }

        /**
         * @Route("/search-article", name="search Article")
         */
        public function searchArticle()
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

                $entityManager = $this->getDoctrine()->getManager();

                return $this->render('medica_wintrade/quotation-change.html.twig', [
                ]);
        }


        /**
         * @Route("/reports", name="Reports")
         */
        public function reports()
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

                $entityManager = $this->getDoctrine()->getManager();

                $oQuotation = $entityManager->getRepository(WtQuotation::class)->findAll();

                return $this->render('medica_wintrade/reports.html.twig', [
                        'Quotation' => $oQuotation,
                ]);
        }


        /**
         * @Route("/quotation-update-quotationline/{quotationNumber}", name="ChangeQuotationQuotationLine")
         */
        public function changeQuotationQuotationLine($quotationNumber, Request $request)
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

                $entityManager = $this->getDoctrine()->getManager();

                $Quantity = $request->query->get('Quantity');

                $SellingPrice = $request->query->get('SellingPrice');

                $QuotationLineId = $request->query->get('QuotationLineId');

                $oQuotation = $entityManager->getRepository(WtQuotation::class)->findByQuotationNumber($quotationNumber);

                $oQuotationLine = $entityManager->getRepository(WtQuotationLine::class)->find($QuotationLineId);

                $oQuotationLine->setQuantity($Quantity);
                $oQuotationLine->setSellingPrice($SellingPrice);

                $entityManager->persist($oQuotationLine);
                $entityManager->flush();

                return $this->redirect($this->generateUrl('ChangeQuotation', ['quotationNumber' => $quotationNumber]));
        }

        /**
         * @Route("/delete_quotationline/{id}/{quotationNumber}", name="DeleteQuotationLine")
         */
        public function deleteQuotationLine($id, $quotationNumber)
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

                $entityManager = $this->getDoctrine()->getManager();

                $oQuotationLine = $entityManager->getRepository(WtQuotationLine::class)->find($id);

                $entityManager->remove($oQuotationLine);

                $entityManager->flush();

                return $this->redirect($this->generateUrl('ChangeQuotation', ['quotationNumber' => $quotationNumber]));
        }

        /**
         * @Route("/export-quotationlines", name="ExportQuotationExcel")
         */
        public function exportQuotationToXls(Request $request)
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

		$entityManager = $this->getDoctrine()->getManager();

		$oQuotation = $entityManager->getRepository(WtQuotation::class)->findAll();

		$oQuotationLines = $entityManager->getRepository(WtQuotationLine::class)->findAll();

		$startDate = new \DateTime($request->query->get('StartDate'));
		$endDate = new \DateTime($request->query->get('EndDate'));

		$spreadsheet = new Spreadsheet();

		$aColumns = range('A', 'AC');

		foreach ($aColumns as $aColumn) {
			$spreadsheet->getActiveSheet()->getColumnDimension($aColumn)->setAutoSize(true);
			$spreadsheet->getActiveSheet()->getStyle($aColumn . '1')->getFont()->setBold($aColumn . '1');
		}

		$sheet = $spreadsheet->getActiveSheet();

		$sheet->setCellValue('A1', 'Quotation number');
		$sheet->setCellValue('B1', 'Version');
		$sheet->setCellValue('C1', 'Date');
		$sheet->setCellValue('D1', 'Id customer');
		$sheet->setCellValue('E1', 'Id contact');
		$sheet->setCellValue('F1', 'Employee');
		$sheet->setCellValue('G1', 'Memo1');
		$sheet->setCellValue('H1', 'Memo2');
		$sheet->setCellValue('I1', 'Status');
		$sheet->setCellValue('J1', 'Name');
		$sheet->setCellValue('K1', 'Name customer');
		$sheet->setCellValue('L1', 'City customer');
		$sheet->setCellValue('M1', 'Quotation Line');
		$sheet->setCellValue('N1', 'Article number');
		$sheet->setCellValue('O1', 'Description');
		$sheet->setCellValue('P1', 'Purchase price');
		$sheet->setCellValue('Q1', 'Min. margin');
		$sheet->setCellValue('R1', 'Pref. margin');
		$sheet->setCellValue('S1', 'Min. price');
		$sheet->setCellValue('T1', 'Pref. price');
		$sheet->setCellValue('U1', 'Selling unit');
		$sheet->setCellValue('V1', 'Quantity');
		$sheet->setCellValue('W1', 'Selling price');
		$sheet->setCellValue('X1', 'Name supplier');
		$sheet->setCellValue('Y1', 'Classification');
		$sheet->setCellValue('Z1', 'Product line');
		$sheet->setCellValue('AA1', 'Art. supplier');
		$sheet->setCellValue('AB1', 'VAT');
		$sheet->setCellValue('AC1', 'Article type');

		$teller = 2;

		foreach($oQuotationLines as $QuotationLine) {

			$QuotationId = $QuotationLine->getQuotation();

			$ProductLineId = $QuotationLine->getProductLine();
			if($ProductLineId)
			{
				$ProductLineIds = $ProductLineId->getProductLineId();
			}
			else
			{
				$ProductLineIds = "";
			}
			//$oProductLines = $entityManager->getRepository(WtProductLine::class)->find($ProductLineId);

			$CompanyId = $QuotationId->getCustomer();
			if(!$CompanyId)
			{
				$CompanyId = "";
			}

			$CompanyContactsId = $QuotationId->getCompanyContacts();
			if($CompanyContactsId) {
				$CompanyContactId = $CompanyContactsId->getContactId();
			}
			else
			{
				$CompanyContactId = "";
			}

			$Status = $QuotationId->getStatus();
			if($Status) {
				$StatusId = $Status->getId();
			}
			else
			{
				$StatusId = "";
			}

			$ClassificationId = $QuotationLine->getClassification();

				if ($QuotationId->getDate() >= $startDate && $QuotationId->getDate() <= $endDate) {
					$sheet->setCellValue('A' . $teller, $QuotationId->getQuotationNumber());
					$sheet->setCellValue('B' . $teller, $QuotationId->getVersion());
					$sheet->setCellValue('C' . $teller, $QuotationId->getDate());
					$sheet->setCellValue('D' . $teller, $CompanyId->getCompanyId());
					$sheet->setCellValue('E' . $teller, $CompanyContactId);
					$sheet->setCellValue('F' . $teller, $QuotationId->getEmployee());
					$sheet->setCellValue('G' . $teller, $QuotationId->getMemo1());
					$sheet->setCellValue('H' . $teller, $QuotationId->getMemo2());
					$sheet->setCellValue('I' . $teller, $StatusId);
					$sheet->setCellValue('J' . $teller, $QuotationId->getName());
					$sheet->setCellValue('K' . $teller, $QuotationId->getCustomer());
					$sheet->setCellValue('L' . $teller, $CompanyId->getCompanyCity());
					$sheet->setCellValue('M' . $teller, $QuotationLine->getId());
					$sheet->setCellValue('N' . $teller, $QuotationLine->getArticleNumber());
					$sheet->setCellValue('O' . $teller, $QuotationLine->getDescription());
					$sheet->setCellValue('P' . $teller, $QuotationLine->getPurchasePrice());
					$sheet->setCellValue('Q' . $teller, $QuotationLine->getMinMargin());
					$sheet->setCellValue('R' . $teller, $QuotationLine->getPreferredMargin());
					$sheet->setCellValue('S' . $teller, $QuotationLine->getMinPrice());
					$sheet->setCellValue('T' . $teller, $QuotationLine->getPreferredPrice());
					$sheet->setCellValue('U' . $teller, $QuotationLine->getSellingUnit());
					$sheet->setCellValue('V' . $teller, $QuotationLine->getQuantity());
					$sheet->setCellValue('W' . $teller, $QuotationLine->getSellingPrice());
					$sheet->setCellValue('X' . $teller, $QuotationLine->getNameSupplier());
					$sheet->setCellValue('Y' . $teller, $ClassificationId->getClassificationId());
					$sheet->setCellValue('Z' . $teller, $ProductLineIds);
					$sheet->setCellValue('AA' . $teller, $QuotationLine->getArticleNumberSupplier());
					$sheet->setCellValue('AB' . $teller, $QuotationLine->getVAT());
					$sheet->setCellValue('AC' . $teller, $QuotationLine->getArticleType());
					$teller++;
				}
		}

		$writer = new Xlsx($spreadsheet);

		$fileName = 'export_quotationLines' . date('Ymd') . '.xlsx';

		$FilePath = $_SERVER['DOCUMENT_ROOT'] . '/Sheets/' . $fileName;

		$writer->save($FilePath);

		$response = new BinaryFileResponse($FilePath);

		$response->setContentDisposition(
			ResponseHeaderBag::DISPOSITION_ATTACHMENT,
			$fileName
		);

		return $response;
        }

        /**
         * @Route("/exportQuotation", name="ExportQuotation")
         */
        public function exportQuotation(Request $request)
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

                $entityManager = $this->getDoctrine()->getManager();

                $oQuotation = $entityManager->getRepository(WtQuotation::class)->findAll();

                $oQuotationLines = $entityManager->getRepository(WtQuotationLine::class)->findAll();

                $startDate = new \DateTime($request->query->get('StartDate'));
                $endDate = new \DateTime($request->query->get('EndDate'));

                $spreadsheet = new Spreadsheet();

                $aColumns = range('A', 'L');

                foreach ($aColumns as $aColumn) {
                        $spreadsheet->getActiveSheet()->getColumnDimension($aColumn)->setAutoSize(true);
                        $spreadsheet->getActiveSheet()->getStyle($aColumn . '1')->getFont()->setBold($aColumn . '1');
                }

                $sheet = $spreadsheet->getActiveSheet();

                $sheet->setCellValue('A1', 'Quotation number');
                $sheet->setCellValue('B1', 'Version');
                $sheet->setCellValue('C1', 'Date');
                $sheet->setCellValue('D1', 'Name');
                $sheet->setCellValue('E1', 'Customer');
                $sheet->setCellValue('F1', 'City');
                $sheet->setCellValue('G1', 'Purchase price');
                $sheet->setCellValue('H1', 'Selling price');
                $sheet->setCellValue('I1', 'Margin %');
                $sheet->setCellValue('J1', 'Margin €');
                $sheet->setCellValue('K1', 'Memo1');
                $sheet->setCellValue('L1', 'Memo2');

                $teller = 2;

		foreach($oQuotationLines as $QuotationLine) {
			$QuotationId = $QuotationLine->getQuotation();
			$CompanyId = $QuotationId->getCustomer();
			if($CompanyId)
			{
				$CompanyCity = $CompanyId->getCompanyCity();
			}
			else
			{
				$CompanyCity = "";
			}

			$marginEuro = $QuotationLine->getSellingPrice() - $QuotationLine->getPurchasePrice();
			if($QuotationLine->getSellingPrice() > 0)
			{
				$marginProcent = ($QuotationLine->getSellingPrice() - $QuotationLine->getPurchasePrice()) / ($QuotationLine->getSellingPrice() / 100);
			}
			else
			{
				$marginProcent = 100;
			}

			$StatusArchive = $entityManager->getRepository(WtStatus::class)->find(7);

				if ($QuotationId->getDate() >= $startDate && $QuotationId->getDate() <= $endDate) {
					$sheet->setCellValue('A' . $teller, $QuotationId->getQuotationNumber());
					$sheet->setCellValue('B' . $teller, $QuotationId->getVersion());
					$sheet->setCellValue('C' . $teller, $QuotationId->getDate());
					$sheet->setCellValue('D' . $teller, $QuotationId->getName());
					$sheet->setCellValue('E' . $teller, $QuotationId->getCustomer());
					$sheet->setCellValue('F' . $teller, $CompanyCity);
					$sheet->setCellValue('G' . $teller, $QuotationLine->getPurchasePrice());
					$sheet->setCellValue('H' . $teller, $QuotationLine->getSellingPrice());
					$sheet->setCellValue('I' . $teller, $marginProcent);
					$sheet->setCellValue('J' . $teller, $marginEuro);
					$sheet->setCellValue('K' . $teller, $QuotationId->getMemo1());
					$sheet->setCellValue('L' . $teller, $QuotationId->getMemo2());
					$teller++;
				}
		}

                $writer = new Xlsx($spreadsheet);

                $fileName = 'export_quotations' . date('Ymd') . '.xlsx';

                $FilePath = $_SERVER['DOCUMENT_ROOT'] . '/Sheets/' . $fileName;

                $writer->save($FilePath);

                $response = new BinaryFileResponse($FilePath);

                $response->setContentDisposition(
                        ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                        $fileName
                );

                return $response;
        }

	/**
	 * @Route("/exportProductLine", name="ExportProductLine")
	 */
	public function exportProductLine(Request $request)
	{
                $this->denyAccessUnlessGranted('ROLE_USER');

		$entityManager = $this->getDoctrine()->getManager();

		$oQuotationLines = $entityManager->getRepository(WtQuotationLine::class)->findAll();

		$startDate = new \DateTime($request->query->get('StartDate'));
		$endDate = new \DateTime($request->query->get('EndDate'));

		$spreadsheet = new Spreadsheet();

		$aColumns = range('A', 'M');

		foreach ($aColumns as $aColumn) {
			$spreadsheet->getActiveSheet()->getColumnDimension($aColumn)->setAutoSize(true);
			$spreadsheet->getActiveSheet()->getStyle($aColumn . '1')->getFont()->setBold($aColumn . '1');
		}

		$sheet = $spreadsheet->getActiveSheet();

		$sheet->setCellValue('A1', 'Quotation number');
		$sheet->setCellValue('B1', 'Version');
		$sheet->setCellValue('C1', 'Product Line');
		$sheet->setCellValue('D1', 'Date');
		$sheet->setCellValue('E1', 'Name');
		$sheet->setCellValue('F1', 'Customer');
		$sheet->setCellValue('G1', 'City');
		$sheet->setCellValue('H1', 'Purchase price');
		$sheet->setCellValue('I1', 'Selling price');
		$sheet->setCellValue('J1', 'Margin %');
		$sheet->setCellValue('K1', 'Margin €');
		$sheet->setCellValue('L1', 'Memo1');
		$sheet->setCellValue('M1', 'Memo2');

		$teller = 2;

		foreach($oQuotationLines as $QuotationLine) {
			$QuotationId = $QuotationLine->getQuotation();
			$ProductLineId = $QuotationLine->getProductLine();

			if($ProductLineId)
			{
				$ProductLineIds = $ProductLineId->getProductLineId();
			}
			else
			{
				$ProductLineIds = "";
			}

			$CompanyId = $QuotationId->getCustomer();
			if($CompanyId)
			{
				$CompanyCity = $CompanyId->getCompanyCity();
			}
			else
			{
				$CompanyCity = "";
			}

			//$oProductLines = $entityManager->getRepository(WtProductLine::class)->find($ProductLineId);

			$marginEuro = $QuotationLine->getSellingPrice() - $QuotationLine->getPurchasePrice();
			if($QuotationLine->getSellingPrice() > 0)
			{
				$marginProcent = ($QuotationLine->getSellingPrice() - $QuotationLine->getPurchasePrice()) / ($QuotationLine->getSellingPrice() / 100);
			}
			else
			{
				$marginProcent = 100;
			}

				if ($QuotationId->getDate() >= $startDate && $QuotationId->getDate() <= $endDate) {

					$sheet->setCellValue('A' . $teller, $QuotationId->getQuotationNumber());
					$sheet->setCellValue('B' . $teller, $QuotationId->getVersion());
					$sheet->setCellValue('C' . $teller, $ProductLineIds);
					$sheet->setCellValue('D' . $teller, $QuotationId->getDate());
					$sheet->setCellValue('E' . $teller, $QuotationId->getName());
					$sheet->setCellValue('F' . $teller, $QuotationId->getCustomer());
					$sheet->setCellValue('G' . $teller, $CompanyCity);
					$sheet->setCellValue('H' . $teller, $QuotationLine->getPurchasePrice());
					$sheet->setCellValue('I' . $teller, $QuotationLine->getSellingPrice());
					$sheet->setCellValue('J' . $teller, $marginProcent);
					$sheet->setCellValue('K' . $teller, $marginEuro);
					$sheet->setCellValue('L' . $teller, $QuotationId->getMemo1());
					$sheet->setCellValue('M' . $teller, $QuotationId->getMemo2());
					$teller++;
				}
		}

		$writer = new Xlsx($spreadsheet);

		$fileName = 'export_product_lines' . date('Ymd') . '.xlsx';

		$FilePath = $_SERVER['DOCUMENT_ROOT'] . '/Sheets/' . $fileName;

		$writer->save($FilePath);

		$response = new BinaryFileResponse($FilePath);

		$response->setContentDisposition(
			ResponseHeaderBag::DISPOSITION_ATTACHMENT,
			$fileName
		);

		return $response;
	}


	/**
	 * @Route("/quotation-new", name="new quotation")
	 */
	public function quotationNew(Request $request)
	{
                $this->denyAccessUnlessGranted('ROLE_USER');

		$entityManager = $this->getDoctrine()->getManager();

		$oCompanys = $entityManager->getRepository(Company::class)->findFirstRowsCompany(10);
		$aUsers = $entityManager->getRepository(SecurityUser::class)->findAll();

		$oQuotation = new WtQuotation();

		$user = $this->getUser();

		$form = $this->createForm(AddQuotationType::class, $oQuotation);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->addFlash('success', 'Quotation saved succesfull!');

			$FormData = $form->getData();

			$QuotationNumber = "WT";

			$QuotationNumber .= $this->getmaxQuotationNum();

			$oQuotation->setQuotationNumber($QuotationNumber);

			$oQuotation->setEmployee($user->getName());

			$QuotationStatusCalculeren = $entityManager->getRepository(WtStatus::class)->find(4);

			$oQuotation->setStatus($QuotationStatusCalculeren);

			$oQuotation->setDate(new \DateTime());

			$oQuotation->setVersion('');

			$entityManager->persist($oQuotation);

			$entityManager->flush();

			return $this->redirect($this->generateUrl('ChangeQuotation', ['quotationNumber' => $QuotationNumber]));
		}

		//dump($aUsers);

		return $this->render('medica_wintrade/quotation-new.html.twig', [
			'Quotation' => $oQuotation,
			'oCompanys' => $oCompanys,
			'form' => $form->createView(),
			'SaveButton' => true,
		]);
	}

        /**
         * @Route("/find-articles/ajax")
         */
        public function findArticles(Request $request)
        {
                if ($request->isXmlHttpRequest() || $request->query->get('showJson') == 1) {

                        $entityManager = $this->getDoctrine()->getManager();

                        $aSearchFields = array();

                        $aSearchFields['articleNumber'] = $request->request->get('articleNumber');
                        $aSearchFields['ArticleNumberSupplier'] = $request->request->get('ArticleNumberSupplier');
                        $aSearchFields['Description'] = $request->request->get('Description');
                        $aSearchFields['Supplier'] = $request->request->get('Supplier');

                        $aArticles = $entityManager->getRepository(WtArticle::class)->filterArticles($aSearchFields);

                        return new JsonResponse($aArticles);
                }
        }

        /**
         * @Route("/add-article", name="Add articles to quotation")
         */
        public function AddArticlesToQuotation(Request $request)
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

                $entityManager = $this->getDoctrine()->getManager();

                $quotationnumber = $request->query->get('QuotationNumber');

                $oQuotation = $entityManager->getRepository(WtQuotation::class)->findByQuotationNumber($quotationnumber);

                $AllFormFields = $request->query->all();

                foreach ($AllFormFields as $ArticleId => $Quantity) {

                        if(substr($ArticleId,0,3) == 'id_' && substr($ArticleId,-6, 6) != '_price') {

                                if ($Quantity > 0) {

                                        $OnlyArticleId = substr($ArticleId, 3);

                                        $PriceIndex = $ArticleId . '_price';

                                        $SellingPrice = $AllFormFields[$PriceIndex];

                                        $this->AddArticles2Quotation($quotationnumber, $oQuotation, $OnlyArticleId, $Quantity, $SellingPrice);
                                }
                        }
                }

                return $this->redirect($this->generateUrl('ChangeQuotation', ['quotationNumber' => $quotationnumber]));
        }

        /**
         * @Route("/add-article/ajax")
         */
        public function addArticleAjax(Request $request)
        {
                if ($request->isXmlHttpRequest() || $request->query->get('showJson') == 1) {

                        $quotationnumber = $request->request->get('quotationnumber');
                        $articlenumber = $request->request->get('articlenumber');
                        $quantity = $request->request->get('quantity');
                        $articleid = $request->request->get('articleid');

			$entityManager = $this->getDoctrine()->getManager();

			$oQuotation = $entityManager->getRepository(WtQuotation::class)->findByQuotationNumber($quotationnumber);

                        $aQuotationArticles = $entityManager->getRepository(WtQuotationLine::class)->findBy(
                        	[
                        		'QuotationNumber' => $quotationnumber,
					'ArticleNumber' => $articlenumber
				]
			);

                        if(sizeof($aQuotationArticles) == 0)
			{
				$this->AddArticles2Quotation($quotationnumber, $oQuotation, $articleid, $quantity);
			}
                        else
			{
				$oQuotationLine = $aQuotationArticles[0];

				$quantityOld = $oQuotationLine->getQuantity();

				$oQuotationLine->setQuantity($quantityOld + $quantity);

				$entityManager->persist($oQuotationLine);

				$entityManager->flush();
			}

                        $description = $oQuotationLine->getDescription();

			if($oQuotationLine->getClassification())
			{
				$quotationLineClassification = $oQuotationLine->getClassification()->getClassificationId();
			}
			else
			{
				$quotationLineClassification = "";
			}

			if($oQuotationLine->getProductLine())
			{
				$quotationLineProductLine = $oQuotationLine->getProductLine()->getProductlineId();
			}
			else
			{
				$quotationLineProductLine = "";
			}

                        $jsonData = array(
                                'quotationnumber' => $quotationnumber,
                                'articlenumber' => $articlenumber,
                                'quantity' => $quantity,
                                'articleid' => $articleid,
				'classification' => $quotationLineClassification,
				'productline' => $quotationLineProductLine,
				'description' => $description,
                        );

                        return new JsonResponse($jsonData);
                }
        }

        /**
         * @Route("/export-excel", name="exportExcel")
         */
        public function exportExcel(Request $request)
        {
                $this->denyAccessUnlessGranted('ROLE_USER');

                $entityManager = $this->getDoctrine()->getManager();

                $oQuotation = $entityManager->getRepository(WtQuotation::class)->findAll();

                $oCompany = $entityManager->getRepository(Company::class)->findFirstRowsCompany(10);

                $optionValue = $request->query->get('SelectCondition');

                $startDate = new \DateTime($request->query->get('StartDate'));
                $endDate = new \DateTime($request->query->get('EndDate'));

                $spreadsheet = new Spreadsheet();

                $aColumns = range('A', 'I');

                foreach ($aColumns as $aColumn) {
                        $spreadsheet->getActiveSheet()->getColumnDimension($aColumn)->setAutoSize(true);
                        $spreadsheet->getActiveSheet()->getStyle($aColumn . '1')->getFont()->setBold($aColumn . '1');
                }

                $sheet = $spreadsheet->getActiveSheet();

                $sheet->setCellValue('A1', 'Quotation number');
                $sheet->setCellValue('B1', 'Version');
                $sheet->setCellValue('C1', 'Date');
                $sheet->setCellValue('D1', 'Id customer');
                $sheet->setCellValue('E1', 'Id contact');
                $sheet->setCellValue('F1', 'Employee');
                $sheet->setCellValue('G1', 'Memo1');
                $sheet->setCellValue('H1', 'Memo2');
                $sheet->setCellValue('I1', 'Status');
                $sheet->setCellValue('J1', 'Name');
                $sheet->setCellValue('K1', 'Name customer');
                $sheet->setCellValue('L1', 'City customer');
                $sheet->setCellValue('M1', 'Quotation Line');
                $sheet->setCellValue('N1', 'Article number');
                $sheet->setCellValue('O1', 'Description');
                $sheet->setCellValue('P1', 'Purchase price');
                $sheet->setCellValue('Q1', 'Min. margin');
                $sheet->setCellValue('R1', 'Pref. margin');
                $sheet->setCellValue('S1', 'Min. price');
                $sheet->setCellValue('T1', 'Pref. price');
                $sheet->setCellValue('U1', 'Selling unit');
                $sheet->setCellValue('V1', 'Quantity');
                $sheet->setCellValue('W1', 'Selling price');
                $sheet->setCellValue('X1', 'Name supplier');
                $sheet->setCellValue('Y1', 'Classification');
                $sheet->setCellValue('Z1', 'Product line');
                $sheet->setCellValue('AA1', 'Art. supplier');
                $sheet->setCellValue('AB1', 'VAT');
                $sheet->setCellValue('AC1', 'Article type');

                $teller = 2;

                foreach ($oQuotation as $Quotation) {
                        $quotationNumber = $Quotation->getQuotationNumber();
                        $aQuotationLine = $entityManager->getRepository(WtQuotationLine::class)->findQuotationLines($quotationNumber);
                        $CompanyId = $Quotation->getCustomer();
                        $Company = $entityManager->getRepository(Company::class)->find($CompanyId);


                        foreach($aQuotationLine as $QuotationLine) {
                                $ProductLineId = $QuotationLine->getProductLine();
                                $oProductLines = $entityManager->getRepository(WtProductLine::class)->find($ProductLineId);

                                if ($optionValue == 'Date') {
                                        if ($Quotation->getDate() >= $startDate && $Quotation->getDate() <= $endDate) {
                                                $sheet->setCellValue('A' . $teller, $Quotation->getQuotationNumber());
                                                $sheet->setCellValue('B' . $teller, $Quotation->getVersion());
                                                $sheet->setCellValue('C' . $teller, $Quotation->getDate());
                                                $sheet->setCellValue('D' . $teller, $CompanyId->getCompanyId());
                                                $sheet->setCellValue('E' . $teller, $Quotation->getCompanyContacts());
                                                $sheet->setCellValue('F' . $teller, $Quotation->getEmployee());
                                                $sheet->setCellValue('G' . $teller, $Quotation->getMemo1());
                                                $sheet->setCellValue('H' . $teller, $Quotation->getMemo2());
                                                $sheet->setCellValue('I' . $teller, $Quotation->getStatus());
                                                $sheet->setCellValue('J' . $teller, $Quotation->getName());
                                                $sheet->setCellValue('K' . $teller, $oCompany->getCompanyName());
                                                $sheet->setCellValue('L' . $teller, $oCompany->getCompanyCity());
                                                $sheet->setCellValue('M' . $teller, $QuotationLine->getId());
                                                $sheet->setCellValue('N' . $teller, $QuotationLine->getArticleNumber());
                                                $sheet->setCellValue('O' . $teller, $QuotationLine->getDescription());
                                                $sheet->setCellValue('P' . $teller, $QuotationLine->getPurchasePrice());
                                                $sheet->setCellValue('Q' . $teller, $QuotationLine->getMinMargin());
                                                $sheet->setCellValue('R' . $teller, $QuotationLine->getPreferredMargin());
                                                $sheet->setCellValue('S' . $teller, $QuotationLine->getMinPrice());
                                                $sheet->setCellValue('T' . $teller, $QuotationLine->getPreferredPrice());
                                                $sheet->setCellValue('U' . $teller, $QuotationLine->getSellingUnit());
                                                $sheet->setCellValue('V' . $teller, $QuotationLine->getQuantity());
                                                $sheet->setCellValue('W' . $teller, $QuotationLine->getSellingPrice());
                                                $sheet->setCellValue('X' . $teller, $QuotationLine->getNameSupplier());
                                                $sheet->setCellValue('Y' . $teller, $QuotationLine->getClassification());
                                                $sheet->setCellValue('Z' . $teller, $oProductLines->getProductineId());
                                                $sheet->setCellValue('AA' . $teller, $QuotationLine->getArticleNumberSupplier());
                                                $sheet->setCellValue('AB' . $teller, $QuotationLine->getVAT());
                                                $sheet->setCellValue('AC' . $teller, $QuotationLine->getArticleType());
                                                $teller++;
                                        }
                                } else {
                                        $sheet->setCellValue('A' . $teller, $Quotation->getQuotationNumber());
                                        $sheet->setCellValue('B' . $teller, $Quotation->getVersion());
                                        $sheet->setCellValue('C' . $teller, $Quotation->getDate());
                                        $sheet->setCellValue('D' . $teller, $Quotation->getCustomer());
                                        $sheet->setCellValue('E' . $teller, $Quotation->getCompanyContacts());
                                        $sheet->setCellValue('F' . $teller, $Quotation->getEmployee());
                                        $sheet->setCellValue('G' . $teller, $Quotation->getMemo1());
                                        $sheet->setCellValue('H' . $teller, $Quotation->getMemo2());
                                        $sheet->setCellValue('I' . $teller, $Quotation->getName());
                                        //$sheet->setCellValue('J' . $teller, $oQuotationLines->getDescription());
                                        $teller++;
                                }
                        }
                }

                $writer = new Xlsx($spreadsheet);

                $fileName = 'ExportQuotations' . '.xlsx';

                $FilePath = $_SERVER['DOCUMENT_ROOT'] . '/Sheets/' . $fileName;

                $writer->save($FilePath);

                $response = new BinaryFileResponse($FilePath);

                $response->setContentDisposition(
                        ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                        $fileName
                );

                return $response;
        }

	/**
	 * @Route("/quotation-calculation/{quotationNumber}", name="QuotationCalculation")
	 */
	public function quotationCalculation($quotationNumber, Request $request)
	{
                $this->denyAccessUnlessGranted('ROLE_USER');

		$entityManager = $this->getDoctrine()->getManager();

		$oQuotation = $entityManager->getRepository(WtQuotation::class)->findByQuotationNumber($quotationNumber);

		$oCompanys = $entityManager->getRepository(Company::class)->findFirstRowsCompany(10);

		$oQuotationLines = $oQuotation->getWtQuotationLines();

		$user = $this->getUser();

		$version = $oQuotation->getVersion();

		$nextVersion = $this->getNextVersion($version);

		$form = $this->createForm(QuotationCalculationType::class, $oQuotation);

		$form->handleRequest($request);

		$totalMarginEuro = 0;

		$totalMarginProcent = 0;

		foreach($oQuotationLines as $QuotationLine) {
			$aMarginEuro[] = $QuotationLine->getSellingPrice() - $QuotationLine->getPurchasePrice();

			$quantity = $QuotationLine->getQuantity();
			$allMarginEuro[] = ($QuotationLine->getSellingPrice() * $quantity) - ($QuotationLine->getPurchasePrice() * $quantity);

			if($QuotationLine->getSellingPrice() > 0)
			{
				$marginProcent[] = ($QuotationLine->getSellingPrice() - $QuotationLine->getPurchasePrice()) / ($QuotationLine->getSellingPrice() / 100);
			}
			else
			{
				$marginProcent[] = 100;
			}

			$sellingPriceTotal[] = $QuotationLine->getQuantity() * $QuotationLine->getSellingPrice();
		}

		$totalMarginEuro = array_sum($allMarginEuro);

		$totalSellingPrice = array_sum($sellingPriceTotal);

		$totalMarginProcent = $totalMarginEuro / ($totalSellingPrice / 100);


		if ($form->isSubmitted() && $form->isValid()) {
			$this->addFlash('success', 'Quotation saved succesfull!');

			$FormData = $form->getData();

			$oQuotation->setEmployee($user->getName());

			$oQuotation->setVersion($nextVersion);

			$entityManager->persist($oQuotation);

			$entityManager->flush();

			$form = $this->createForm(QuotationCalculationType::class, $oQuotation);
		}

		return $this->render('medica_wintrade/quotation-calculate.html.twig', [
			'Quotation' => $oQuotation,
			'form' => $form->createView(),
			'quotationNumber' => $quotationNumber,
			'QuotationLines' => $oQuotationLines,
			//'ProductLine' => $productLineId,
			'oCompanys' => $oCompanys,
			'marginEuro' => $aMarginEuro,
			'marginProcent' => $marginProcent,
			'totalMarginEuro' => $totalMarginEuro,
			'totalMarginProcent' => $totalMarginProcent,
			'sellingPriceTotal' => $sellingPriceTotal,
			'SaveButton' => true,
		]);
	}

	/**
	 * @Route("/quotation-calculation-export/{quotationNumber}", name="QuotationCalculationExport")
	 */
	public function quotationCalculationExport($quotationNumber, Request $request)
	{
                $this->denyAccessUnlessGranted('ROLE_USER');

		$entityManager = $this->getDoctrine()->getManager();

		$oQuotation = $entityManager->getRepository(WtQuotation::class)->findByQuotationNumber($quotationNumber);

		$CompanyId = $oQuotation->getCustomer();

		$oQuotationLines = $oQuotation->getWtQuotationLines();

		$spreadsheet = new Spreadsheet();

		$aColumns = range('A', 'I');

		foreach ($aColumns as $aColumn) {
			$spreadsheet->getActiveSheet()->getColumnDimension($aColumn)->setAutoSize(true);
			$spreadsheet->getActiveSheet()->getStyle($aColumn . '5')->getFont()->setBold($aColumn . '5');
		}

		$sheet = $spreadsheet->getActiveSheet();

		$sheet->setCellValue('A1', 'Quotation number');
		$sheet->setCellValue('B1', $quotationNumber);
		$sheet->setCellValue('D1', 'Customer');
		$sheet->setCellValue('E1', $CompanyId->getCompanyName());
		$sheet->setCellValue('F1', 'Date');
		$sheet->setCellValue('G1', $oQuotation->getDate());
		$sheet->setCellValue('D2', 'City');
		$sheet->setCellValue('E2', $CompanyId->getCompanyCity());
		$sheet->setCellValue('A3', 'Memo1');
		$sheet->setCellValue('B3', $oQuotation->getMemo1());
		$sheet->setCellValue('A5', 'Article');
		$sheet->setCellValue('B5', 'Description');
		$sheet->setCellValue('C5', 'Product line');
		$sheet->setCellValue('D5', 'Quantity');
		$sheet->setCellValue('E5', 'Purchase price');
		$sheet->setCellValue('F5', 'Min. margin');
		$sheet->setCellValue('G5', 'Selling price');
		$sheet->setCellValue('H5', 'Margin %');
		$sheet->setCellValue('I5', 'Margin €');

		$spreadsheet->getActiveSheet()->getStyle('E2:E100')
			->getNumberFormat()
			->setFormatCode('€ #,##0.00');

		$spreadsheet->getActiveSheet()->getStyle('G2:G100')
			->getNumberFormat()
			->setFormatCode('€ #,##0.00');

		$spreadsheet->getActiveSheet()->getStyle('i2:i100')
			->getNumberFormat()
			->setFormatCode('€ #,##0.00');

		$teller = 6;

		foreach($oQuotationLines as $QuotationLine) {

			$ProductLineId = $QuotationLine->getProductLine();

			if($ProductLineId) {
				$ProductLineIds = $ProductLineId->getProductLineId();
			}
			else {
				$ProductLineIds = "";
			}

			$marginEuro = $QuotationLine->getSellingPrice() - $QuotationLine->getPurchasePrice();

			if($QuotationLine->getSellingPrice() > 0) {
				$marginProcent = ($QuotationLine->getSellingPrice() - $QuotationLine->getPurchasePrice()) / ($QuotationLine->getSellingPrice() / 100);
			}
			else {
				$marginProcent = 100;
			}

			$sheet->setCellValue('A' . $teller, $QuotationLine->getArticleNumber());
			$sheet->setCellValue('B' . $teller, $QuotationLine->getDescription());
			$sheet->setCellValue('C' . $teller, $ProductLineIds);
			$sheet->setCellValue('D' . $teller, $QuotationLine->getQuantity());
			$sheet->setCellValue('E' . $teller, $QuotationLine->getPurchasePrice());
			$sheet->setCellValue('F' . $teller, $QuotationLine->getMinMargin());
			$sheet->setCellValue('G' . $teller, $QuotationLine->getSellingPrice());
			$sheet->setCellValue('H' . $teller, $marginProcent);
			$sheet->setCellValue('I' . $teller, $marginEuro);
			$teller++;
		}

		$writer = new Xlsx($spreadsheet);

		$fileName = 'export_quotations_calculation_' . $quotationNumber . '.xlsx';

		$FilePath = $_SERVER['DOCUMENT_ROOT'] . '/Sheets/' . $fileName;

		$writer->save($FilePath);

		$response = new BinaryFileResponse($FilePath);

		$response->setContentDisposition(
			ResponseHeaderBag::DISPOSITION_ATTACHMENT,
			$fileName
		);

		return $response;
	}

	/**
	 * @Route("/quotation-calculation-status/{quotationNumber}", name="QuotationCalculationStatus")
	 */
	public function quotationCalculationStatus($quotationNumber, Request $request)
	{
                $this->denyAccessUnlessGranted('ROLE_USER');

		$entityManager = $this->getDoctrine()->getManager();

		$oQuotation = $entityManager->getRepository(WtQuotation::class)->findByQuotationNumber($quotationNumber);

		$QuotationStatusOffer = $entityManager->getRepository(WtStatus::class)->find(6);

		$oQuotation->setStatus($QuotationStatusOffer);

		$entityManager->persist($oQuotation);

		$entityManager->flush();

		return $this->redirect($this->generateUrl('ChangeQuotation', ['quotationNumber' => $quotationNumber]));
	}

	private function getmaxQuotationNum()
	{
		$entityManager = $this->getDoctrine()->getManager();

		$oMaxQuotationNum = $entityManager->getRepository(WtCounters::class)->find(1);

		$MaxQuotationNum = $oMaxQuotationNum->getMaxQuotationNumber();

		$oMaxQuotationNum->setMaxQuotationNumber($MaxQuotationNum + 1);

		$entityManager->persist($oMaxQuotationNum);

		$entityManager->flush();

		return $MaxQuotationNum;
	}

        private function getNextVersion($Version)
        {
                $aLetters = range('A', 'Z');

                if($Version === '' || $Version === 0)
		{
			$Version = 'A';
		}

		if($Version === 'Z')
		{
			$Version = 'Z';
		}
		else {
			$PositionLastLetter = array_search($Version, $aLetters);

			$Version = $aLetters[$PositionLastLetter + 1];
		}

                return $Version;
        }

        private function AddArticles2Quotation($Quotationnumber, $oQuotation, $articleid, $Quantity, $SellingPrice = 0)
        {
                $entityManager = $this->getDoctrine()->getManager();

                $oArticle = $entityManager->getRepository(WtArticle::class)->find($articleid);

		$aQuotationLine = $entityManager->getRepository(WtQuotationLine::class)->findQuotationLine($Quotationnumber, $oQuotation->getVersion(), $oArticle->getArticleNumber());

                if(sizeof($aQuotationLine) > 0) {

			$oQuotationLine = $aQuotationLine[0];

			$oldQuantity = $oQuotationLine->getQuantity($Quantity);

			$oQuotationLine->setQuantity($Quantity + $oldQuantity);
		}
		else {
			$oQuotationLine = new WtQuotationLine();
			$oQuotationLine->setQuotation($oQuotation);
			$oQuotationLine->setClassification($oArticle->getClassification());
			$oQuotationLine->setProductLine($oArticle->getProductline());
			$oQuotationLine->setArticleNumber($oArticle->getArticleNumber());
			$oQuotationLine->setDescription($oArticle->getDescription());
			$oQuotationLine->setPurchasePrice($oArticle->getPurchasePrice());
			$oQuotationLine->setMinMargin($oArticle->getMinMargin());
			$oQuotationLine->setPreferredMargin($oArticle->getPreferredMargin());
			$oQuotationLine->setPreferredPrice($oArticle->getPreferredPrice());
			$oQuotationLine->setMinPrice($oArticle->getMinimalPrice());
			$oQuotationLine->setSellingUnit($oArticle->getPackingUnit());
			$oQuotationLine->setQuantity($Quantity);
			$oQuotationLine->setSellingPrice($SellingPrice);
			$oQuotationLine->setNameSupplier($oArticle->getNameSupplier());
			$oQuotationLine->setVAT($oArticle->getVAT());
			$oQuotationLine->setArticleType($oArticle->getArticleType());
			$oQuotationLine->setArticleNumberSupplier($oArticle->getArticleNumberSupplier());
		}

                $entityManager->persist($oQuotationLine);
                $entityManager->flush();

                return $oQuotationLine;
        }

        private function getmaxArticleNum()
	{
		$entityManager = $this->getDoctrine()->getManager();

		$oMaxArticleNum = $entityManager->getRepository(WtCounters::class)->find(1);

		$MaxArticleNum = $oMaxArticleNum->getMaxArticle();

		$oMaxArticleNum->setMaxArticle($MaxArticleNum + 1);

		$entityManager->persist($oMaxArticleNum);

		$entityManager->flush();

		return $MaxArticleNum;
	}
}