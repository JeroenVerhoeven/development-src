<?php

namespace App\Repository;

use App\Entity\ComponentSupplier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ComponentSupplier|null find($id, $lockMode = null, $lockVersion = null)
 * @method ComponentSupplier|null findOneBy(array $criteria, array $orderBy = null)
 * @method ComponentSupplier[]    findAll()
 * @method ComponentSupplier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComponentSupplierRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, ComponentSupplier::class);
	}

	public function findWithSupplierDescription()
	{
		return $this->createQueryBuilder('c')
			->andWhere('c.Name IS NOT NULL')
			->andWhere("c.Name !=''")
			->orderBy('c.Name', 'ASC')
			->getQuery()
			->getResult();
	}

	// /**
	//  * @return ComponentSupplier[] Returns an array of ComponentSupplier objects
	//  */
	/*
	public function findByExampleField($value)
	{
	    return $this->createQueryBuilder('c')
		->andWhere('c.exampleField = :val')
		->setParameter('val', $value)
		->orderBy('c.id', 'ASC')
		->setMaxResults(10)
		->getQuery()
		->getResult()
	    ;
	}
	*/

	/*
	public function findOneBySomeField($value): ?ComponentSupplier
	{
	    return $this->createQueryBuilder('c')
		->andWhere('c.exampleField = :val')
		->setParameter('val', $value)
		->getQuery()
		->getOneOrNullResult()
	    ;
	}
	*/
}
