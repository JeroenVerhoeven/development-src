<?php

namespace App\Repository;

use App\Entity\WtQuotation;
use App\Entity\Company;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method WtQuotation|null find($id, $lockMode = null, $lockVersion = null)
 * @method WtQuotation|null findOneBy(array $criteria, array $orderBy = null)
 * @method WtQuotation[]    findAll()
 * @method WtQuotation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WtQuotationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WtQuotation::class);
    }

        public function findByFirst($limit)
        {
                return $this->createQueryBuilder('w')
                        ->orderBy('w.QuotationNumber', 'DESC')
                        ->setMaxResults($limit)
                        ->getQuery()
                        ->getResult();
        }

        public function findByQuotationNumber($QuotationNumber)
        {
                return $this->createQueryBuilder('w')
                        ->andWhere('w.QuotationNumber = :QuotationNumber')
                        ->setParameter(':QuotationNumber', $QuotationNumber)
			->orderBy('w.QuotationNumber', 'DESC')
                        ->getQuery()
                        ->getOneOrNullResult();
        }

        public function filterQuotations($aSearchfield)
        {
                $QueryBuilder =  $this->createQueryBuilder('w')
			->orderBy('w.QuotationNumber', 'DESC');

                if ($aSearchfield['searchStatus'] != '-1') {
                        $QueryBuilder->leftJoin('w.Status','co');
                        $QueryBuilder->andWhere('co.id = :StatusId');
                        $QueryBuilder->setParameter('StatusId', $aSearchfield['searchStatus']);
                }

                if(!empty($aSearchfield['searchCustomer'])) {
                        $QueryBuilder->leftJoin('w.Customer','b');
                        $QueryBuilder->andWhere('b.CompanyName LIKE  :CompanyName');
                        $QueryBuilder->setParameter("CompanyName", '%' . $aSearchfield['searchCustomer'] . '%');
                }

                if(!empty($aSearchfield['searchArticleNumber'])) {
                        $QueryBuilder->leftJoin('w.wtQuotationLines','d');
                        $QueryBuilder->andWhere('d.ArticleNumber LIKE  :ArticleNumber');
                        $QueryBuilder->setParameter("ArticleNumber", '%' . $aSearchfield['searchArticleNumber'] . '%');
                }

                if($aSearchfield['searchProductLine'] != '-1') {
                        $QueryBuilder->leftJoin('w.wtQuotationLines','ql');
                        $QueryBuilder->leftJoin('ql.ProductLine','pl');
                        $QueryBuilder->andWhere('pl.id LIKE  :ProductLineId');
                        $QueryBuilder->setParameter("ProductLineId", $aSearchfield['searchProductLine']);
                }

                if(!empty($aSearchfield['searchQuotation'])) {
                        $QueryBuilder->andWhere('w.QuotationNumber LIKE  :QuotationNumber');
                        $QueryBuilder->setParameter("QuotationNumber", '%' . $aSearchfield['searchQuotation'] . '%');
                }

                if(!empty($aSearchfield['searchName'])) {
                        $QueryBuilder->andWhere('w.Name LIKE  :Name');
                        $QueryBuilder->setParameter("Name", '%' . $aSearchfield['searchName'] . '%');
                }

                if(!empty($aSearchfield['searchDate'])) {
                        $QueryBuilder->andWhere('w.Date LIKE  :Date');
                        $QueryBuilder->setParameter("Date", '%' . $aSearchfield['searchDate'] . '%');
                }

                return $QueryBuilder->getQuery()->getResult();

        }

    // /**
    //  * @return WtQuotation[] Returns an array of WtQuotation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WtQuotation
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
