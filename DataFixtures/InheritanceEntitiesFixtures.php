<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\ComponentGroup;
use App\Entity\ComponentStatus;
use App\Entity\ComponentClassification;
use App\Entity\ComponentSupplier;
use App\Entity\ComponentType;
use App\Entity\Component;

use App\Entity\CptBurden;
use App\Entity\CptSurgeryType;
use App\Entity\CptStatus;
use App\Entity\Cpt;

class InheritanceEntitiesFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

    }
}
