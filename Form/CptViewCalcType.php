<?php

namespace App\Form;

use App\Entity\Cpt;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CptViewCalcType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
		->add('CptNumber', TextType::class, [
			'disabled' => true,
		])
		->add('Margin', TextType::class, [
			'disabled' => true,
		])
		->add('Burden', TextType::class, [
			'disabled' => true,
		])
	;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cpt::class,
        ]);
    }
}
