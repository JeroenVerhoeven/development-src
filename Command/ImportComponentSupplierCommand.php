<?php
/**
 * Created by PhpStorm.
 * User: Jeroen
 * Date: 27-8-2019
 * Time: 04:43
 */

namespace App\Command;

use App\Entity\ComponentSupplier;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportComponentSupplierCommand extends Command
{
	protected static $defaultName = 'medica:import-component-supplier';

	private $entityManager;

	private $container;

	public function __construct(ContainerInterface $container)
	{
		parent::__construct();
		$this->container = $container;
	}

	protected function configure()
	{
		$this
			->setDescription("Import Component supplier")
			->setHelp("This command allows you to import all Component suppliers")
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$output->writeln("Start import import-component-supplier");

		$InputPath = getcwd() . '/src/Command/ImportData/';

		$InputFile = "tbl_leveranciers.csv";

		$InputPathFile = $InputPath . $InputFile;

		if(!file_exists($InputPathFile)) {

			$LogData = "ERROR: LET OP! Bestand " . $InputPathFile . " bestaat niet";

			$output->writeln("<info>". $LogData . "</info>");

			exit;
		}

		$this->entityManager = $this->container->get('doctrine')->getManager();

		$handle = fopen($InputPathFile, "r");

		if (empty($handle) === false) {

			while (($data = fgetcsv($handle, 999999, "|")) !== FALSE) {

				$aContent['component_supplier_id'] = (int) $data[0];
				$aContent['name'] = $data[1];

				if(!$this->existsInDb($aContent['name']))
				{
					$ComponentSupplier = new ComponentSupplier();
					$ComponentSupplier->setName($aContent['name']);
					$ComponentSupplier->setSupplierId($aContent['component_supplier_id']);

					$this->entityManager->persist($ComponentSupplier);
				}
			}

			$this->entityManager->flush();
		}

		$output->writeln("Einde import import-component-supplier");
	}

	private function existsInDb($name)
	{
		$result = $this->entityManager->getRepository(ComponentSupplier::class)->findBy(
			['Name' => $name]
		);

		if($result) {

			return true;
		}
		return false;
	}
}