<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CptSurgeryTypeRepository")
 */
class CptSurgeryType
{
	public function __toString()
	{
		return $this->getDescription();
	}

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $TosId;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $Description;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Cpt", mappedBy="Customer")
	 */
	private $cpts;

	public function __construct()
	{
		$this->cpts = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getTosId(): ?int
	{
		return $this->TosId;
	}

	public function setTosId(int $TosId): self
	{
		$this->TosId = $TosId;

		return $this;
	}

	public function getDescription(): ?string
	{
		return $this->Description;
	}

	public function setDescription(string $Description): self
	{
		$this->Description = $Description;

		return $this;
	}

	/**
	 * @return Collection|Cpt[]
	 */
	public function getCpts(): Collection
	{
		return $this->cpts;
	}

	public function addCpt(Cpt $cpt): self
	{
		if (!$this->cpts->contains($cpt)) {
			$this->cpts[] = $cpt;
			$cpt->setCustomer($this);
		}

		return $this;
	}

	public function removeCpt(Cpt $cpt): self
	{
		if ($this->cpts->contains($cpt)) {
			$this->cpts->removeElement($cpt);
			// set the owning side to null (unless already changed)
			if ($cpt->getCustomer() === $this) {
				$cpt->setCustomer(null);
			}
		}

		return $this;
	}
}
