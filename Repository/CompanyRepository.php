<?php

namespace App\Repository;

use App\Entity\Company;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Company|null find($id, $lockMode = null, $lockVersion = null)
 * @method Company|null findOneBy(array $criteria, array $orderBy = null)
 * @method Company[]    findAll()
 * @method Company[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, Company::class);
	}


	public function findFirstRowsCompany($MaxResults)
	{
		return $this->createQueryBuilder('c')
			->select('c.CompanyName', 'c.CompanyStreet', 'c.CopmpanyZipcode', 'c.CompanyCity', 'c.CompanyPhone',  'c.id as CompanyId', 'co.ContactName', 'co.id as CompanyContactId')
			->leftJoin('c.companyContacts','co')
			->orderBy('c.CompanyName', 'ASC')
			->setMaxResults($MaxResults)
			->getQuery()
			->getResult();
	}

        public function findFirstRowsComponent($MaxResults)
        {
                return $this->createQueryBuilder('c')
                        ->select(
                                'c.id',
                                'c.ComponentNumber',
                                'c.ComponentNumberSupplier',
                                'c.PuchasePrice',
                                'c.Description as ComponentDescription',
                                'co.Name',
                                'b.Description as ComponentGroup',
                                'd.Description as Status',
                                'e.Description as Classification'
                        )
                        ->leftJoin('c.Supplier','co')
                        ->leftJoin('c.ComponentGroup','b')
                        ->leftJoin('c.Status','d')
                        ->leftJoin('c.Classification','e')
                        ->orderBy('co.Name', 'ASC')
                        ->andWhere('co.Name IS NOT NULL')
                        ->andWhere("c.Supplier != ''")
                        ->setMaxResults($MaxResults)
                        ->getQuery()
                        ->getResult();
        }

        public function searchCompanies($aSearchFields)
        {
                return $this->createQueryBuilder('c')
                        ->select('c.CompanyName', 'c.CompanyStreet', 'c.CopmpanyZipcode', 'c.CompanyCity', 'c.CompanyPhone', 'c.id as CompanyId', 'co.ContactName', 'co.id as CompanyContactId')
                        ->leftJoin('c.companyContacts','co')

                        ->andWhere('c.CompanyName LIKE :Companyname')
                        ->setParameter('Companyname', '%' . $aSearchFields['searchCompanyname'] . '%')

                        ->andWhere('c.CopmpanyZipcode LIKE :searchZipCode')
                        ->setParameter('searchZipCode', '%' . $aSearchFields['searchZipCode'] . '%')

                        ->andWhere('c.CompanyCity LIKE :CompanyCity')
                        ->setParameter('CompanyCity', '%' . $aSearchFields['searchCity'] . '%')

			->andWhere('co.ContactName LIKE :ContactName')
			->setParameter('ContactName', '%' . $aSearchFields['searchCompanyContact'] . '%')

                        ->orderBy('c.CompanyName', 'ASC')
                        ->getQuery()
                        ->getResult()
                        ;
        }


	// /**
	//  * @return Company[] Returns an array of Company objects
	//  */
	/*
	public function findByExampleField($value)
	{
	    return $this->createQueryBuilder('c')
		->andWhere('c.exampleField = :val')
		->setParameter('val', $value)
		->orderBy('c.id', 'ASC')
		->setMaxResults(10)
		->getQuery()
		->getResult()
	    ;
	}
	*/

	/*
	public function findOneBySomeField($value): ?Company
	{
	    return $this->createQueryBuilder('c')
		->andWhere('c.exampleField = :val')
		->setParameter('val', $value)
		->getQuery()
		->getOneOrNullResult()
	    ;
	}
	*/
}
