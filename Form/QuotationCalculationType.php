<?php

namespace App\Form;

use App\Entity\WtQuotation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\PostSubmitEvents;
use Symfony\Component\Form;

class QuotationCalculationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('QuotationNumber', TextType::class, [
            	'disabled' => true,
	    ])
            ->add('Version', TextType::class, [
            	'disabled' => true,
	    ])
            ->add('Date', DateType::class, [
            	'format' => 'yyyy-MM-dd'
	    ])
            ->add('Memo1', TextAreaType::class, [
            	'attr' => ['class' => 'tinymce'],
	    	'required' => false,
	    ])
            ->add('Memo2', TextAreaType::class, [
            	'attr' => ['class' => 'tinymce'],
	    	'required' => false,
	    ])
            ->add('Name', TextType::class, [
            	'label' => 'Quotation name',
	    ])
            ->add('Customer')
            ->add('CompanyContacts')
	    ->add('Save', SubmitType::class, [
			'label' => 'Save quotation'
		])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WtQuotation::class,
        ]);
    }
}
