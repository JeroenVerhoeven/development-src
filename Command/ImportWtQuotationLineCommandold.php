<?php
/**
 * Created by PhpStorm.
 * User: Jeroen
 * Date: 27-8-2019
 * Time: 04:43
 */

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportWtQuotationLineCommandold extends Command
{
        protected static $defaultName = 'medica:import-wintrade-quotationline-old';

        private $entityManager;

        private $container;

        public function __construct(ContainerInterface $container)
        {
                parent::__construct();
                $this->container = $container;
        }

        protected function configure()
        {
                $this
                        ->setDescription("Import Wintrade quotation line")
                        ->setHelp("This command allows you to import all quotation lines")
                ;
        }

        protected function execute(InputInterface $input, OutputInterface $output)
        {
                $output->writeln("Start import import-wintrade-quotationlines");

                $InputPath = getcwd() . '/src/Command/ImportData/';

                $InputFile = "tbl_wtofferteregels.csv";

                $InputPathFile = $InputPath . $InputFile;

                if(!file_exists($InputPathFile)) {

                        $LogData = "ERROR: LET OP! Bestand " . $InputPathFile . " bestaat niet";

                        $output->writeln("<info>". $LogData . "</info>");

                        exit;
                }

                $this->entityManager = $this->container->get('doctrine')->getManager();

                $handle = fopen($InputPathFile, "r");

                if (empty($handle) === false) {

                        while (($data = fgetcsv($handle, 999999, ";")) !== FALSE) {

                                $aContent['Quotationnumber'] = $data[0];
                                $aContent['Version'] = $data[1];
                                $aContent['Quotationline'] = $data[2];
                                $aContent['ArticleNumber'] = $data[3];
                                $aContent['Description'] = $data[4];
                                $aContent['CostPrice'] = $data[5];
                                $aContent['MinMargin'] = $data[6];
                                $aContent['PrefMargin'] = $data[7];
                                $aContent['PrefPrice'] = $data[8];
                                $aContent['MinPrice'] = $data[9];
                                $aContent['SellingUnit'] = $data[10];
                                $aContent['Quantity'] = $data[11];
                                $aContent['SellingPrice'] = $data[12];
                                $aContent['NameSupplier'] = $data[13];
                                $aContent['Classification'] = $data[14];
                                $aContent['ProductLine'] = $data[15];
                                $aContent['ArticleNumberSupplier'] = $data[16];
                                $aContent['VAT'] = $data[17];
                                $aContent['ArticleType'] = $data[18];

//                                if(!$this->existsInDb($aContent['Quotationline']))
//                                {
                                        $ComponentType = new ComponentType();
                                        $ComponentType->setDescription($aContent['description']);

                                        $this->entityManager->persist($ComponentType);
//                                }
                        }

                        $this->entityManager->flush();
                }

                $output->writeln("Einde import import-wintrade-quotationlines");
        }

        private function existsInDb($description)
        {
                $result = $this->entityManager->getRepository(ComponentType::class)->findBy(
                        ['Description' => $description]
                );

                if($result) {

                        return true;
                }
                return false;
        }
}