<?php

namespace App\Form;

use App\Entity\Company;
use App\Entity\Cpt;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class CptFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            	->add('Company', TextType::class, [
                        'disabled' => true,
    		])

		->add('CompanyContact', TextType::class, [
			'label' => 'Client Name',
                        'disabled' => true,
    		])

		->add('EndUser', TextType::class, [
			'label' => 'Enduser Name',
			'disabled' => true,
		])

		->add('EndUserPlace', TextType::class, [
			'label' => 'Enduser City',
			'disabled' => true,
		])

		->add('Employee', TextType::class, [
			'label' => 'Employee Medica',
			'disabled' => true,
		])

		->add('YearUsage', TextType::class, [
			'label' => 'Yearusage',
			'disabled' => true,
		])

            	->add('TypeOfSurgery', TextType::class, [
		    'disabled' => true,
	    	])

		->add('Quote', TextType::class, [
			'disabled' => true,
		])

		->add('Memo', TextType::class, [
			'disabled' => true,
		])

//            ->add('Packaging')
//            ->add('NumberInPackage')
//            ->add('Margin')
//            ->add('Burden')
//            ->add('Date')
//            ->add('SalesPrice')
//            ->add('OriginalSalesPrice')
//            ->add('OriginalMargin')
//            ->add('CreationDate')
//            ->add('CptName')
//            ->add('CptStatus')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cpt::class,
        ]);
    }
}
