<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191211141029 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX component_number ON component');
        $this->addSql('CREATE INDEX component_number ON component (component_number)');
        $this->addSql('DROP INDEX cpt_number ON cpt');
        $this->addSql('CREATE INDEX cpt_number ON cpt (cpt_number)');
        $this->addSql('DROP INDEX cpt_Component_number ON cpt_componenten');
        $this->addSql('DROP INDEX component_number ON cpt_componenten');
        $this->addSql('CREATE INDEX cpt_Component_number ON cpt_componenten (cpt_number, component_number)');
        $this->addSql('CREATE INDEX component_number ON cpt_componenten (component_number)');
        $this->addSql('ALTER TABLE wt_article ADD productline_id INT NOT NULL, DROP productline');
        $this->addSql('ALTER TABLE wt_article ADD CONSTRAINT FK_4900543CD634D365 FOREIGN KEY (productline_id) REFERENCES wt_product_line (id)');
        $this->addSql('CREATE INDEX IDX_4900543CD634D365 ON wt_article (productline_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX component_number ON component');
        $this->addSql('CREATE INDEX component_number ON component (component_number(191))');
        $this->addSql('DROP INDEX cpt_number ON cpt');
        $this->addSql('CREATE INDEX cpt_number ON cpt (cpt_number(191))');
        $this->addSql('DROP INDEX cpt_Component_number ON cpt_componenten');
        $this->addSql('DROP INDEX component_number ON cpt_componenten');
        $this->addSql('CREATE INDEX cpt_Component_number ON cpt_componenten (cpt_number(191), component_number(191))');
        $this->addSql('CREATE INDEX component_number ON cpt_componenten (component_number(191))');
        $this->addSql('ALTER TABLE wt_article DROP FOREIGN KEY FK_4900543CD634D365');
        $this->addSql('DROP INDEX IDX_4900543CD634D365 ON wt_article');
        $this->addSql('ALTER TABLE wt_article ADD productline VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, DROP productline_id');
    }
}
