<?php

namespace App\Form;

use App\Entity\SecurityUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class AddUserType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('email')
			->add('password', PasswordType::class)
			->add('Name')
			->add('roles', ChoiceType::class, array(
					'attr'  =>  array('class' => 'form-control',
						'style' => 'margin:5px 0;'),
					'choices' =>
						array
						(
							'Administrator' => array
							(
								'Yes' => 'ROLE_ADMIN',
							),
							'Viewer' => array
							(
								'Yes' => 'ROLE_VIEWER'
							),
						)
				,
					'multiple' => true,
					'required' => true,
				)
			)
			->add('Save', SubmitType::class, ['label' => 'Register'])
		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => SecurityUser::class,
		]);
	}
}
