<?php

namespace App\Repository;

use App\Entity\ComponentGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ComponentGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method ComponentGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method ComponentGroup[]    findAll()
 * @method ComponentGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComponentGroupRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, ComponentGroup::class);
	}

	public function findWithDescription()
	{
		return $this->createQueryBuilder('c')
			->andWhere('c.Description IS NOT NULL')
			->andWhere("c.Description !=''")
			->orderBy('c.Description', 'ASC')
			->getQuery()
			->getResult();
	}

	// /**
	//  * @return ComponentGroup[] Returns an array of ComponentGroup objects
	//  */
	/*
	public function findByExampleField($value)
	{
	    return $this->createQueryBuilder('c')
		->andWhere('c.exampleField = :val')
		->setParameter('val', $value)
		->orderBy('c.id', 'ASC')
		->setMaxResults(10)
		->getQuery()
		->getResult()
	    ;
	}
	*/

	/*
	public function findOneBySomeField($value): ?ComponentGroup
	{
	    return $this->createQueryBuilder('c')
		->andWhere('c.exampleField = :val')
		->setParameter('val', $value)
		->getQuery()
		->getOneOrNullResult()
	    ;
	}
	*/
}
