<?php

namespace App\Repository;

use App\Entity\CptSurgeryType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CptSurgeryType|null find($id, $lockMode = null, $lockVersion = null)
 * @method CptSurgeryType|null findOneBy(array $criteria, array $orderBy = null)
 * @method CptSurgeryType[]    findAll()
 * @method CptSurgeryType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CptSurgeryTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CptSurgeryType::class);
    }

    // /**
    //  * @return CptSurgeryType[] Returns an array of CptSurgeryType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CptSurgeryType
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
