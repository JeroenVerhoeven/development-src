<?php
/**
 * Created by PhpStorm.
 * User: Jeroen
 * Date: 27-8-2019
 * Time: 04:43
 */

namespace App\Command;

use App\Entity\Component;
use App\Entity\Cpt;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AddImages2Components extends Command
{
	protected static $defaultName = 'medica:images2components';

	private $entityManager;

	private $container;

	private $params;

	public function __construct(ContainerInterface $container, ParameterBagInterface $params)
	{
		parent::__construct();
		$this->container = $container;
		$this->params = $params;
	}

	protected function configure()
	{
		$this
			->setDescription("Link images to components")
			->setHelp("This command allows you to link image files to components")
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$output->writeln("Start linking images");

		$this->entityManager = $this->container->get('doctrine')->getManager();

		$ComponentImagePath = $this->params->get('component_image_path');

		$finder = new Finder();

		$finder->files()->name(['*.gif','*.jpg','*.png',])->in($ComponentImagePath);

		if ($finder->hasResults()) {

			foreach ($finder as $file) {

				$fileNameWithExtension = $file->getRelativePathname();

				$aImageParts = explode(".", $fileNameWithExtension);

				$aComponent = $this->entityManager->getRepository(Component::class)->findby(
					['ComponentNumber' => $aImageParts[0]]
				);

				if(sizeof($aComponent) > 0) {

					$oComponent = $aComponent[0];

					$output->writeln("Component gevonden (" . $oComponent->getComponentNumber() . "), toevoegen image " . $fileNameWithExtension);

					$oComponent->setImage($fileNameWithExtension);

					$this->entityManager->persist($oComponent);
				}
			}
			$this->entityManager->flush();
		}

		$output->writeln("Einde linking images");
	}
}