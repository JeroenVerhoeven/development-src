<?php
/**
 * Created by PhpStorm.
 * User: Jeroen
 * Date: 27-8-2019
 * Time: 04:43
 */

namespace App\Command;

use App\Entity\Company;
use App\Entity\CompanyContacts;
use App\Entity\CptBurden;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportCompanyContactsCommand extends Command
{
	protected static $defaultName = 'medica:import-company-contacts';

	private $entityManager;

	private $container;

	public function __construct(ContainerInterface $container)
	{
		parent::__construct();
		$this->container = $container;
	}

	protected function configure()
	{
		$this
			->setDescription("Import Company/Contacts")
			->setHelp("This command allows you to import all Company with contacts")
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$output->writeln("Start import import-company-contacts");

		$InputPath = getcwd() . '/src/Command/ImportData/';

		$InputFile = "Efficy.csv";

		$InputPathFile = $InputPath . $InputFile;

		if(!file_exists($InputPathFile)) {

			$LogData = "ERROR: LET OP! Bestand " . $InputPathFile . " bestaat niet";

			$output->writeln("<info>". $LogData . "</info>");

			exit;
		}

		$this->entityManager = $this->container->get('doctrine')->getManager();

		$time_start 	= microtime(true);

		$fp = file($InputPathFile);

		$AantalRegels =  count($fp);

		$teller = 0;

		$output->writeln("Inlezen file: " . $InputPathFile);

		$time_start 	= microtime(true);

		$fp = file($InputPathFile);
		$AantalRegels =  count($fp);

		$handle = fopen($InputPathFile, "r");

		if (empty($handle) === false) {

			$teller = 0;

			while (($data = fgetcsv($handle, 999999, ";")) !== FALSE) {

				$teller++;

				if($teller < 2) {
					continue;
				}

				if ($teller % 100 == 0) {

					$time_end 	= microtime(true);
					$time 		= sprintf("%.3f", $time_end - $time_start);

					$output->writeln("rows: " . $teller . "/" . $AantalRegels . ", time spend: ". $time);

					$time_start = microtime(true);
				}

				$aCompanyContact['attn'] = $data[0];
				$aCompanyContact['contact_naam'] = $data[1];
				$aCompanyContact['contact_functie'] = $data[2];
				$aCompanyContact['company_naam'] = $data[3];
				$aCompanyContact['company_id'] = $data[4];
				$aCompanyContact['company_straat'] = $data[5];
				$aCompanyContact['company_postcode'] = $data[6];
				$aCompanyContact['company_plaats'] = $data[7];
				$aCompanyContact['company_land'] = $data[8];
				$aCompanyContact['contact_id'] = $data[9];
				$aCompanyContact['contact_aanhef'] = $data[10];

				if($this->ExistCompanyInDb($aCompanyContact['company_id'])) {

					$this->updateCompany($aCompanyContact);
				}

				else {
					$this->addCompany($aCompanyContact);
				}

				if(!$this->ExistContactInDb($aCompanyContact['contact_id'], $aCompanyContact['company_id'])) {

					$this->addCompanyContact($aCompanyContact);
				}
				else {
					$this->updateCompanyContact($aCompanyContact);
				}

				$this->entityManager->flush();
				$this->entityManager->clear();
			}
		}

		$output->writeln("Einde import import-company-contacts");
	}

	/**
	 * @param string $CompanyId
	 * @return bool
	 */
	private function ExistCompanyInDb(string $CompanyId)
	{
		$result = $this->entityManager->getRepository(Company::class)->findBy(
			['CompanyId' => $CompanyId]
		);

		if(sizeof($result) > 0) {

			return true;
		}
		return false;
	}

	/**
	 * @param string $ContactId
	 * @return bool
	 */
	private function ExistContactInDb(string $ContactId, $company_id)
	{
		$result = $this->entityManager->getRepository(CompanyContacts::class)->FindContactByIdComapnyId($ContactId, $company_id);

		if(sizeof($result) > 0) {

			return true;
		}
		return false;
	}

	/**
	 * @param string $CompanyId
	 * @return mixed
	 */
	private function getCompany(string $CompanyId)
	{
		$oCompany = $this->entityManager->getRepository(Company::class)->findBy(
			['CompanyId' => $CompanyId]
		);

		return $oCompany[0];
	}

	private function getContact(string $contact_id)
	{
		$oContacty = $this->entityManager->getRepository(CompanyContacts::class)->findBy(
			['ContactId' => $contact_id]
		);

		return $oContacty[0];
	}

	private function addCompany(array $aCompanyContact)
	{
		$oCompany = new Company();

		$oCompany->setCompanyId($aCompanyContact['company_id']);
		$oCompany->setCompanyName($aCompanyContact['company_naam']);
		$oCompany->setCompanyStreet($aCompanyContact['company_straat']);
		$oCompany->setCopmpanyZipcode($aCompanyContact['company_postcode']);
		$oCompany->setCompanyCity($aCompanyContact['company_plaats']);
//		$oCompany->setCompanyCountry($aCompanyContact['company_land']);
//		$oCompany->setCompanyPhone($aCompanyContact['Phone']);

		$this->entityManager->persist($oCompany);
		$this->entityManager->flush();
		$this->entityManager->clear();
	}

	private function updateCompany(array $aCompanyContact)
	{
		$oCompany = $this->getCompany($aCompanyContact['company_id']);

		$oCompany->setCompanyName($aCompanyContact['company_naam']);
		$oCompany->setCompanyStreet($aCompanyContact['company_straat']);
		$oCompany->setCopmpanyZipcode($aCompanyContact['company_postcode']);
		$oCompany->setCompanyCity($aCompanyContact['company_plaats']);

//		$oCompany->setCompanyCountry($aCompanyContact['company_land']);
//		$oCompany->setCompanyPhone($aCompanyContact['Phone']);

		$this->entityManager->persist($oCompany);
		$this->entityManager->flush();
		$this->entityManager->clear();

	}

	private function addCompanyContact(array $aCompanyContact)
	{
		$oCompany = $this->getCompany($aCompanyContact['company_id']);

		$oContact = new CompanyContacts();

		$oContact->setContactId($aCompanyContact['contact_id']);
		$oContact->setContactName($aCompanyContact['contact_naam']);
		$oContact->setContactAttn($aCompanyContact['contact_aanhef']);
		$oContact->setContactJobDescription($aCompanyContact['contact_functie']);

		$oContact->setCompany($oCompany);

		$this->entityManager->persist($oContact);
		$this->entityManager->flush();
		$this->entityManager->clear();
	}

	private function updateCompanyContact(array $aCompanyContact)
	{
		$oContact = $this->getContact($aCompanyContact['contact_id']);

		$oCompany = $this->getCompany($aCompanyContact['company_id']);

		$oContact->setContactName($aCompanyContact['contact_naam']);
		$oContact->setContactAttn($aCompanyContact['contact_aanhef']);
		$oContact->setContactJobDescription($aCompanyContact['contact_functie']);
		$oContact->setCompany($oCompany);

		$this->entityManager->persist($oContact);
		$this->entityManager->flush();
		$this->entityManager->clear();
	}
}