<?php
/**
 * Created by PhpStorm.
 * User: Jeroen
 * Date: 27-8-2019
 * Time: 04:43
 */

namespace App\Command;

use App\Entity\Company;
use App\Entity\CompanyContacts;
use App\Entity\Cpt;
use App\Entity\WtClassification;
use App\Entity\WtProductLine;
use App\Entity\WtQuotation;
use App\Entity\WtQuotationLine;
use App\Entity\WtStatus;
use DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportWtQuotationLineCommand extends Command
{
        protected static $defaultName = 'medica:import-wintrade-quotationline';

        private $entityManager;

        private $container;

        public function __construct(ContainerInterface $container)
        {
                parent::__construct();
                $this->container = $container;
        }

        protected function configure()
        {
                $this
                        ->setDescription("Import Wintrade quotation lines")
                        ->setHelp("This command allows you to import all Wintrade quotation lines")
                ;
        }

        protected function execute(InputInterface $input, OutputInterface $output)
        {
                $output->writeln("Start import import-wintrade-quotationlines");

                $InputPath = getcwd() . '/src/Command/ImportData/';

                $InputFile = "tbl_wtofferteregels.csv";

                $InputPathFile = $InputPath . $InputFile;

                if(!file_exists($InputPathFile)) {

                        $LogData = "ERROR: LET OP! Bestand " . $InputPathFile . " bestaat niet";

                        $output->writeln("<info>". $LogData . "</info>");

                        exit;
                }

                $this->entityManager = $this->container->get('doctrine')->getManager();

                $time_start 	= microtime(true);

                $fp = file($InputPathFile);
                $AantalRegels =  count($fp);

                $handle = fopen($InputPathFile, "r");

                if (empty($handle) === false) {

                        $teller = 0;

                        while (($data = fgetcsv($handle, 999999, "|")) !== FALSE) {

                                $teller++;

				if($teller < 2) {
					continue;
				}

				if ($teller % 100 == 0) {

                                        $time_end 	= microtime(true);
                                        $time 		= sprintf("%.3f", $time_end - $time_start);

                                        $output->writeln("rows: " . $teller . "/" . $AantalRegels . ", time spend: ". $time);

                                        $time_start = microtime(true);
                                }

                                $aContent['Quotationnumber'] = $data[0];
                                $aContent['Version'] = $data[1];
                                $aContent['Quotationline'] = $data[2];
                                $aContent['ArticleNumber'] = $data[3];
                                $aContent['Description'] = $data[4];
                                $aContent['PurchasePrice'] = $data[5];
                                $aContent['MinMargin'] = $data[6];
                                $aContent['PrefMargin'] = $data[7];
                                $aContent['PrefPrice'] = $data[8];
                                $aContent['MinPrice'] = $data[9];
                                $aContent['SellingUnit'] = $data[10];
                                $aContent['Quantity'] = (int)  $data[11];
                                $aContent['SellingPrice'] = $data[12];
                                $aContent['NameSupplier'] = $data[13];
                                $aContent['Classification'] = $data[14];
                                $aContent['ProductLine'] = $data[15];
                                $aContent['ArticleNumberSupplier'] = $data[16];
                                $aContent['VAT'] = $data[17];
                                $aContent['ArticleType'] = $data[18];

//                                print_r($aContent);
//                                exit;

                                // Check bestaat quotation regels
                                if(!$this->existsInDb($aContent['Quotationnumber'], $aContent['ArticleNumber'])) {

                                        $this->addQuotationLine($aContent);
                                }

                                $this->entityManager->flush();
                                $this->entityManager->clear();
                        }
                }
                $output->writeln("Einde import import-wintrade-quotationlines");
        }

        private function addQuotationLine($aContent)
        {
                $oQuotationLine = new WtQuotationLine();

                $oQuotationLine->setArticleNumber($aContent['ArticleNumber']);
                $oQuotationLine->setDescription($aContent['Description']);
                $oQuotationLine->setPurchasePrice($aContent['PurchasePrice']);
                $oQuotationLine->setMinMargin($aContent['MinMargin']);
                $oQuotationLine->setPreferredMargin($aContent['PrefMargin']);
                $oQuotationLine->setPreferredPrice($aContent['PrefPrice']);
                $oQuotationLine->setMinPrice($aContent['MinPrice']);
                $oQuotationLine->setSellingUnit($aContent['SellingUnit']);
                $oQuotationLine->setQuantity($aContent['Quantity']);
                $oQuotationLine->setSellingPrice($aContent['SellingPrice']);
                $oQuotationLine->setNameSupplier($aContent['NameSupplier']);
                $oQuotationLine->setVAT($aContent['VAT']);
                $oQuotationLine->setArticleType($aContent['ArticleType']);
                $oQuotationLine->setArticleNumberSupplier($aContent['ArticleNumberSupplier']);

                $oQuotation = $this->getQuotation($aContent['Quotationnumber']);
                if($oQuotation) {
                        $oQuotationLine->setQuotation($oQuotation);
                        $oQuotation->setVersion($this->getLetter($aContent['Version']));

                }

                $oClassification = $this->getClassification($aContent['Classification']);
                if($oClassification) {
			$oQuotationLine->setClassification($oClassification);
                }

                $oProductLine = $this->getProductLine($aContent['ProductLine']);
                if($oProductLine) {
			$oQuotationLine->setProductLine($oProductLine);
                }


                $this->entityManager->persist($oQuotationLine);
        }

        private function getQuotation($quotation_number)
        {
                $oQuotation = $this->entityManager->getRepository(WtQuotation::class)->findBy(
                        [
                        	'QuotationNumber' => $quotation_number,
				//'Version' => $version
			]
                );

                if($oQuotation) {
                        return $oQuotation[0];
                }

                return;
        }

	private function getStatus($status)
	{
		$oCptStatus = $this->entityManager->getRepository(WtStatus::class)->findBy(
			['id' => $status]
		);

		if($oCptStatus) {
			return $oCptStatus[0];
		}

		return;
	}

        private function getClassification($classificationId)
        {
                $aClassification = $this->entityManager->getRepository(WtClassification::class)->findBy(
                        ['Classification_Id' => $classificationId]
                );

                if($aClassification) {
                        return $aClassification[0];
                }

                return;
        }

        private function getProductLine($productLine)
        {
                $aProductLine = $this->entityManager->getRepository(WtProductLine::class)->findBy(
                        ['productline_id' => $productLine]
                );

		if($aProductLine) {
                        return $aProductLine[0];
                }

                return;
        }

        private function existsInDb($quotationNumber, $articleNumber)
        {

		$aQuotationLines = $this->entityManager->getRepository(WtQuotationline::class)->findQuotationLine_test(
			$quotationNumber,
			//$version,
			$articleNumber
		);

                if($aQuotationLines) {

                        return true;
                }
                return false;
        }

        private function existsQuotationInDb($quotationNumber, $version)
        {
                $aQuotations = $this->entityManager->getRepository(WtQuotation::class)->findBy(
                        ['QuotationNumber' => $quotationNumber, 'Version' => $version]
                );

                if($aQuotations) {
                        return true;
                }

                return false;
        }

        private function getLetter($Version)
	{
		$currentVersion = $Version;

		$aLetters = range('A', 'Z');

		$newLetter = "";

		if(array_key_exists($currentVersion, $aLetters))
		{
			$newLetter = $aLetters[$currentVersion];

			if($newLetter == 'A')
			{
				$newLetter = "";
			}
		}

		return $newLetter;
	}

}