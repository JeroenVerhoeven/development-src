<?php

namespace App\Form;

use App\Entity\CptTemplates;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CptTemplatesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Name', TextType::class, ['label' => 'Name template'])
            ->add('DocumentName', FileType::class, ['label' => 'Upload template'])
            ->add('DocumentType', ChoiceType::class, [
                    'choices' => [
                            'Offerte' => 'Offerte',
                            'Rapport' => 'Rapport',
                    ],
            ])
                ->add('application', ChoiceType::class, [
                        'choices' => [
                                'Wintrade' => 'Wintrade',
                                'Wincalc' => 'Wincalc',
                        ],
                ])
            ->add('Language', ChoiceType::class, [
                    'choices' => [
                            'English' => 'EN',
                            'Dutch' => 'NL',
                            'French' => 'FR',
                            'German' => 'DE',
                            'Spanish' => 'ES',
                            'Italian' => 'IT'
                    ],
            ])

            ->add('save', SubmitType::class, ['label' => 'Upload template']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CptTemplates::class,
        ]);
    }
}
