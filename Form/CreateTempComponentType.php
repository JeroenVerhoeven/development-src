<?php

namespace App\Form;

use App\Entity\CptTempComponenten;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class CreateTempComponentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('SupplierComponentNumber')
            ->add('Supplier')
            ->add('Description')
            ->add('SampleSupplied', CheckboxType::class, [
                    'label'    => 'Sample supplied',
                    'required' => false,
            ])
            ->add('PurchasePrice')
            ->add('NormTime')
            ->add('Classification')
            ->add('ComponentType')
            ->add('Status')
            ->add('Save', SubmitType::class, ['label' => 'Save component'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CptTempComponenten::class,
        ]);
    }
}
