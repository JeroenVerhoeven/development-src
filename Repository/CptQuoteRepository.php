<?php

namespace App\Repository;

use App\Entity\CptQuote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CptQuote|null find($id, $lockMode = null, $lockVersion = null)
 * @method CptQuote|null findOneBy(array $criteria, array $orderBy = null)
 * @method CptQuote[]    findAll()
 * @method CptQuote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CptQuoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CptQuote::class);
    }

    // /**
    //  * @return CptQuote[] Returns an array of CptQuote objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CptQuote
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
