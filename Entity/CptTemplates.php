<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CptTemplatesRepository")
 */
class CptTemplates
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\File(
     * maxSize = "1024k",
     * mimeTypes = {"application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/x-pdf"},
     * mimeTypesMessage = "Please upload a valid template"
     * )
     */
    private $DocumentName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $DocumentType;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $Language;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $application;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getDocumentName(): ?string
    {
        return $this->DocumentName;
    }

    public function setDocumentName(string $DocumentName): self
    {
        $this->DocumentName = $DocumentName;

        return $this;
    }

    public function getDocumentType(): ?string
    {
        return $this->DocumentType;
    }

    public function setDocumentType(string $DocumentType): self
    {
        $this->DocumentType = $DocumentType;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->Language;
    }

    public function setLanguage(string $Language): self
    {
        $this->Language = $Language;

        return $this;
    }

    public function getApplication(): ?string
    {
        return $this->application;
    }

    public function setApplication(string $application): self
    {
        $this->application = $application;

        return $this;
    }
}
