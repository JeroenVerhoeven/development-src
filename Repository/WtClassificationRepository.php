<?php

namespace App\Repository;

use App\Entity\WtClassification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method WtClassification|null find($id, $lockMode = null, $lockVersion = null)
 * @method WtClassification|null findOneBy(array $criteria, array $orderBy = null)
 * @method WtClassification[]    findAll()
 * @method WtClassification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WtClassificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WtClassification::class);
    }

    // /**
    //  * @return WtClassification[] Returns an array of WtClassification objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WtClassification
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
