<?php

namespace App\Controller;

use App\Entity\Company;
use App\Entity\Component;
use App\Entity\ComponentStatus;
use App\Entity\Cpt;
use App\Entity\CptBurden;
use App\Entity\CptComponenten;
use App\Entity\CptStatus;
use App\Entity\CptTemplates;
use App\Entity\MaxCptNum;
use App\Entity\SecurityUser;
use App\Entity\WtArticle;
use App\Entity\WtClassification;
use App\Entity\WtProductLine;
use App\Entity\WtQuotationLine;
use App\Form\AddUserType;
use App\Form\ArticleImportFormType;
use App\Form\CptBurdenType;
use App\Form\CptImportFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Form\CptTemplatesType;

use App\Form\CptChangeFormType;

class MedicaAdminController extends AbstractController
{
	/**
	 * @Route("/admin/users", name="users")
	 */
	public function users()
	{
		$entityManager = $this->getDoctrine()->getManager();

		$aUsers = $entityManager->getRepository(SecurityUser::class)->findAll();

		return $this->render('medica_admin/users.html.twig', [
			'Users' => $aUsers
		]);
	}

        /**
         * @Route("/admin/cpt-change-burden", name="ChangeBurden")
         */
        public function ChangeBurden(Request $request)
        {
                $entityManager = $this->getDoctrine()->getManager();

                $burdenId = $request->query->get('burdenId');

                $Description = $request->query->get('burdenDescription');

                $Rate = $request->query->get('Rate');

                $id = $request->query->get('BurdenId');

                $oBurden = $entityManager->getRepository(CptBurden::class)->find($id);

                $Burden = $entityManager->getRepository(CptBurden::class)->findBy(
                        ['BurdenId' => $burdenId]
                );

                if(sizeof($Burden) > 0) {
                        $this->addFlash('ChangeBurdenIdExists', 'Burden id already exists!');
                } else {
                        $oBurden->setBurdenId($burdenId);
                }

                $oBurden->setDescription($Description);
                $oBurden->setRate($Rate);

                $entityManager->persist($oBurden);
                $entityManager->flush();

                return $this->redirectToRoute('burden');
        }

        /**
         * @Route("/admin/manage_burden", name="burden")
         */
        public function manageBurden(Request $request)
        {
                $entityManager = $this->getDoctrine()->getManager();

                $aBurden = $entityManager->getRepository(CptBurden::class)->findAll();

                $newBurden = new CptBurden();

                $form = $this->createForm(CptBurdenType::class, $newBurden);

                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {

                        $BurdenId = $form['BurdenId']->getData();

                        $Burden = $entityManager->getRepository(CptBurden::class)->findBy(
                                ['BurdenId' => $BurdenId]
                        );

                        if(sizeof($Burden) > 0) {
                                $this->addFlash('BurdenIdExists', 'Burden id already exists!');
                        }
                        else {
                                $entityManager->persist($newBurden);
                                $entityManager->flush();
                        }

                        return $this->redirectToRoute('burden');
                }

                return $this->render('medica_admin/manage_burden.html.twig', [
                        'Burden' => $aBurden,
                        'form' => $form->createView()
                ]);
        }

        /**
         * @Route("/admin/delete_burden/{burden_id}", name="DeleteBurden")
         */
        public function DeleteBurden($burden_id)
        {
                $entityManager = $this->getDoctrine()->getManager();

                $deleteBurden = $entityManager->getRepository(CptBurden::class)->find($burden_id);

                $aCpts = $entityManager->getRepository(Cpt::class)->findBy(
                        ['Burden' => $burden_id]
                );

                if(sizeof($aCpts) > 0) {
                        $this->addFlash('notice', 'This burden is still linked to CPT(s)!');
                } else {
                        $entityManager->remove($deleteBurden);
                        $entityManager->flush();
                }

                return $this->redirectToRoute('burden');
        }

        /**
         * @Route("/admin/import_cpt", name="ImportCpt")
         */
        public function importCPT(Request $request)
        {
                $entityManager = $this->getDoctrine()->getManager();

                $oCompanys = $entityManager->getRepository(Company::class)->findFirstRowsCompany(10);

                $user = $this->getUser();

                $oCpt = new Cpt();

                $form = $this->createForm(CptImportFormType::class, $oCpt);

                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {

			$FormData = $form->getData();

			$CptNum = $this->getmaxCptNum();

			$CptNum .= "AA";

			$oCpt->setCptNumber($CptNum);

                        $oCpt->setCreationDate(new \DateTime());

                        if($user) {
                                $oCpt->setEmployee($user->getName());
                        } else {
                                $oCpt->setEmployee("Anonymous user");
                        }

			$CptStatusNew = $entityManager->getRepository(CptStatus::class)->find(1);

			$ComponentStatusNew = $entityManager->getRepository(ComponentStatus::class)->find(1);

			$oCpt->setCptStatus($CptStatusNew);

			$entityManager->persist($oCpt);

			$entityManager->flush();

			// Componenten inlezen

                        $file = $form->get('ComponentFile')->getData();

			$FileContent = file_get_contents($file->getPathname());

			$FileLines = explode("\r\n", $FileContent);

			foreach ($FileLines as $FileLine) {

				$aLineFileds = explode('|', $FileLine);

				if(!empty($aLineFileds[0])) {

					$aContent['component_number'] = $aLineFileds[0];
					$aContent['Quantity'] = $aLineFileds[1];

					$aComponenten = $entityManager->getRepository(Component::class)->findBy(
						['ComponentNumber' => $aContent['component_number']]
					);

					if($aComponenten[0]) {

						$NewComponent = new CptComponenten();

						$NewComponent->setCptNumber($CptNum);
						$NewComponent->setCpt($oCpt);
						$NewComponent->setComponentNumber($aContent['component_number']);
						$NewComponent->setQuantity($aContent['Quantity']);
						$NewComponent->setPurchasePrice($aComponenten[0]->getPuchasePrice());
						$NewComponent->setNormTime($aComponenten[0]->getNormTime());
						$NewComponent->setCptComponentStatus($aComponenten[0]->getStatus());
						$NewComponent->setComponentGroup($aComponenten[0]->getComponentGroup());
						$NewComponent->setComponentClassification($aComponenten[0]->getClassification());
						$NewComponent->setStatus($ComponentStatusNew);
						$NewComponent->setComponent($aComponenten[0]);
						$NewComponent->setComponentSupplier($aComponenten[0]->getSupplier());
						$NewComponent->setDescription($aComponenten[0]->getDescription());
						$NewComponent->setComponentNumberSupplier($aComponenten[0]->getComponentNumberSupplier());

						$entityManager->persist($NewComponent);

						$entityManager->flush();
					}
				}
			}

			// Redirect
			return $this->redirect($this->generateUrl('ChangeCpt', ['cptnumber' => $CptNum]));
                }

                return $this->render('medica_admin/import_cpt.html.twig', [
                        'controller_name' => 'MedicaCalc',
                        'Cpt' => $oCpt,
                        'form' => $form->createView(),
                        'oCompanys' => $oCompanys,
                ]);
        }

	/**
	 * @Route("/admin/import_article", name="ImportArticle")
	 */
	public function importArticle(Request $request)
	{
		$entityManager = $this->getDoctrine()->getManager();

		$form = $this->createForm(ArticleImportFormType::class);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {

			$file = $form->get('ArticleFile')->getData();

			$FileContent = file_get_contents($file->getPathname());

			$FileLines = explode("\n", $FileContent);

			$counter = 0;

			foreach ($FileLines as $FileLine) {

				$aLineFileds = explode('|', $FileLine);

				if(!empty($aLineFileds[0]) && $counter > 0) {


					$purchase_price = str_replace(',', '.', $aLineFileds[6]);
					$preferred_price = str_replace(',', '.', $aLineFileds[8]);
					$minimal_price = str_replace(',', '.', $aLineFileds[9]);
					$vat = str_replace(',', '.', $aLineFileds[10]);
					$preferred_margin = str_replace(',', '.', $aLineFileds[11]);
					$minimal_margin = str_replace(',', '.', $aLineFileds[12]);

					$aContent['article_number'] = trim($aLineFileds[0]);
					$aContent['article_number_supplier'] = trim($aLineFileds[1]);
					$aContent['description'] = trim($aLineFileds[2]);
					$aContent['article_type'] = trim($aLineFileds[3]);
					$aContent['product_line'] = trim($aLineFileds[4]);
					$aContent['packing_unit'] = (int)$aLineFileds[5];
					$aContent['purchase_price'] = (float)$purchase_price;
					$aContent['name_supplier'] = trim($aLineFileds[7]);
					$aContent['preferred_price'] = (float)$preferred_price;
					$aContent['minimal_price'] = (float)$minimal_price;
					$aContent['vat'] = (float)$vat;
					$aContent['preferred_margin'] = (float)$preferred_margin;
					$aContent['minimal_margin'] = (float)$minimal_margin;

					if(!empty(trim($aLineFileds[13]))) {
						$aContent['classification'] = trim($aLineFileds[13]);
					}
					else {
						$aContent['classification'] = 'P1';
					}

					$aProductLine = $entityManager->getRepository(WtProductLine::class)->findBy(
						['productline_id' => $aContent['product_line']]
					);

					$aClassification = $entityManager->getRepository(WtClassification::class)->findBy(
						['Classification_Id' => $aContent['classification']]
					);

					$ClassificationId = $aClassification[0];

					$productLineId = $aProductLine[0];

					$aArticles = $this->existsInDb($aContent['article_number']);

					if(!$aArticles) {
						$newArticle = new WtArticle();
					}
					else {
						$newArticle = $aArticles[0];
					}

					$newArticle->setArticleNumber($aContent['article_number']);
					$newArticle->setArticleNumberSupplier($aContent['article_number_supplier']);
					$newArticle->setDescription($aContent['description']);
					$newArticle->setArticleType($aContent['article_type']);
					$newArticle->setProductline($productLineId);
					$newArticle->setPackingUnit($aContent['packing_unit']);
					$newArticle->setPurchasePrice($aContent['purchase_price']);
					$newArticle->setNameSupplier($aContent['name_supplier']);
					$newArticle->setPreferredPrice($aContent['preferred_price']);
					$newArticle->setMinimalPrice($aContent['minimal_price']);
					$newArticle->setVAT($aContent['vat']);
					$newArticle->setPreferredMargin($aContent['preferred_margin']);
					$newArticle->setMinMargin($aContent['minimal_margin']);
					$newArticle->setClassification($ClassificationId);

					$entityManager->persist($newArticle);
					$entityManager->flush();
				}
				$counter++;
			}
			$this->addFlash('notice', 'Articles imported!');
		}

		return $this->render('medica_admin/import_article.html.twig', [
			'form' => $form->createView(),
		]);
	}

	private function existsInDb($articleNumber)
	{
		$entityManager = $this->getDoctrine()->getManager();

		$aArticles = $entityManager->getRepository(WtArticle::class)->findBy(
			['ArticleNumber' => $articleNumber]

		);

		if($aArticles) {

			return $aArticles;
		}
		return false;
	}

        /**
         * @Route("/admin/manage_templates", name="manage_templates")
         */
        public function manageTemplates(Request $request)
        {
                $entityManager = $this->getDoctrine()->getManager();

                $aTemplates = $entityManager->getRepository(CptTemplates::class)->findAll();

                $newTemplate = new CptTemplates();

                $form = $this->createForm(CptTemplatesType::class, $newTemplate);

                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                        $file = $form->get('DocumentName')->getData();

                        $fileName = $file->getClientOriginalName();

                        $file->move(
                                $this->getParameter('templates_directory'),
                                $fileName
                        );

                        $newTemplate->setDocumentName($fileName);

                        $entityManager->persist($newTemplate);

                        $entityManager->flush();

                        return $this->redirectToRoute('manage_templates');
                }

                return $this->render('medica_admin/manage_templates.html.twig', [
                        'Templates' => $aTemplates,
                        'form' => $form->createView(),
                ]);
        }

	/**
	 * @Route("/admin/test", name="test Jeroen")
	 */
	public function testJeroen()
	{
		$cptNumber = '100157AB';

		$entityManager = $this->getDoctrine()->getManager();

		$aCpts = $entityManager->getRepository(Cpt::class)->findByCptNumber($cptNumber);

		dump($aCpts);

		return $this->render('medica_admin/test.html.twig', [
			'controller_name' => 'MedicaTestController',
		]);
	}


        /**
	 * @Route("/admin/new_user", name="Create new user")
	 */
	public function createUser(Request $request, UserPasswordEncoderInterface $passwordEncoder)
	{
		$user = new SecurityUser();

		$form = $this->createForm(AddUserType::class, $user);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {

			$FormData = $form->getData();

			$user->setPassword(
				$passwordEncoder->encodePassword($user, $FormData->getPassword())
			);

			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->persist($user);
			$entityManager->flush();

			return $this->redirectToRoute('users');
		}

		return $this->render('medica_admin/new_user.html.twig', [
			'form' => $form->createView(),
		]);
	}


        /**
         * @Route("/admin/delete_template/{template_id}", name="DeleteTemplate")
         */
        public function DeleteTemplates($template_id)
        {
                $entityManager = $this->getDoctrine()->getManager();

                $deleteTemplate = $entityManager->getRepository(CptTemplates::class)->find($template_id);

                $entityManager->remove($deleteTemplate);

                $entityManager->flush();

                return $this->redirectToRoute('manage_templates');
        }


        /**
         * @Route("/admin/delete_user/{userId}", name="DeleteUser")
         */
        public function deleteUser($userId)
        {
                $entityManager = $this->getDoctrine()->getManager();

                $deleteUser = $entityManager->getRepository(SecurityUser::class)->find($userId);

                $entityManager->remove($deleteUser);

                $entityManager->flush();

                return $this->redirectToRoute('users');
        }

        private function getmaxCptNum()
        {
                $entityManager = $this->getDoctrine()->getManager();

                $oMaxCptNum = $entityManager->getRepository(MaxCptNum::class)->find(1);

                $MaxCptNum = $oMaxCptNum->getMaxCptNum();

                $oMaxCptNum->setMaxCptNum($MaxCptNum + 1);

                $entityManager->persist($oMaxCptNum);

                $entityManager->flush();

                return $MaxCptNum;
        }

}
