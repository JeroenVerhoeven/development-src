<?php

namespace App\Repository;

use App\Entity\WtProductLine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method WtProductLine|null find($id, $lockMode = null, $lockVersion = null)
 * @method WtProductLine|null findOneBy(array $criteria, array $orderBy = null)
 * @method WtProductLine[]    findAll()
 * @method WtProductLine[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WtProductLineRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WtProductLine::class);
    }

    public function findWithDescription()
    {
    	return $this->createQueryBuilder('w')
		->andWhere('w.Description IS NOT NULL')
		->andWhere("w.Description !=''")
		->orderBy('w.Description', 'ASC')
		->getQuery()
		->getResult();
    }


    // /**
    //  * @return WtProductLine[] Returns an array of WtProductLine objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WtProductLine
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
