<?php

namespace App\Form;

use App\Entity\WtArticle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ArticleImportFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$builder
		->add('Save', SubmitType::class, ['label' => 'Import articles'])
		;

        $builder
		->add("ArticleFile",FileType::class,array(
			'label'=>'Select article file',
			'multiple'=> false,
			'mapped'=> false,
			"required"=>true
		));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WtArticle::class,
        ]);
    }
}
