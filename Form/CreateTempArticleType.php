<?php

namespace App\Form;

use App\Entity\WtArticle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CreateTempArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ArticleNumber')
            ->add('Description')
            ->add('PackingUnit')
            ->add('NameSupplier')
            ->add('PurchasePrice')
            ->add('MinMargin')
            ->add('PreferredMargin')
            ->add('MinimalPrice')
            ->add('PreferredPrice')
            ->add('ArticleNumberSupplier')
            ->add('ArticleType')
            ->add('VAT')
            ->add('Classification')
            ->add('Productline')
                ->add('Save', SubmitType::class, ['label' => 'Save article'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WtArticle::class,
        ]);
    }
}
