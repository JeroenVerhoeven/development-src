<?php

namespace App\Repository;

use App\Entity\CptTempComponenten;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CptTempComponenten|null find($id, $lockMode = null, $lockVersion = null)
 * @method CptTempComponenten|null findOneBy(array $criteria, array $orderBy = null)
 * @method CptTempComponenten[]    findAll()
 * @method CptTempComponenten[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CptTempComponentenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CptTempComponenten::class);
    }

        public function findFirstRowsComponent($MaxResults)
        {
                return $this->createQueryBuilder('c')
                        ->select(
                                'c.id',
                                'c.SupplierComponentNumber',
                                'c.Supplier',
                                'c.SampleSupplied',
                                'c.PurchasePrice',
                                'c.NormTime',
                                'co.Description as Classification',
                                'b.Description as ComponentType',
                                'd.Description as cptComponentens',
                                'e.Description as Status'
                        )
                        ->leftJoin('c.Classification', 'co')
                        ->leftJoin('c.ComponentType', 'b')
                        ->leftJoin('c.cptComponentens', 'd')
                        ->leftJoin('c.Status', 'e')
//                        ->orderBy('co.Name', 'ASC')
//                        ->andWhere('co.Name IS NOT NULL')
                        ->andWhere("c.Supplier != ''")
                        ->setMaxResults($MaxResults)
                        ->getQuery()
                        ->getResult();
        }

    // /**
    //  * @return CptTempComponenten[] Returns an array of CptTempComponenten objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CptTempComponenten
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
