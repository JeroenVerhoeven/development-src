<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200609021723 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX component_number ON component');
        $this->addSql('CREATE INDEX component_number ON component (component_number)');
        $this->addSql('DROP INDEX cpt_number ON cpt');
        $this->addSql('CREATE INDEX cpt_number ON cpt (cpt_number)');
        $this->addSql('ALTER TABLE company ADD company_phone VARCHAR(255) DEFAULT NULL');
        $this->addSql('DROP INDEX component_number ON cpt_componenten');
        $this->addSql('DROP INDEX cpt_Component_number ON cpt_componenten');
        $this->addSql('CREATE INDEX component_number ON cpt_componenten (component_number)');
        $this->addSql('CREATE INDEX cpt_Component_number ON cpt_componenten (cpt_number, component_number)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company DROP company_phone');
        $this->addSql('DROP INDEX component_number ON component');
        $this->addSql('CREATE INDEX component_number ON component (component_number(191))');
        $this->addSql('DROP INDEX cpt_number ON cpt');
        $this->addSql('CREATE INDEX cpt_number ON cpt (cpt_number(191))');
        $this->addSql('DROP INDEX cpt_Component_number ON cpt_componenten');
        $this->addSql('DROP INDEX component_number ON cpt_componenten');
        $this->addSql('CREATE INDEX cpt_Component_number ON cpt_componenten (cpt_number(191), component_number(191))');
        $this->addSql('CREATE INDEX component_number ON cpt_componenten (component_number(191))');
    }
}
