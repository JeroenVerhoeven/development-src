<?php
/**
 * Created by PhpStorm.
 * User: Jeroen
 * Date: 27-8-2019
 * Time: 04:43
 */

namespace App\Command;

use App\Entity\Company;
use App\Entity\CompanyContacts;
use App\Entity\Cpt;
use App\Entity\WtArticle;
use App\Entity\WtClassification;
use App\Entity\WtHelpArticle;
use App\Entity\WtProductLine;
use App\Entity\WtQuotation;
use App\Entity\WtStatus;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportWtHelpArticleCommand extends Command
{
        protected static $defaultName = 'medica:import-wintrade-helparticle';

        private $entityManager;

        private $container;

        public function __construct(ContainerInterface $container)
        {
                parent::__construct();
                $this->container = $container;
        }

        protected function configure()
        {
                $this
                        ->setDescription("Import Wintrade help articles")
                        ->setHelp("This command allows you to import all Wintrade help articles")
                ;
        }

        protected function execute(InputInterface $input, OutputInterface $output)
        {
                $output->writeln("Start import import-wintrade-helparticle");

                $InputPath = getcwd() . '/src/Command/ImportData/';

                $InputFile = "tbl_hulp_artikel.csv";

                $InputPathFile = $InputPath . $InputFile;

                if(!file_exists($InputPathFile)) {

                        $LogData = "ERROR: LET OP! Bestand " . $InputPathFile . " bestaat niet";

                        $output->writeln("<info>". $LogData . "</info>");

                        exit;
                }

                $this->entityManager = $this->container->get('doctrine')->getManager();
                
                $time_start 	= microtime(true);

                $fp = file($InputPathFile);
                $AantalRegels =  count($fp);

                $handle = fopen($InputPathFile, "r");

                if (empty($handle) === false) {

                        $teller = 0;

                        while (($data = fgetcsv($handle, 999999, ";")) !== FALSE) {

                                $teller++;

                                if($teller < 2) {
                                	continue;
				}

                                if ($teller % 100 == 0) {

                                        $time_end 	= microtime(true);
                                        $time 		= sprintf("%.3f", $time_end - $time_start);

                                        $output->writeln("rows: " . $teller . "/" . $AantalRegels . ", time spend: ". $time);

                                        $time_start = microtime(true);
                                }

                                $aContent['Articlenumber'] = $data[0];
                                $aContent['Description'] = $data[1];
                                $aContent['packingUnit'] = (int) $data[2];
                                $aContent['nameSupplier'] = $data[3];
                                $aContent['classificationId'] = $data[4];
                                $aContent['productline'] = $data[5];
                                $aContent['purchasePrice'] = $data[6];
                                $aContent['minMargin'] = $data[7];
                                $aContent['PrefMargin'] = $data[8];
                                $aContent['minPrice'] = $data[9];
                                $aContent['PrefPrice'] = $data[10];
                                $aContent['articlenumberSupplier'] = $data[11];
                                $aContent['articleType'] = $data[12];
                                $aContent['vat'] = $data[13];

//                                print_r($aContent);

                                if(!$this->existsInDb($aContent['Articlenumber']))
                                {
                                        $this->addArticle($aContent);
                                }

                                $this->entityManager->flush();
                                $this->entityManager->clear();
                        }
                }

                $output->writeln("Einde import import-wintrade-helparticle");
        }

        private function addArticle($aContent)
        {

                $oHelpArticle = new WtHelpArticle();

                $oHelpArticle->setArticleNumber($aContent['Articlenumber']);
                $oHelpArticle->setDescription($aContent['Description']);
                $oHelpArticle->setPackingInit($aContent['packingUnit']);
                $oHelpArticle->setNameSupplier($aContent['nameSupplier']);
                $oHelpArticle->setPurchasePrice($aContent['purchasePrice']);
                $oHelpArticle->setMinMargin($aContent['minMargin']);
                $oHelpArticle->setPreferredMargin($aContent['PrefMargin']);
                $oHelpArticle->setMinimalPrice($aContent['minPrice']);
                $oHelpArticle->setPreferredPrice($aContent['PrefPrice']);
                $oHelpArticle->setArticleNumberSupplier($aContent['articlenumberSupplier']);
                $oHelpArticle->setArticleType($aContent['articleType']);
                $oHelpArticle->setVAT($aContent['vat']);

                $oClassification = $this->getClassification($aContent['classificationId']);
                if($oClassification) {
                        $oHelpArticle->setClassification($oClassification);
                }

                $oProductLine = $this->getProductLine($aContent['productline']);
                if($oProductLine){
                        $oHelpArticle->setProductline($oProductLine);
                }

                $this->entityManager->persist($oHelpArticle);
        }


        private function getClassification($classificationId)
        {
                $aClassification = $this->entityManager->getRepository(WtClassification::class)->findBy(
                        ['Classification_Id' => $classificationId]
                );

                if($aClassification) {
                        return $aClassification[0];
                }

                return;
        }

	private function getProductLine($productLine)
	{
		$aProductLine = $this->entityManager->getRepository(WtProductLine::class)->findBy(
			['productline_id' => $productLine]
		);

		if($aProductLine) {
			return $aProductLine[0];
		}

		return;
	}


	private function existsInDb($Articlenumber)
        {
                $result = $this->entityManager->getRepository(WtArticle::class)->findBy(
                        ['ArticleNumber' => $Articlenumber]
                );

                if($result) {

                        return true;
                }
                return false;
        }

}