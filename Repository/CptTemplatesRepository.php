<?php

namespace App\Repository;

use App\Entity\CptTemplates;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CptTemplates|null find($id, $lockMode = null, $lockVersion = null)
 * @method CptTemplates|null findOneBy(array $criteria, array $orderBy = null)
 * @method CptTemplates[]    findAll()
 * @method CptTemplates[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CptTemplatesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CptTemplates::class);
    }

    // /**
    //  * @return CptTemplates[] Returns an array of CptTemplates objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CptTemplates
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
