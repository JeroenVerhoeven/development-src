<?php

namespace App\Form;

use App\Entity\CptBurden;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CptBurdenType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('BurdenId')
            ->add('Description')
            ->add('Rate')
            ->add('save', SubmitType::class, ['label' => 'Create burden']);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CptBurden::class,
        ]);
    }
}
