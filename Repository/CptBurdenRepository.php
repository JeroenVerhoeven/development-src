<?php

namespace App\Repository;

use App\Entity\CptBurden;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CptBurden|null find($id, $lockMode = null, $lockVersion = null)
 * @method CptBurden|null findOneBy(array $criteria, array $orderBy = null)
 * @method CptBurden[]    findAll()
 * @method CptBurden[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CptBurdenRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, CptBurden::class);
	}

	// /**
	//  * @return CptBurden[] Returns an array of CptBurden objects
	//  */
	/*
	public function findByExampleField($value)
	{
	    return $this->createQueryBuilder('c')
		->andWhere('c.exampleField = :val')
		->setParameter('val', $value)
		->orderBy('c.id', 'ASC')
		->setMaxResults(10)
		->getQuery()
		->getResult()
	    ;
	}
	*/

	/*
	public function findOneBySomeField($value): ?CptBurden
	{
	    return $this->createQueryBuilder('c')
		->andWhere('c.exampleField = :val')
		->setParameter('val', $value)
		->getQuery()
		->getOneOrNullResult()
	    ;
	}
	*/
}
