<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WtClassificationRepository")
 */
class WtClassification
{
        public function __toString()
        {
                return $this->getClassificationId();
        }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $Classification_Id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WtHelpArticle", mappedBy="Classification")
     */
    private $wtHelpArticles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WtQuotationLine", mappedBy="Classification")
     */
    private $wtQuotationLines;

    public function __construct()
    {
        $this->wtHelpArticles = new ArrayCollection();
        $this->wtQuotationLines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClassificationId(): ?string
    {
        return $this->Classification_Id;
    }

    public function setClassificationId(string $Classification_Id): self
    {
        $this->Classification_Id = $Classification_Id;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    /**
     * @return Collection|WtHelpArticle[]
     */
    public function getWtHelpArticles(): Collection
    {
        return $this->wtHelpArticles;
    }

    public function addWtHelpArticle(WtHelpArticle $wtHelpArticle): self
    {
        if (!$this->wtHelpArticles->contains($wtHelpArticle)) {
            $this->wtHelpArticles[] = $wtHelpArticle;
            $wtHelpArticle->setClassification($this);
        }

        return $this;
    }

    public function removeWtHelpArticle(WtHelpArticle $wtHelpArticle): self
    {
        if ($this->wtHelpArticles->contains($wtHelpArticle)) {
            $this->wtHelpArticles->removeElement($wtHelpArticle);
            // set the owning side to null (unless already changed)
            if ($wtHelpArticle->getClassification() === $this) {
                $wtHelpArticle->setClassification(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|WtQuotationLine[]
     */
    public function getWtQuotationLines(): Collection
    {
        return $this->wtQuotationLines;
    }

    public function addWtQuotationLine(WtQuotationLine $wtQuotationLine): self
    {
        if (!$this->wtQuotationLines->contains($wtQuotationLine)) {
            $this->wtQuotationLines[] = $wtQuotationLine;
            $wtQuotationLine->setClassification($this);
        }

        return $this;
    }

    public function removeWtQuotationLine(WtQuotationLine $wtQuotationLine): self
    {
        if ($this->wtQuotationLines->contains($wtQuotationLine)) {
            $this->wtQuotationLines->removeElement($wtQuotationLine);
            // set the owning side to null (unless already changed)
            if ($wtQuotationLine->getClassification() === $this) {
                $wtQuotationLine->setClassification(null);
            }
        }

        return $this;
    }
}
