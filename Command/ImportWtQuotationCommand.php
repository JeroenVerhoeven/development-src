<?php
/**
 * Created by PhpStorm.
 * User: Jeroen
 * Date: 27-8-2019
 * Time: 04:43
 */

namespace App\Command;

use App\Entity\Company;
use App\Entity\CompanyContacts;
use App\Entity\Cpt;
use App\Entity\WtQuotation;
use App\Entity\WtStatus;
use DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportWtQuotationCommand extends Command
{
        protected static $defaultName = 'medica:import-wintrade-quotation';

        private $entityManager;

        private $container;

        public function __construct(ContainerInterface $container)
        {
                parent::__construct();
                $this->container = $container;
        }

        protected function configure()
        {
                $this
                        ->setDescription("Import Wintrade quotations")
                        ->setHelp("This command allows you to import all Wintrade quotations")
                ;
        }

        protected function execute(InputInterface $input, OutputInterface $output)
        {
                $output->writeln("Start import import-wintrade-quotations");

                $InputPath = getcwd() . '/src/Command/ImportData/';

                $InputFile = "tbl_wtofferte.csv";

                $InputPathFile = $InputPath . $InputFile;

                if(!file_exists($InputPathFile)) {

                        $LogData = "ERROR: LET OP! Bestand " . $InputPathFile . " bestaat niet";

                        $output->writeln("<info>". $LogData . "</info>");

                        exit;
                }

                $this->entityManager = $this->container->get('doctrine')->getManager();

                $time_start 	= microtime(true);

                $fp = file($InputPathFile);
                $AantalRegels =  count($fp);

                $handle = fopen($InputPathFile, "r");

                if (empty($handle) === false) {

                        $teller = 0;

                        while (($data = fgetcsv($handle, 999999, "|")) !== FALSE) {

                                $teller++;

                                if ($teller % 100 == 0) {

                                        $time_end 	= microtime(true);
                                        $time 		= sprintf("%.3f", $time_end - $time_start);

                                        $output->writeln("rows: " . $teller . "/" . $AantalRegels . ", time spend: ". $time);

                                        $time_start = microtime(true);
                                }

                                $aContent['Quotationnumber'] = $data[0];
                                $aContent['varId'] = $data[1];
                                $aContent['datumId'] = $data[2];
                                $aContent['klantId'] = $data[3];
                                $aContent['contactpersoonId'] = $data[4];
                                $aContent['medewerker'] = $data[5];
                                $aContent['memo1'] = $data[6];
                                $aContent['memo2'] = $data[7];
                                $aContent['statusId'] = $data[8];
                                $aContent['naam'] = $data[9];

//                                $output->writeln($aContent['Quotationnumber']);

                                if(!$this->existsInDb($aContent['Quotationnumber']))
                                {
                                        $this->addQuotation($aContent);
                                }

                                $this->entityManager->flush();
                                $this->entityManager->clear();
                        }
                }

                $output->writeln("Einde import import-wintrade-quotations");
        }

        private function addQuotation($aContent)
        {

                $Newdate = new DateTime($aContent['datumId']);

                $oQuotation = new WtQuotation();

                $oQuotation->setQuotationNumber($aContent['Quotationnumber']);
                $oQuotation->setVersion($aContent['varId']);
                $oQuotation->setEmployee($aContent['medewerker']);
                $oQuotation->setDate($Newdate);
                $oQuotation->setMemo1($aContent['memo1']);
                $oQuotation->setMemo2($aContent['memo2']);
                $oQuotation->setName($aContent['naam']);

                $oQuotationStatus = $this->getQuotationStatus($aContent['statusId']);
                if($oQuotationStatus) {
                        $oQuotation->setStatus($oQuotationStatus);
                }

                $oCustomer = $this->getCustomer($aContent['klantId']);
                if($oCustomer) {
                        $oQuotation->setCustomer($oCustomer);
                }

                $oContact = $this->getContact($aContent['contactpersoonId']);
                if($oContact){
                        $oQuotation->setCompanyContacts($oContact);
                }

                $this->entityManager->persist($oQuotation);
        }


        private function getQuotationStatus($status)
        {
                $oCptStatus = $this->entityManager->getRepository(WtStatus::class)->findBy(
                        ['id' => $status]
                );

                if($oCptStatus) {
                        return $oCptStatus[0];
                }

                return;
        }

        private function existsInDb($quotationNumber)
        {
                $aQuotations = $this->entityManager->getRepository(WtQuotation::class)->findBy(
                        ['QuotationNumber' => $quotationNumber]
                );

                if($aQuotations) {
                        return true;
                }

                return false;
        }

        private function getCustomer($IdCustomer)
        {
                $oCompany = $this->entityManager->getRepository(Company::class)->findBy(
                        ['CompanyId' => $IdCustomer]
                );

                if($oCompany) {
                        return $oCompany[0];
                }
                return;
        }

        private function getContact($IdContact)
        {
                $aContact = $this->entityManager->getRepository(CompanyContacts::class)->findBy(
                        ['ContactId' => $IdContact]
                );

                if($aContact) {
                        return $aContact[0];
                }

                return;
        }

}