<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WtQuotationRepository")
 */
class WtQuotation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $QuotationNumber;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $Version;

    /**
     * @ORM\Column(type="datetime")
     */
    private $Date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="wtQuotations")
     * @ORM\JoinColumn(nullable=true)
     */
    private $Customer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CompanyContacts", inversedBy="wtQuotations")
     * @ORM\JoinColumn(nullable=true)
     */
    private $CompanyContacts;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Employee;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Memo1;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Memo2;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\WtStatus", inversedBy="wtQuotations")
     * @ORM\JoinColumn(nullable=true)
     */
    private $Status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WtQuotationLine", mappedBy="Quotation")
     */
    private $wtQuotationLines;

    public function __construct()
    {
        $this->wtQuotationLines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuotationNumber(): ?string
    {
        return $this->QuotationNumber;
    }

    public function setQuotationNumber(string $QuotationNumber): self
    {
        $this->QuotationNumber = $QuotationNumber;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->Version;
    }

    public function setVersion(?string $Version): self
    {
        $this->Version = $Version;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->Date;
    }

    public function setDate(\DateTimeInterface $Date): self
    {
        $this->Date = $Date;

        return $this;
    }

    public function getCustomer(): ?Company
    {
        return $this->Customer;
    }

    public function setCustomer(?Company $Customer): self
    {
        $this->Customer = $Customer;

        return $this;
    }

    public function getCompanyContacts(): ?CompanyContacts
    {
        return $this->CompanyContacts;
    }

    public function setCompanyContacts(?CompanyContacts $CompanyContacts): self
    {
        $this->CompanyContacts = $CompanyContacts;

        return $this;
    }

    public function getEmployee(): ?string
    {
        return $this->Employee;
    }

    public function setEmployee(string $Employee): self
    {
        $this->Employee = $Employee;

        return $this;
    }

    public function getMemo1(): ?string
    {
        return $this->Memo1;
    }

    public function setMemo1(?string $Memo1): self
    {
        $this->Memo1 = $Memo1;

        return $this;
    }

    public function getMemo2(): ?string
    {
        return $this->Memo2;
    }

    public function setMemo2(?string $Memo2): self
    {
        $this->Memo2 = $Memo2;

        return $this;
    }

    public function getStatus(): ?WtStatus
    {
        return $this->Status;
    }

    public function setStatus(?WtStatus $Status): self
    {
        $this->Status = $Status;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    /**
     * @return Collection|WtQuotationLine[]
     */
    public function getWtQuotationLines(): Collection
    {
        return $this->wtQuotationLines;
    }

    public function addWtQuotationLine(WtQuotationLine $wtQuotationLine): self
    {
        if (!$this->wtQuotationLines->contains($wtQuotationLine)) {
            $this->wtQuotationLines[] = $wtQuotationLine;
            $wtQuotationLine->setQuotation($this);
        }

        return $this;
    }

    public function removeWtQuotationLine(WtQuotationLine $wtQuotationLine): self
    {
        if ($this->wtQuotationLines->contains($wtQuotationLine)) {
            $this->wtQuotationLines->removeElement($wtQuotationLine);
            // set the owning side to null (unless already changed)
            if ($wtQuotationLine->getQuotation() === $this) {
                $wtQuotationLine->setQuotation(null);
            }
        }

        return $this;
    }
}
