<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Table(indexes={@Index(name="contact_id", columns={"contact_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\CompanyContactsRepository")
 */
class CompanyContacts
{
	public function __toString()
               	{
               		return $this->getContactName();
               	}

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $ContactId;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $ContactName;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $ContactAttn;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $ContactJobDescription;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="companyContacts")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $Company;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Cpt", mappedBy="CompanyContact")
	 */
	private $cpts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WtQuotation", mappedBy="CompanyContacts")
     */
    private $wtQuotations;

	public function __construct()
               	{
               		$this->cpts = new ArrayCollection();
               		$this->Cpts = new ArrayCollection();
                 $this->wtQuotations = new ArrayCollection();
               	}

	public function getId(): ?int
               	{
               		return $this->id;
               	}

	public function getContactId(): ?int
               	{
               		return $this->ContactId;
               	}

	public function setContactId(int $ContactId): self
               	{
               		$this->ContactId = $ContactId;
               
               		return $this;
               	}

	public function getContactName(): ?string
               	{
               		return $this->ContactName;
               	}

	public function setContactName(string $ContactName): self
               	{
               		$this->ContactName = $ContactName;
               
               		return $this;
               	}

	public function getContactAttn(): ?string
               	{
               		return $this->ContactAttn;
               	}

	public function setContactAttn(string $ContactAttn): self
               	{
               		$this->ContactAttn = $ContactAttn;
               
               		return $this;
               	}

	public function getContactJobDescription(): ?string
               	{
               		return $this->ContactJobDescription;
               	}

	public function setContactJobDescription(?string $ContactJobDescription): self
               	{
               		$this->ContactJobDescription = $ContactJobDescription;
               
               		return $this;
               	}

	public function getCompany(): ?Company
               	{
               		return $this->Company;
               	}

	public function setCompany(?Company $Company): self
               	{
               		$this->Company = $Company;
               
               		return $this;
               	}

	/**
	 * @return Collection|Cpt[]
	 */
	public function getCpts(): Collection
               	{
               		return $this->cpts;
               	}

	public function addCpt(Cpt $cpt): self
               	{
               		if (!$this->cpts->contains($cpt)) {
               			$this->cpts[] = $cpt;
               			$cpt->setCompanyContact($this);
               		}
               
               		return $this;
               	}

	public function removeCpt(Cpt $cpt): self
               	{
               		if ($this->cpts->contains($cpt)) {
               			$this->cpts->removeElement($cpt);
               			// set the owning side to null (unless already changed)
               			if ($cpt->getCompanyContact() === $this) {
               				$cpt->setCompanyContact(null);
               			}
               		}
               
               		return $this;
               	}

    /**
     * @return Collection|WtQuotation[]
     */
    public function getWtQuotations(): Collection
    {
        return $this->wtQuotations;
    }

    public function addWtQuotation(WtQuotation $wtQuotation): self
    {
        if (!$this->wtQuotations->contains($wtQuotation)) {
            $this->wtQuotations[] = $wtQuotation;
            $wtQuotation->setCompanyContacts($this);
        }

        return $this;
    }

    public function removeWtQuotation(WtQuotation $wtQuotation): self
    {
        if ($this->wtQuotations->contains($wtQuotation)) {
            $this->wtQuotations->removeElement($wtQuotation);
            // set the owning side to null (unless already changed)
            if ($wtQuotation->getCompanyContacts() === $this) {
                $wtQuotation->setCompanyContacts(null);
            }
        }

        return $this;
    }
}
