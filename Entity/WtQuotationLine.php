<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WtQuotationLineRepository")
 */
class WtQuotationLine
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\WtQuotation", inversedBy="wtQuotationLines")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Quotation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ArticleNumber;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Description;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5)
     */
    private $PurchasePrice;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=1)
     */
    private $MinMargin;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=1)
     */
    private $PreferredMargin;

    /**
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $PreferredPrice;

    /**
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $MinPrice;

    /**
     * @ORM\Column(type="integer")
     */
    private $SellingUnit;

    /**
     * @ORM\Column(type="integer")
     */
    private $Quantity;

    /**
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $SellingPrice;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $NameSupplier;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\WtClassification", inversedBy="wtQuotationLines")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Classification;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\WtProductLine", inversedBy="wtQuotationLines")
     * @ORM\JoinColumn(nullable=true)
     */
    private $ProductLine;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ArticleNumberSupplier;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2, nullable=true)
     */
    private $VAT;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ArticleType;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuotation(): ?WtQuotation
    {
        return $this->Quotation;
    }

    public function setQuotation(?WtQuotation $Quotation): self
    {
        $this->Quotation = $Quotation;

        return $this;
    }

    public function getArticleNumber(): ?string
    {
        return $this->ArticleNumber;
    }

    public function setArticleNumber(string $ArticleNumber): self
    {
        $this->ArticleNumber = $ArticleNumber;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getPurchasePrice(): ?string
    {
        return $this->PurchasePrice;
    }

    public function setPurchasePrice(string $PurchasePrice): self
    {
        $this->PurchasePrice = $PurchasePrice;

        return $this;
    }

    public function getMinMargin(): ?string
    {
        return $this->MinMargin;
    }

    public function setMinMargin(string $MinMargin): self
    {
        $this->MinMargin = $MinMargin;

        return $this;
    }

    public function getPreferredMargin(): ?string
    {
        return $this->PreferredMargin;
    }

    public function setPreferredMargin(string $PreferredMargin): self
    {
        $this->PreferredMargin = $PreferredMargin;

        return $this;
    }

    public function getPreferredPrice(): ?string
    {
        return $this->PreferredPrice;
    }

    public function setPreferredPrice(string $PreferredPrice): self
    {
        $this->PreferredPrice = $PreferredPrice;

        return $this;
    }

    public function getMinPrice(): ?string
    {
        return $this->MinPrice;
    }

    public function setMinPrice(string $MinPrice): self
    {
        $this->MinPrice = $MinPrice;

        return $this;
    }

    public function getSellingUnit(): ?int
    {
        return $this->SellingUnit;
    }

    public function setSellingUnit(int $SellingUnit): self
    {
        $this->SellingUnit = $SellingUnit;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->Quantity;
    }

    public function setQuantity(int $Quantity): self
    {
        $this->Quantity = $Quantity;

        return $this;
    }

    public function getSellingPrice(): ?string
    {
        return $this->SellingPrice;
    }

    public function setSellingPrice(?string $SellingPrice = null): ?self
    {
        $this->SellingPrice = $SellingPrice;

        return $this;
    }

    public function getNameSupplier(): ?string
    {
        return $this->NameSupplier;
    }

    public function setNameSupplier(string $NameSupplier): self
    {
        $this->NameSupplier = $NameSupplier;

        return $this;
    }

    public function getClassification(): ?WtClassification
    {
        return $this->Classification;
    }

    public function setClassification(?WtClassification $Classification): self
    {
        $this->Classification = $Classification;

        return $this;
    }

    public function getProductLine(): ?WtProductLine
    {
        return $this->ProductLine;
    }

    public function setProductLine(?WtProductLine $ProductLine): self
    {
        $this->ProductLine = $ProductLine;

        return $this;
    }

    public function getArticleNumberSupplier(): ?string
    {
        return $this->ArticleNumberSupplier;
    }

    public function setArticleNumberSupplier(?string $ArticleNumberSupplier): self
    {
        $this->ArticleNumberSupplier = $ArticleNumberSupplier;

        return $this;
    }

    public function getVAT(): ?string
    {
        return $this->VAT;
    }

    public function setVAT(?string $VAT): self
    {
        $this->VAT = $VAT;

        return $this;
    }

    public function getArticleType(): ?string
    {
        return $this->ArticleType;
    }

    public function setArticleType(?string $ArticleType): self
    {
        $this->ArticleType = $ArticleType;

        return $this;
    }
}
