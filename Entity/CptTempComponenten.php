<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CptTempComponentenRepository")
 */
class CptTempComponenten
{
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $SupplierComponentNumber;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $Supplier;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $Description;

	/**
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	private $SampleSupplied;

	/**
	 * @ORM\Column(type="decimal", precision=15, scale=5, nullable=true)
	 */
	private $PurchasePrice;

	/**
	 * @ORM\Column(type="decimal", precision=15, scale=4, nullable=true)
	 */
	private $NormTime;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\ComponentClassification", inversedBy="cptTempComponentens")
	 */
	private $Classification;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\ComponentType", inversedBy="cptTempComponentens")
	 */
	private $ComponentType;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\CptComponenten", mappedBy="CptTempComponenten")
	 */
	private $cptComponentens;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\ComponentStatus", inversedBy="cptTempComponentens")
	 */
	private $Status;

	public function __construct()
	{
		$this->cptComponentens = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getSupplierComponentNumber(): ?string
	{
		return $this->SupplierComponentNumber;
	}

	public function setSupplierComponentNumber(string $SupplierComponentNumber): self
	{
		$this->SupplierComponentNumber = $SupplierComponentNumber;

		return $this;
	}

	public function getSupplier(): ?string
	{
		return $this->Supplier;
	}

	public function setSupplier(?string $Supplier): self
	{
		$this->Supplier = $Supplier;

		return $this;
	}

	public function getDescription(): ?string
	{
		return $this->Description;
	}

	public function setDescription(?string $Description): self
	{
		$this->Description = $Description;

		return $this;
	}

	public function getSampleSupplied(): ?bool
	{
		return $this->SampleSupplied;
	}

	public function setSampleSupplied(?bool $SampleSupplied): self
	{
		$this->SampleSupplied = $SampleSupplied;

		return $this;
	}

	public function getPurchasePrice(): ?string
	{
		return $this->PurchasePrice;
	}

	public function setPurchasePrice(?string $PurchasePrice): self
	{
		$this->PurchasePrice = $PurchasePrice;

		return $this;
	}

	public function getNormTime(): ?string
	{
		return $this->NormTime;
	}

	public function setNormTime(?string $NormTime): self
	{
		$this->NormTime = $NormTime;

		return $this;
	}

	public function getClassification(): ?ComponentClassification
	{
		return $this->Classification;
	}

	public function setClassification(?ComponentClassification $Classification): self
	{
		$this->Classification = $Classification;

		return $this;
	}

	public function getComponentType(): ?ComponentType
	{
		return $this->ComponentType;
	}

	public function setComponentType(?ComponentType $ComponentType): self
	{
		$this->ComponentType = $ComponentType;

		return $this;
	}

	/**
	 * @return Collection|CptComponenten[]
	 */
	public function getCptComponentens(): Collection
	{
		return $this->cptComponentens;
	}

	public function addCptComponenten(CptComponenten $cptComponenten): self
	{
		if (!$this->cptComponentens->contains($cptComponenten)) {
			$this->cptComponentens[] = $cptComponenten;
			$cptComponenten->setCptTempComponenten($this);
		}

		return $this;
	}

	public function removeCptComponenten(CptComponenten $cptComponenten): self
	{
		if ($this->cptComponentens->contains($cptComponenten)) {
			$this->cptComponentens->removeElement($cptComponenten);
			// set the owning side to null (unless already changed)
			if ($cptComponenten->getCptTempComponenten() === $this) {
				$cptComponenten->setCptTempComponenten(null);
			}
		}

		return $this;
	}

	public function getStatus(): ?ComponentStatus
	{
		return $this->Status;
	}

	public function setStatus(?ComponentStatus $Status): self
	{
		$this->Status = $Status;

		return $this;
	}
}
