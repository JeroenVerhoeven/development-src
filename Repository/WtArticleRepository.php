<?php

namespace App\Repository;

use App\Entity\WtArticle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method WtArticle|null find($id, $lockMode = null, $lockVersion = null)
 * @method WtArticle|null findOneBy(array $criteria, array $orderBy = null)
 * @method WtArticle[]    findAll()
 * @method WtArticle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WtArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WtArticle::class);
    }

        public function findFirstArticleRows($MaxResults)
        {
                return $this->createQueryBuilder('w')
			->leftjoin('w.Classification', 'c')
                        ->orderBy('w.ArticleNumber', 'ASC')
                        ->setMaxResults($MaxResults)
                        ->getQuery()
                        ->getResult();
        }

        public function filterArticles($aSearchFields)
        {
                $QueryBuilder =  $this->createQueryBuilder('w');

		$QueryBuilder->select('w.ArticleNumber, w.ArticleType, w.id, w.Description, w.PackingUnit, w.VAT, w.PurchasePrice, w.MinMargin, w.PreferredMargin, w.MinimalPrice, w.PreferredPrice, w.ArticleNumberSupplier, c.Description as Classification');

		$QueryBuilder->leftjoin('w.Classification', 'c');
                $QueryBuilder->leftjoin('w.Productline', 'p');

		if(!empty($aSearchFields['articleNumber'])) {
                        $QueryBuilder->andWhere('w.ArticleNumber LIKE  :articleNumber');
                        $QueryBuilder->setParameter("articleNumber", '%' . $aSearchFields['articleNumber'] . '%');
                }

		if(!empty($aSearchFields['ArticleNumberSupplier'])) {
			$QueryBuilder->andWhere('w.ArticleNumberSupplier LIKE  :ArticleNumberSupplier');
			$QueryBuilder->setParameter("ArticleNumberSupplier", '%' . $aSearchFields['ArticleNumberSupplier'] . '%');
		}

		if(!empty($aSearchFields['Description'])) {
			$QueryBuilder->andWhere('w.Description LIKE  :Description');
			$QueryBuilder->setParameter("Description", '%' . $aSearchFields['Description'] . '%');
		}

		return $QueryBuilder->getQuery()->getResult();
        }

    // /**
    //  * @return WtArticle[] Returns an array of WtArticle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WtArticle
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
