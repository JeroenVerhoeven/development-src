<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CptQuoteRepository")
 */
class CptQuote
{
	public function __toString()
	{
		return $this->getDescription();
	}

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $QuoteId;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $Description;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Cpt", mappedBy="Quote")
	 */
	private $cpts;

	public function __construct()
	{
		$this->cpts = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getQuoteId(): ?int
	{
		return $this->QuoteId;
	}

	public function setQuoteId(int $QuoteId): self
	{
		$this->QuoteId = $QuoteId;

		return $this;
	}

	public function getDescription(): ?string
	{
		return $this->Description;
	}

	public function setDescription(string $Description): self
	{
		$this->Description = $Description;

		return $this;
	}

	/**
	 * @return Collection|Cpt[]
	 */
	public function getCpts(): Collection
	{
		return $this->cpts;
	}

	public function addCpt(Cpt $cpt): self
	{
		if (!$this->cpts->contains($cpt)) {
			$this->cpts[] = $cpt;
			$cpt->setQuote($this);
		}

		return $this;
	}

	public function removeCpt(Cpt $cpt): self
	{
		if ($this->cpts->contains($cpt)) {
			$this->cpts->removeElement($cpt);
			// set the owning side to null (unless already changed)
			if ($cpt->getQuote() === $this) {
				$cpt->setQuote(null);
			}
		}

		return $this;
	}
}
