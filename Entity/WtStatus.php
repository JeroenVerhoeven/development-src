<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WtStatusRepository")
 */
class WtStatus
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WtQuotation", mappedBy="Status")
     */
    private $wtQuotations;

    public function __construct()
    {
        $this->wtQuotations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    /**
     * @return Collection|WtQuotation[]
     */
    public function getWtQuotations(): Collection
    {
        return $this->wtQuotations;
    }

    public function addWtQuotation(WtQuotation $wtQuotation): self
    {
        if (!$this->wtQuotations->contains($wtQuotation)) {
            $this->wtQuotations[] = $wtQuotation;
            $wtQuotation->setStatus($this);
        }

        return $this;
    }

    public function removeWtQuotation(WtQuotation $wtQuotation): self
    {
        if ($this->wtQuotations->contains($wtQuotation)) {
            $this->wtQuotations->removeElement($wtQuotation);
            // set the owning side to null (unless already changed)
            if ($wtQuotation->getStatus() === $this) {
                $wtQuotation->setStatus(null);
            }
        }

        return $this;
    }
}
