<?php
/**
 * Created by PhpStorm.
 * User: Jeroen
 * Date: 27-8-2019
 * Time: 04:43
 */

namespace App\Command;

use App\Entity\ComponentClassification;
use App\Entity\ComponentGroup;
use App\Entity\ComponentStatus;
use App\Entity\ComponentSupplier;
use App\Entity\ComponentType;
use App\Entity\Cpt;
use App\Entity\CptBurden;
use App\Entity\CptComponenten;
use App\Entity\CptSurgeryType;
use App\Entity\CptStatus;
use App\Entity\Component;
//use App\Repository\ComponentGroupRepository;
use App\Entity\CptTempComponenten;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportCptComponentCommand extends Command
{
	protected static $defaultName = 'medica:import-cpts-components';

	private $entityManager;

	private $container;

	public function __construct(ContainerInterface $container)
	{
		parent::__construct();
		$this->container = $container;
	}

	protected function configure()
	{
		$this
			->setDescription("Import Cpt-Componenten")
			->setHelp("This command allows you to import all Cpt-Componenten")
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$output->writeln("Start import import-cpts-components");

		$InputPath = getcwd() . '/src/Command/ImportData/';

		$InputFile = "tbl_cpt_component.csv";

		$InputPathFile = $InputPath . $InputFile;

		if(!file_exists($InputPathFile)) {

			$LogData = "ERROR: LET OP! Bestand " . $InputPathFile . " bestaat niet";

			$output->writeln("<info>". $LogData . "</info>");

			exit;
		}

		$this->entityManager = $this->container->get('doctrine')->getManager();

		$time_start 	= microtime(true);

		$fp = file($InputPathFile);
		$AantalRegels =  count($fp);

		$handle = fopen($InputPathFile, "r");

		if (empty($handle) === false) {

			$teller = 0;

			while (($data = fgetcsv($handle, 999999, "|")) !== FALSE) {

				$teller++;

				if($teller < 473800) {
					continue;
				}

//				if(trim($data[0]) != '117241AA') {
//					continue;
//				}

				if ($teller % 100 == 0) {

					$time_end 	= microtime(true);
					$time 		= sprintf("%.3f", $time_end - $time_start);

					$output->writeln("rows: " . $teller . "/" . $AantalRegels . ", time spend: ". $time);

					$time_start = microtime(true);
				}

                                $aContent['CptNumber'] = trim($data[0]);
                                $aContent['component_number'] = $data[1];
                                $aContent['ComponentNumberSupplier'] = $data[2];
                                $aContent['Description'] = $data[3];
                                $aContent['ComponentGroup'] = $data[4];
                                $aContent['ComponentSupplier'] = $data[5];
                                $aContent['PurchasePrice'] = $data[6];
                                $aContent['Classification'] = $data[7];
                                $aContent['Status'] = $data[8];
                                $aContent['SampleSupplied'] = $data[9];
                                $aContent['Image'] = $data[10];
                                $aContent['ComponentType'] = $data[11];
                                $aContent['Quantity'] = $data[12];
                                $aContent['NormTime'] = $data[13];
                                $aContent['ComponentStatus'] = $data[14];

//                                print_r($aContent);
//                                exit;

//				if($aContent['CptNumber'] !== '100004AC') {
//					continue;
//				}

				if(empty($aContent['CptNumber'])) {
					continue;
				}

				if(!$this->existsInDb($aContent['CptNumber'], $aContent['component_number']))
				{
					$this->addCptComponent($aContent);
				}

				$this->entityManager->flush();

				$this->entityManager->clear();
			}
		}

		$output->writeln("Einde import import-cpts-components");
	}

	private function addCptComponent($aContent)
	{
		$oCptComponenten = new CptComponenten();

//		print_r($aContent);
//		exit;

		$oCptComponenten->setQuantity($aContent['Quantity']);
		$oCptComponenten->setComponentNumber($aContent['component_number']);
		$oCptComponenten->setCptNumber($aContent['CptNumber']);
		$oCptComponenten->setComponentNumberSupplier($aContent['ComponentNumberSupplier']);
		$oCptComponenten->setDescription($aContent['Description']);
		$oCptComponenten->setPurchasePrice($aContent['PurchasePrice']);
		$oCptComponenten->setSampleSupplied($aContent['SampleSupplied']);
		$oCptComponenten->setImage($aContent['Image']);
		$oCptComponenten->setNormTime($aContent['NormTime']);

		$oCpt = $this->getCpt($aContent['CptNumber']);
		if($oCpt) {
			$oCptComponenten->setCpt($oCpt);
		}
		else {
			echo "Cpt " . $aContent['CptNumber'] . " bestaat niet, volgende" . PHP_EOL;
			return;
		}

		$oComponent = $this->getComponent($aContent['component_number']);
		if($oComponent) {
			$oCptComponenten->setComponent($oComponent);
		}

                $oCptComponentStatus = $this->getStatusComp($aContent['ComponentStatus']);
                if($oCptComponentStatus) {
                        $oCptComponenten->setCptComponentStatus($oCptComponentStatus);
                }

		if(!empty($aContent['Status'])) {
			$oComponentStatus = $this->getStatusCompId($aContent['Status']);
			if($oComponentStatus) {
				$oCptComponenten->setStatus($oComponentStatus);
			}
		}

                $oComponentSupplier = $this->getSupplier($aContent['ComponentSupplier']);
                if($oComponentSupplier) {
                        $oCptComponenten->setComponentSupplier($oComponentSupplier);
                }

                $oComponentGroup = $this->getComponentGroup($aContent['ComponentGroup']);
                if($oComponentGroup) {
                        $oCptComponenten->setComponentGroup($oComponentGroup);
                }

                $oComponentClassification = $this->getComponentClassification($aContent['Classification']);
                if($oComponentClassification) {
                        $oCptComponenten->setComponentClassification($oComponentClassification);
                }

                $oComponentType = $this->getComponentType($aContent['ComponentType']);
                if($oComponentType) {
                        $oCptComponenten->setComponentType($oComponentType);
                }

                $this->entityManager->persist($oCptComponenten);

                $this->entityManager->flush();
	}

	private function existsInDb($CptNumber, $ComponentNumber)
	{
		$result = $this->entityManager->getRepository(CptComponenten::class)->findBy(
			[
				'CptNumber' => $CptNumber,
				'ComponentNumber' => $ComponentNumber
			]
		);

		if($result) {

			return true;
		}
		return false;
	}

	private function getCpt($CptNumber)
	{
		$aCpt = $this->entityManager->getRepository(Cpt::class)->findBy(
			['CptNumber' => $CptNumber ]
		);

		if($aCpt) {
			return $aCpt[0];
		}
	}

	private function getComponent($component_number)
	{
		$aComponent = $this->entityManager->getRepository(Component::class)->findBy(
			['ComponentNumber' => $component_number]
		);

		if($aComponent) {
			return $aComponent[0];
		}
	}

        private function getStatusComp($ComponentStatus)
        {
                $aComponentStatus = $this->entityManager->getRepository(ComponentStatus::class)->findBy(
                        ['Description' => $ComponentStatus]
                );

                if($aComponentStatus) {
                        return $aComponentStatus[0];
                }
        }

	private function getStatusCompId($ComponentStatusId)
	{
		$aComponentStatus = $this->entityManager->getRepository(ComponentStatus::class)->findBy(
			['id' => $ComponentStatusId]
		);

		if($aComponentStatus) {
			return $aComponentStatus[0];
		}
	}

        private function getSupplier($ComponentSupplier)
        {
                $aComponentSupplier = $this->entityManager->getRepository(ComponentSupplier::class)->findBy(
                        ['SupplierId' => $ComponentSupplier]
                );

                if($aComponentSupplier) {
                        return $aComponentSupplier[0];
                }
        }

        private function getComponentGroup($ComponentGroup)
        {
                $aComponentGroup = $this->entityManager->getRepository(ComponentGroup::class)->findBy(
                        ['ComponentGroupId' => $ComponentGroup]
                );

                if($aComponentGroup) {
                        return $aComponentGroup[0];
                }
        }

        private function getComponentClassification($Classification)
        {
                $aComponentClassification = $this->entityManager->getRepository(ComponentClassification::class)->findBy(
                        ['id' => $Classification]
                );

                if($aComponentClassification) {
                        return $aComponentClassification[0];
                }
        }

        private function getComponentType($ComponentType)
        {
                $aComponentType = $this->entityManager->getRepository(ComponentType::class)->findBy(
                        ['id' => $ComponentType]
                );

                if($aComponentType) {
                        return $aComponentType[0];
                }
        }

}