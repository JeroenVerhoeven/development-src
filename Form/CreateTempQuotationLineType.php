<?php

namespace App\Form;

use App\Entity\WtQuotationLine;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CreateTempQuotationLineType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ArticleNumber')
            ->add('Description')
            ->add('PurchasePrice')
            ->add('MinMargin')
            ->add('PreferredMargin')
            ->add('PreferredPrice')
            ->add('MinPrice')
            ->add('SellingUnit')
            ->add('Quantity')
            ->add('SellingPrice')
            ->add('NameSupplier')
            ->add('ArticleNumberSupplier')
            ->add('VAT')
            ->add('ArticleType')
            ->add('Classification')
            ->add('ProductLine')
            ->add('Save', SubmitType::class, ['label' => 'Save article'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WtQuotationLine::class,
        ]);
    }
}
