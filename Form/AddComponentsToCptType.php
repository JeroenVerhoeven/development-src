<?php

namespace App\Form;

use App\Entity\Component;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddComponentsToCptType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('component_number')
            ->add('component_number_supplier')
            ->add('description')
            ->add('purchase_price')
            ->add('norm_time')
            ->add('image')
            ->add('description_nl')
            ->add('description_fr')
            ->add('description_ge')
            ->add('description_es')
            ->add('description_it')
            ->add('NL_BTW_CODE')
            ->add('NL_BTW_PERC')
            ->add('BE_BTW_CODE')
            ->add('BE_BTW_PERC')
            ->add('ComponentGroup')
            ->add('Classification')
            ->add('Status')
            ->add('componentSupplier')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Component::class,
        ]);
    }
}
