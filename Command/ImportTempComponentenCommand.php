<?php
/**
 * Created by PhpStorm.
 * User: Jeroen
 * Date: 27-8-2019
 * Time: 04:43
 */

namespace App\Command;

use App\Entity\Component;
use App\Entity\ComponentGroup;
use App\Entity\ComponentStatus;
use App\Entity\ComponentSupplier;
use App\Entity\ComponentType;
use App\Entity\Cpt;
use App\Entity\CptTempComponenten;
use App\Repository\ComponentGroupRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportTempComponentenCommand extends Command
{
	protected static $defaultName = 'medica:import-temp-componenten';

	private $entityManager;

	private $container;

	private $aStatussen;

	private $aClassificaties;

	public function __construct(ContainerInterface $container)
	{
		parent::__construct();
		$this->container = $container;
	}

	protected function configure()
	{
		$this
			->setDescription("Import temp Componenten")
			->setHelp("This command allows you to import all Componenten")
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$output->writeln("Start import import-temp-componenten");

		$InputPath = getcwd() . '/src/Command/ImportData/';

		$InputFile = "tbl_temporary_component.csv";

		$InputPathFile = $InputPath . $InputFile;

		if(!file_exists($InputPathFile)) {

			$LogData = "ERROR: LET OP! Bestand " . $InputPathFile . " bestaat niet";

			$output->writeln("<info>". $LogData . "</info>");

			exit;
		}

		$this->aStatussen = $this->getStatusFromFile($InputPath . 'tbl_status_component.csv');

		$this->aClassificaties = $this->getClassificatiesFromFile($InputPath . 'tbl_classificatie.csv');;

		$this->entityManager = $this->container->get('doctrine')->getManager();

		$handle = fopen($InputPathFile, "r");

		if (empty($handle) === false) {

			$teller = 0;

			while (($data = fgetcsv($handle, 999999, "|")) !== FALSE) {

				$teller++;

				$aContent['supplier_component_number'] = trim($data[0]);
				$aContent['supplier'] = $data[1];
				$aContent['description'] = $data[2];
				$aContent['classification'] = $data[3];
				$aContent['component_type'] = $data[4];
				$aContent['sample_supplied'] = $data[5];
				$aContent['inkoopprijs'] = $data[6];
				$aContent['normtijd'] = $data[7];

				if(!$this->existsInDb($aContent['supplier_component_number']))
				{
					$this->addComponent($aContent);
				}
			}

			$this->entityManager->flush();
		}

		$output->writeln("Einde import import-componenten");
	}

	private function addComponent($aContent)
	{
		$oTempComponent = new CptTempComponenten();

		$Inkoopprijs =  $input = str_replace(',', '.', $aContent['inkoopprijs']);
		$Normtijd =  $input = str_replace(',', '.', $aContent['normtijd']);

		$oTempComponent->setSupplierComponentNumber($aContent['supplier_component_number']);
		$oTempComponent->setSupplier($aContent['supplier']);
		$oTempComponent->setDescription($aContent['description']);
		$oTempComponent->setPurchasePrice($Inkoopprijs);
		$oTempComponent->setNormTime($Normtijd);

		if($aContent['sample_supplied'] == 'Yes') {
			$oTempComponent->setSampleSupplied(1);
		}

//		$oComponentStatus = $this->getComponentStatus($aContent['status']);
//		if($oComponentStatus) {
//			$oTempComponent->setStatus($oComponentStatus);
//		}

		if($this->aClassificaties[$aContent['classification']]) {
			$oTempComponent->setClassification($this->aClassificaties[$aContent['classification']]);
		}

		$oComponentType = $this->getComponentType($aContent['component_type']);
		if($oComponentType) {
			$oTempComponent->setComponentType($oComponentType);
		}

		$this->entityManager->persist($oTempComponent);
	}

	private function existsInDb($supplier_component_number)
	{
		$result = $this->entityManager->getRepository(CptTempComponenten::class)->findBy(
			['SupplierComponentNumber' => $supplier_component_number]
		);

		if($result) {

			return true;
		}
		return false;
	}

	private function getSupplier($leverancier_id)
	{
		$oComponentSupplier = $this->entityManager->getRepository(ComponentSupplier::class)->findBy(
			['SupplierId' => $leverancier_id]
		);

		return $oComponentSupplier[0];
	}

	private function getComponentGroup($component_group_id)
	{
		$oComponentGroup = $this->entityManager->getRepository(ComponentGroup::class)->findBy(
			['ComponentGroupId' => $component_group_id]
		);

		return $oComponentGroup[0];
	}

	private function getStatusFromFile($InputPathFile)
	{
		$handle = fopen($InputPathFile, "r");

		if (empty($handle) === false) {

			while (($data = fgetcsv($handle, 999999, "|")) !== FALSE) {

				$this->aStatussen[$data[1]] = $data[0];
			}
		}

		return;
	}

	private function getComponentStatus($status)
	{
		$oComponentStatus = $this->entityManager->getRepository(ComponentStatus::class)->findBy(
			['Description' => $status]
		);

		if(!$oComponentStatus) {

			$oComponentStatus = new ComponentStatus();
			$oComponentStatus->setDescription($status);

			$this->entityManager->persist($oComponentStatus);
			$this->entityManager->flush();

			return $oComponentStatus;

		}
		return $oComponentStatus[0];
	}

	private function getClassificatiesFromFile($InputPathFile)
	{
		$handle = fopen($InputPathFile, "r");

		if (empty($handle) === false) {

			while (($data = fgetcsv($handle, 999999, "|")) !== FALSE) {

				$this->aClassificaties[$data[0]] = $data[1];
			}
		}

		return;
	}

	private function getComponentType($component_type)
	{
		$oComponentType = $this->entityManager->getRepository(ComponentType::class)->findBy(
			['Description' => $component_type]
		);

		if($oComponentType) {
			return $oComponentType[0];
		}

	}
}