<?php

namespace App\Form;

use App\Entity\Component;
use App\Entity\Cpt;

use App\Repository\ComponentRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CptChangeCalcType extends AbstractType
{
	//SELECT component_nummer, omschrijving from tbl_componenten where componentgroep = 9000
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('CptNumber', TextType::class, [
				'disabled' => true,
			])
			->add('Margin')
			->add('Burden')
			->add('Save', SubmitType::class, [
				'label' => 'Calculate',
			])
		;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cpt::class,
        ]);
    }
}
