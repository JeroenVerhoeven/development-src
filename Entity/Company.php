<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Table(indexes={@Index(name="company_id", columns={"company_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 */
class Company
{
	public function __toString()
               	{
               		return (string)$this->getCompanyName();
               	}

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $CompanyId;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $CompanyName;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $CompanyStreet;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $CopmpanyZipcode;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $CompanyCity;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $CompanyPhone;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\CompanyContacts", mappedBy="Company")
	 */
	private $companyContacts;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Cpt", mappedBy="Company")
	 */
	private $cpts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WtQuotation", mappedBy="Customer")
     */
    private $wtQuotations;


	public function __construct()
               	{
               		$this->companyContacts = new ArrayCollection();
               		$this->cpts = new ArrayCollection();
                 $this->wtQuotations = new ArrayCollection();
               	}

	public function getId(): ?int
               	{
               		return $this->id;
               	}

	public function getCompanyId(): ?int
               	{
               		return $this->CompanyId;
               	}

	public function setCompanyId(int $CompanyId): self
               	{
               		$this->CompanyId = $CompanyId;
               
               		return $this;
               	}

	public function getCompanyName(): ?string
               	{
               		return $this->CompanyName;
               	}

	public function setCompanyName(string $CompanyName): self
               	{
               		$this->CompanyName = $CompanyName;
               
               		return $this;
               	}

	public function getCompanyStreet(): ?string
               	{
               		return $this->CompanyStreet;
               	}

	public function setCompanyStreet(string $CompanyStreet): self
               	{
               		$this->CompanyStreet = $CompanyStreet;
               
               		return $this;
               	}

	public function getCopmpanyZipcode(): ?string
               	{
               		return $this->CopmpanyZipcode;
               	}

	public function setCopmpanyZipcode(?string $CopmpanyZipcode): self
               	{
               		$this->CopmpanyZipcode = $CopmpanyZipcode;
               
               		return $this;
               	}

	public function getCompanyCity(): ?string
               	{
               		return $this->CompanyCity;
               	}

	public function setCompanyCity(string $CompanyCity): self
               	{
               		$this->CompanyCity = $CompanyCity;
               
               		return $this;
               	}

	public function getCompanyPhone(): ?string
               	{
               		return $this->CompanyPhone;
               	}

	public function setCompanyPhone(string $CompanyPhone): self
               	{
               		$this->CompanyPhone = $CompanyPhone;
               
               		return $this;
               	}

	/**
	 * @return Collection|CompanyContacts[]
	 */
	public function getCompanyContacts(): Collection
               	{
               		return $this->companyContacts;
               	}

	public function addCompanyContact(CompanyContacts $companyContact): self
               	{
               		if (!$this->companyContacts->contains($companyContact)) {
               			$this->companyContacts[] = $companyContact;
               			$companyContact->setCompany($this);
               		}
               
               		return $this;
               	}

	public function removeCompanyContact(CompanyContacts $companyContact): self
               	{
               		if ($this->companyContacts->contains($companyContact)) {
               			$this->companyContacts->removeElement($companyContact);
               			// set the owning side to null (unless already changed)
               			if ($companyContact->getCompany() === $this) {
               				$companyContact->setCompany(null);
               			}
               		}
               
               		return $this;
               	}

	/**
	 * @return Collection|Cpt[]
	 */
	public function getCpts(): Collection
               	{
               		return $this->cpts;
               	}

	public function addCpt(Cpt $cpt): self
               	{
               		if (!$this->cpts->contains($cpt)) {
               			$this->cpts[] = $cpt;
               			$cpt->setCompany($this);
               		}
               
               		return $this;
               	}

	public function removeCpt(Cpt $cpt): self
               	{
               		if ($this->cpts->contains($cpt)) {
               			$this->cpts->removeElement($cpt);
               			// set the owning side to null (unless already changed)
               			if ($cpt->getCompany() === $this) {
               				$cpt->setCompany(null);
               			}
               		}
               
               		return $this;
               	}

    /**
     * @return Collection|WtQuotation[]
     */
    public function getWtQuotations(): Collection
    {
        return $this->wtQuotations;
    }

    public function addWtQuotation(WtQuotation $wtQuotation): self
    {
        if (!$this->wtQuotations->contains($wtQuotation)) {
            $this->wtQuotations[] = $wtQuotation;
            $wtQuotation->setCustomer($this);
        }

        return $this;
    }

    public function removeWtQuotation(WtQuotation $wtQuotation): self
    {
        if ($this->wtQuotations->contains($wtQuotation)) {
            $this->wtQuotations->removeElement($wtQuotation);
            // set the owning side to null (unless already changed)
            if ($wtQuotation->getCustomer() === $this) {
                $wtQuotation->setCustomer(null);
            }
        }

        return $this;
    }
}
