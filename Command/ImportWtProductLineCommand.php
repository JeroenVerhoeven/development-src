<?php
/**
 * Created by PhpStorm.
 * User: Jeroen
 * Date: 27-8-2019
 * Time: 04:43
 */

namespace App\Command;

use App\Entity\WtProductLine;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportWtProductLineCommand extends Command
{
        protected static $defaultName = 'medica:import-wintrade-productline';

        private $entityManager;

        private $container;

        public function __construct(ContainerInterface $container)
        {
                parent::__construct();
                $this->container = $container;
        }

        protected function configure()
        {
                $this
                        ->setDescription("Import WinTrade product line")
                        ->setHelp("This command allows you to import all WinTrade product lines")
                ;
        }

        protected function execute(InputInterface $input, OutputInterface $output)
        {
                $output->writeln("Start import import-wintrade-productline");

                $InputPath = getcwd() . '/src/Command/ImportData/';

                $InputFile = "tbl_wtproductlijn.csv";

                $InputPathFile = $InputPath . $InputFile;

                if(!file_exists($InputPathFile)) {

                        $LogData = "ERROR: LET OP! Bestand " . $InputPathFile . " bestaat niet";

                        $output->writeln("<info>". $LogData . "</info>");

                        exit;
                }

                $this->entityManager = $this->container->get('doctrine')->getManager();

                $handle = fopen($InputPathFile, "r");

                if (empty($handle) === false) {

                        while (($data = fgetcsv($handle, 999999, ";")) !== FALSE) {

                                $aContent['productline_id'] = (int) $data[0];
                                $aContent['description'] = $data[1];
                                $aContent['MinMargin'] = $data[2];

                                if(!$this->existsInDb($aContent['description']))
                                {
                                        $WtProductLine = new WtProductLine();
                                        $WtProductLine->setDescription($aContent['description']);
                                        $WtProductLine->setMinMargin($aContent['MinMargin']);
                                        $WtProductLine->setProductlineId($aContent['productline_id']);

                                        $this->entityManager->persist($WtProductLine);
                                }
                        }

                        $this->entityManager->flush();
                }

                $output->writeln("Einde import import-wintrade-productline");
        }

        private function existsInDb($description)
        {
                $result = $this->entityManager->getRepository(WtProductLine::class)->findBy(
                        ['Description' => $description]
                );

                if($result) {

                        return true;
                }
                return false;
        }
}